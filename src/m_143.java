//https://leetcode.com/problems/reorder-list/


import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class m_143 {
    public static void reorderList(ListNode head) {

        Queue<ListNode> queue = new LinkedList<>();
        Stack<ListNode> stack = new Stack<>();

        int len = 0;
        ListNode p = head;
        while (p != null) {
            queue.add(p);
            stack.add(p);
            p = p.next;
            len++;
        }

        ListNode s = new ListNode(), e = new ListNode();
        for (int i = 0; i < len / 2; i++) {
            s = queue.poll();
            e = stack.pop();
            e.next = s.next;
            s.next = e;
        }

        if (len % 2 == 0) {
            e.next = null;
        } else {
            e.next = queue.poll();
            e.next.next = null;
        }

    }


    public static void main(String[] args) {

        ListNode h = new ListNode(1,2);
//        ListNode h = new ListNode(1, 2, 3, 4, 5);
        reorderList(h);
        System.out.println(h);
    }

}
