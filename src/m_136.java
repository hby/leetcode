//https://leetcode.com/problems/single-number/


public class m_136 {
    public static int singleNumber(int[] nums) {
        int result = 0;
        for (int num : nums) {
            result ^= num;
        }
        return result;
    }

    public static void main(String[] args) {


        System.out.println("Hello World");
    }

}
