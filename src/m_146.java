//https://leetcode.com/problems/lru-cache/


public class m_146 {
    static class DualLink {
        Integer key;
        Integer val;
        DualLink priv;
        DualLink next;

        public DualLink(int key, int val) {
            this.key = key;
            this.val = val;
        }

        @Override
        public String toString() {
            String p = "null", n = "null";
            if (priv != null) {
                p = priv.key + ":" + priv.val;
            }
            if (next != null) {
                n = next.key + ":" + next.val;
            }
            return "{key=" + key + ",val=" + val + ",priv=" + p + ",next=" + n + "}";
        }
    }

    DualLink[] cache;
    int capacity;
    int len;
    boolean isFull;

    DualLink head;
    DualLink tail;

    public m_146(int capacity) {
        this.cache = new DualLink[3001];
        this.capacity = capacity;
        this.len = 0;
        this.isFull = false;
        this.head = null;
        this.tail = null;
    }

    public int get(int key) {
        DualLink node = cache[key];
        if (node == null) {
            return -1;
        } else {
            moveToTail(node);
            return node.val;
        }

    }

    public void moveToTail(DualLink node) {
        if (head != tail) {
            if (node == head) {
                head = node.next;
                head.priv = null;

                tail.next = node;

                node.priv = tail;
                node.next = null;

                tail = node;
            } else if (node != tail) {
                DualLink priv = node.priv;
                DualLink next = node.next;
                priv.next = next;
                next.priv = priv;

                tail.next = node;

                node.priv = tail;
                node.next = null;

                tail = node;
            }
        }

    }

    public void put(int key, int value) {
        if (head == null) {
            head = new DualLink(key, value);
            tail = head;
            cache[key] = head;

            len = 1;
            isFull = isFull || len == capacity;
        } else {
            DualLink node = cache[key];
            if (node == null) {
                node = new DualLink(key, value);
                node.priv=tail;
                tail.next = node;
                tail = node;
                cache[key] = node;

                if (isFull) {
                    cache[head.key] = null;
                    head = head.next;
                    head.priv=null;
                } else {
                    len++;
                    isFull = len >= capacity;
                }
            } else {
                node.val = value;

                moveToTail(node);
            }

        }

    }

    public String toString() {

        DualLink w = head;
        if (w == null) {
            return "";
        } else {
            StringBuilder s = new StringBuilder();
            s.append("{");
            while (w != null) {
                s.append(w).append("-------");
                w = w.next;
            }
            s.append("}");
            return s.toString();
        }
    }


    public static void main(String[] args) {

        m_146 lRUCache = new m_146(2);
        lRUCache.put(1, 1); // cache is {1=1}
        lRUCache.put(2, 2); // cache is {1=1, 2=2}
        System.out.println(lRUCache.get(1));    // return 1
        lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
        System.out.println(lRUCache.get(2));    // returns -1 (not found)
        lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
        System.out.println(lRUCache.get(1));    // return -1 (not found)
        System.out.println(lRUCache.get(3));    // return 3
        System.out.println(lRUCache.get(4));    // return 4

//        LRUCache lRUCache = new LRUCache(2); // {}
//        System.out.println(lRUCache.get(2)); // -1
//        lRUCache.put(2, 6); // {2:6}
//        System.out.println(lRUCache.get(1)); // -1
//        lRUCache.put(1, 5); // {1:5,2:6}
//        lRUCache.put(1, 2); // {1:2,2:6}
//        System.out.println(lRUCache.get(1)); //2
//        System.out.println(lRUCache.get(2)); // 6

//        LRUCache lRUCache = new LRUCache(3); // {}
//
//        lRUCache.put(1, 1); // {1:1}
//        lRUCache.put(2, 2); // {1:1,2:2}
//        lRUCache.put(3, 3); // {1:1,2:2,3:3}
//        lRUCache.put(4, 4); // {2:2,3:3,4:4}
//
//        System.out.println(lRUCache.get(4)); // 4      {2:2,3:3,4:4}
//        System.out.println(lRUCache.get(3)); // 3      {2:2,4:4,3:3}
//        System.out.println(lRUCache.get(2)); // 2      {4:4,3:3,2:2}
//        System.out.println(lRUCache.get(1)); // -1
//
//        lRUCache.put(2, 6); // {2:6}   {4:4,3:3,2:6}
//
//        System.out.println(lRUCache.get(1)); // -1
//        System.out.println(lRUCache.get(2)); // 6    {4:4,3:3,2:6}
//        System.out.println(lRUCache.get(3)); // 3    {4:4,2:6,3:3}
//        System.out.println(lRUCache.get(4)); // 4    {2:6,3:3,4:4}
//        System.out.println(lRUCache.get(5)); // -1

    }

}
