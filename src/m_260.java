//https://leetcode.com/problems/single-number-iii/

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class m_260 {
    public static int[] singleNumber(int[] nums) {
        Set<Integer> s = new HashSet<>();
        for(int num:nums){
            if(s.contains(num)){
                s.remove(num);
            }else{
                s.add(num);
            }
        }

        return s.stream().mapToInt(Integer::intValue).toArray();
    }


    public static void main(String[] args) {


        System.out.println(Arrays.toString(singleNumber(new int[]{1, 2, 1, 3, 2, 5})));
    }

}
