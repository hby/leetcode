//https://leetcode.com/problems/n-queens-ii/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_52 {
    public static boolean check(int[] fit, int n, int pointer) {

        for (int f : fit) {
            if (f == 0) {
                return true;
            } else {
                int p = f - 1;
                if ((pointer / n == p / n) //同一条横线上
                        || (pointer % n == p % n) //同一条竖线上
                        || ((pointer / n - p / n) == (pointer % n - p % n))
                        || ((pointer / n - p / n) == (p % n - pointer % n))) {//同一条斜线上（即两个点之间斜率为正负1）
                    return false;
                }
            }
        }
        return true;
    }

    public static int subSolve(int n, int[] fit, int start, int remain) {
        if (remain == 0) {
            return 1;
        }

        int s = 0;
        for (int i = start; i < n * n; i++) {
            if (check(fit, n, i)) {
                fit[n - remain] = i + 1;
                s += subSolve(n, fit, i + 1, remain - 1);
                fit[n - remain] = 0;
            }
        }
        return s;
    }


    public static int totalNQueens(int n) {
        int total = 0;

        for (int first = 0; first < n * n; first++) {
            int[] fit = new int[n];
            fit[0] = first + 1;
            total += subSolve(n, fit, first, n - 1);
        }
        return total;
    }


    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            System.out.println(totalNQueens(i));

        }


    }

}
