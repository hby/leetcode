//https://leetcode.com/problems/longest-consecutive-sequence/


import java.util.*;

public class m_128 {
    public static int longestConsecutive(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        Arrays.sort(nums);
        int max = 1, currentLen = 1;
        int p = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == p + 1) {
                currentLen++;
                p = nums[i];
            } else if (nums[i] == p) {
                continue;
            } else {
                max = Math.max(max, currentLen);
                currentLen = 1;
            }
        }
        max = Math.max(max, currentLen);
        return max;
    }


    public static void main(String[] args) {


        System.out.println(longestConsecutive(new int[]{100, 4, 200, 201, 202, 203, 203, 205, 204, 1, 3, 2}));
        System.out.println(longestConsecutive(new int[]{}));
        System.out.println(longestConsecutive(new int[]{1}));
    }

}
