//https://leetcode.com/problems/word-ladder-ii/


import java.util.*;

public class m_126 {
    public static class MoveTree {
        public List<Long> paths;
        public Long value;

        public MoveTree(MoveTree parent, Long v) {
            this.paths = new LinkedList<>() {{
                addAll(parent.paths);
            }};
            this.paths.add(parent.value);
            this.value = v;
        }

        public MoveTree(Long v) {
            this.paths = new LinkedList<>();
            this.value = v;
        }
    }


    public static final int base = 27;

    public static Long[] mapToLong(String str, int wordLen) {
        // Since all str will be lowercase english letters
        // we will map every string to a 27-base number ( return in decimal)
        // 1 - 26 for a - z , 0 for a wildcard searcher
        // example: "abc" ->  "a":1,"b":2,"c":3 -> return 1*26^2+2*26^1+3*26^0
        //          "?bc" ->  "?":0,"b":2,"c":3 -> return 0*26^2+2*26^1+3*26^0
        // when we calculate "abc", we will also calculate "?bc","a?c","ab?"


        // a string will have 10 chars max, the return value will be less than 26^10
        // which is greater than Integer.MAX , so we use Long to represent the string
        Long[] m = new Long[wordLen + 1];
        Arrays.fill(m, 0L);
        int i = 0;
        for (char c : str.toCharArray()) {
            m[0] = m[0] * base + (c - 'a' + 1);
            for (int j = 1; j <= wordLen; j++) {
                m[j] = (j == i + 1) ? (m[j] * base) : (m[j] * base + (c - 'a' + 1));
            }
            i++;
        }
        return m;
    }

    public static String mapToString(long m, int wordLen) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wordLen; i++) {
            sb.append((char) ('a' + m % base - 1));
            m = m / base;
        }
        return sb.reverse().toString();
    }

    public static boolean getMove(List<MoveTree> layer,
                                  Map<Long, Set<Long>> forwardMap,
                                  Map<Long, Set<Long>> backwardMap,
                                  Long target
    ) {
        boolean found = false;
        int ls = layer.size();

        for (int i = 0; i < ls; i++) {
            MoveTree node = layer.remove(0);
            Set<Long> forwards = forwardMap.get(node.value);
            for (Long f : forwards) {
                Set<Long> inner = backwardMap.get(f);
                if (inner != null) {
                    inner.remove(node.value);
                    for (Long in : inner) {
                        layer.add(new MoveTree(node, in));
                        found = found || in.equals(target);
                    }
                }
            }
        }
        return found;
    }

    public static List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        int wordLen = beginWord.length();

        Long[] froms = mapToLong(beginWord, wordLen);
        Long[] toes = mapToLong(endWord, wordLen);

        Map<Long, Set<Long>> forward = new HashMap<>();
        Map<Long, Set<Long>> backward = new HashMap<>();

        for (String word : wordList) {
            Long[] ws = mapToLong(word, wordLen);

            forward.put(ws[0], new HashSet<>(Arrays.asList(Arrays.copyOfRange(ws, 1, wordLen + 1))));
            for (int j = 1; j <= wordLen; j++) {
                if (!backward.containsKey(ws[j])) {
                    backward.put(ws[j], new HashSet<>() {{
                        add(ws[0]);
                    }});
                } else {
                    backward.get(ws[j]).add(ws[0]);
                }
            }
        }
        MoveTree pathTree = new MoveTree(froms[0]);

        List<MoveTree> layer = new LinkedList<>();
        boolean found = false;

        for (int i = 1; i < froms.length; i++) {
            Set<Long> s = backward.get(froms[i]);
            if (s != null) {
                s.remove(froms[0]);
                for (Long in : s) {
                    layer.add(new MoveTree(pathTree, in));
                    found = found || in.equals(toes[0]);
                }
            }
        }

        while ((!(found || layer.isEmpty()))) {
            found = getMove(layer, forward, backward, toes[0]);
        }

        if (found) {
            List<List<String>> results = new LinkedList<>();
            for (MoveTree move : layer) {
                if (move.value.equals(toes[0])) {
                    List<String> result = new LinkedList<>();
                    for (Long step : move.paths) {
                        result.add(mapToString(step, wordLen));
                    }
                    result.add(mapToString(move.value, wordLen));
                    results.add(result);

                }
            }
            return results;
        } else {
            return new LinkedList<>();
        }
    }


    public static void main(String[] args) {

//        System.out.println(findLadders("hit", "cog", new ArrayList<>() {{
//            addAll(Arrays.asList("hot", "dot", "dog", "lot", "log", "cog"));
//        }}));

        System.out.println(findLadders("a", "c", new ArrayList<>() {{
            addAll(Arrays.asList("a", "b", "c"));
        }}));
    }

}
