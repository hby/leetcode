//https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/


import java.util.Arrays;
import java.util.List;

public class m_167 {
    public static int bs(int[] numbers, int target, int s, int e) {
        int j = (s + e) / 2;
        if (numbers[j] == target) {
            while (j >= 1 && numbers[j - 1] == target) {
                j--;
            }
            return j;
        } else if (numbers[j] > target) {
            return bs(numbers, target, s, j);
        } else {
            return bs(numbers, target, j + 1, e);

        }

    }

    public static int[] twoSum(int[] numbers, int target) {
        int[] s = new int[2001];
        for (int i = 0; i < numbers.length; i++) {
            int num = numbers[i];
            int index = num + 1000;
            s[index]++;

            int pair = target - num;
            if (pair <= 1000 && pair >= -1000 && s[pair + 1000] > 0 && (pair != num || s[pair + 1000] > 1)) {
                return new int[]{bs(numbers, pair, 0, numbers.length - 1) + 1, i + 1};
            }
        }
        return new int[]{0, 0};

//        int p = numbers.length / 2 - 1, q = p + 1;
//
//        while (p >= 0 && q < numbers.length) {
//            if (numbers[p] + numbers[q] == target) {
//                return new int[]{p + 1, q + 1};
//            } else if (p - 1 > 0 && numbers[p - 1] + numbers[q] == target) {
//                return new int[]{p, q + 1};
//            } else if (q + 1 < numbers.length && numbers[p] + numbers[q + 1] == target) {
//                return new int[]{p + 1, q + 2};
//            } else if (p - 1 > 0 && q + 1 < numbers.length && numbers[p - 1] + numbers[q + 1] == target) {
//                return new int[]{p, q + 2};
//            } else {
//                if (numbers[p] + numbers[q] > target) {
//                    p = p - 1;
//                    q = q - 1;
//                } else {
//                    q = q + 1;
//                    p = p + 1;
//                }
//            }
//        }
//        return new int[]{p + 1, q + 1};
    }


    public static void main(String[] args) {
//        System.out.println(Arrays.toString(twoSum(new int[]{0, 0, 3, 4}, 0)));
        System.out.println(Arrays.toString(twoSum(new int[]{-1000, -1, 0, 1}, 1)));

//        System.out.println(Arrays.toString(twoSum(new int[]{12, 13, 23, 28, 43, 44, 59, 60, 61, 68, 70, 86, 88, 92, 124, 125, 136, 168, 173, 173, 180, 199, 212, 221, 227, 230, 277, 282, 306, 314, 316, 321, 325, 328, 336, 337, 363, 365, 368, 370, 370, 371, 375, 384, 387, 394, 400, 404, 414, 422, 422, 427, 430, 435, 457, 493, 506, 527, 531, 538, 541, 546, 568, 583, 585, 587, 650, 652, 677, 691, 730, 737, 740, 751, 755, 764, 778, 783, 785, 789, 794, 803, 809, 815, 847, 858, 863, 863, 874, 887, 896, 916, 920, 926, 927, 930, 933, 957, 981, 997},
//                542)));
//        System.out.println(Arrays.toString(twoSum(new int[]{-1, 0}, -1)));
//        System.out.println(Arrays.toString(twoSum(new int[]{3, 24, 50, 79, 88, 150, 345}, 200)));
    }

}
