// https://leetcode.com/problems/broken-calculator/

public class m_991 {
    public static int brokenCalc(int startValue, int target) {
        if (startValue >= target) {
            return startValue - target;
        } else {
            if (target % 2 == 0) {
                return 1 + brokenCalc(startValue, target / 2);
            } else {
                return 2 + brokenCalc(startValue, (target + 1) / 2);
            }
        }
    }


    public static void main(String[] args) {


        System.out.println(brokenCalc(2, 3));
        System.out.println(brokenCalc(5, 8));
        System.out.println(brokenCalc(3, 10));
    }

}
