//https://leetcode.com/problems/search-in-rotated-sorted-array-ii/


public class m_81 {
    public static boolean search(int[] nums, int target) {
        return subSearch(nums, 0, nums.length - 1, target);
    }

    public static boolean subSearch(int[] nums, int startIndex, int endIndex, int target) {
        if (nums[endIndex] == target || nums[startIndex] == target) {
            return true;
        } else if (endIndex - startIndex <= 1) {
            return nums[endIndex] == target || nums[startIndex] == target;
        } else {
            int cut = (endIndex - startIndex) / 2 + startIndex;
            if (nums[cut] == target) {
                return true;
            }


            if (nums[startIndex] < nums[cut]) { // 旋转点在 [cut,end] 内，则start -> cut递增
                if (nums[cut] > target && nums[startIndex] < target) { // target 在 start-> cut 递增区间
                    return subSearch(nums, startIndex, cut, target);
                } else { // target 在 cut_2 -> end 的不单调区间
                    return subSearch(nums, cut, endIndex, target);
                }
            } else if (nums[startIndex] == nums[cut]) {
                // 若旋转点在 [start,cut] 上，则start -> cut 不单调
                // 若旋转点在 [cut,end] 上，则start -> cut 所有值相等
                // 若旋转点在 [cut,end] 上且target处于[start,cut] 区间，则nums[cut]=target与前面逻辑相悖

                // 故target可能存在于[start,cut]的不单调区间或[cut,end]的不单调区间
                return subSearch(nums, startIndex, cut, target) || subSearch(nums, cut, endIndex, target);

            } else { // cut_2 -> end 递增   start -> cut_1 不递增
                if (nums[cut] < target && nums[endIndex] > target) { // target 在递增区间
                    return subSearch(nums, cut, endIndex, target);
                } else { // target 在不递增区间
                    return subSearch(nums, startIndex, cut, target);
                }
            }
        }
    }


    public static void main(String[] args) {


        System.out.println(search(new int[]{4, 5, 6, 7, 0, 1, 2}, 4));
        System.out.println(search(new int[]{0, 1, 1, 2, 3, 0}, 3));
        System.out.println(search(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1}, 2));
        System.out.println(search(new int[]{2, 5, 6, 0, 0, 1, 2}, 3));
        System.out.println(search(new int[]{3, 3}, 3));


        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 4));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 5));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 6));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 7));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 0));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 1));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 2));
        System.out.println(search(new int[]{3, 4, 5, 6, 7, 0, 1, 2}, 3));
        System.out.println(search(new int[]{3}, 3));
        System.out.println(search(new int[]{3}, 4));
    }

}
