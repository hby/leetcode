//https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/


public class m_109 {
    public static TreeNode sortedListToBST(ListNode head) {
        int[] nums = new int[20001];
        int end = 0;
        while (head != null) {
            nums[end]=head.val;
            head = head.next;
            end++;
        }
        return sub(nums, 0, end - 1);

    }

    public static TreeNode sub(int[] nums, int start, int end) {
        if (end - start < 0) {
            return null;
        } else {
            int mid = (end - start) / 2 + start;
            return new TreeNode(nums[mid], sub(nums, start, mid - 1), sub(nums, mid + 1, end));
        }
    }

    public static void main(String[] args) {


        System.out.println("Hello World");
    }

}
