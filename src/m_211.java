// https://leetcode.com/problems/design-add-and-search-words-data-structure/


import java.util.HashMap;
import java.util.Map;

public class m_211 {
    public static void main(String[] args) {
        WordDictionary wordDictionary = new WordDictionary();
        wordDictionary.addWord("a");
        wordDictionary.addWord("a");
        System.out.println(wordDictionary.search(".")); // return True
        System.out.println(wordDictionary.search("a")); // return True
        System.out.println(wordDictionary.search("aa")); // return False
        System.out.println(wordDictionary.search("a")); // return True
        System.out.println(wordDictionary.search(".a")); // return False
        System.out.println(wordDictionary.search("a.")); // return False

        System.out.println("-----------");

        wordDictionary = new WordDictionary();
        wordDictionary.addWord("bad");
        wordDictionary.addWord("dad");
        wordDictionary.addWord("mad");
        System.out.println(wordDictionary.search("pad")); // return False
        System.out.println(wordDictionary.search("bad")); // return True
        System.out.println(wordDictionary.search(".ad")); // return True
        System.out.println(wordDictionary.search("b..")); // return True

        System.out.println("-----------");

        wordDictionary = new WordDictionary();
        wordDictionary.addWord("at");
        wordDictionary.addWord("and");
        wordDictionary.addWord("an");
        wordDictionary.addWord("add");
        System.out.println(wordDictionary.search("a")); // return False
        System.out.println(wordDictionary.search(".at")); // return False
        wordDictionary.addWord("bat");
        System.out.println(wordDictionary.search(".at")); // return True
        System.out.println(wordDictionary.search("an.")); // return True
        System.out.println(wordDictionary.search("a.d.")); // return False
        System.out.println(wordDictionary.search("b.")); // return False
        System.out.println(wordDictionary.search("a.d")); // return True
        System.out.println(wordDictionary.search(".")); // return False


    }

}

class WordDictionary {
    boolean stopHere;
    Map<Character, WordDictionary> cmap;

    /**
     * Initialize your data structure here.
     */
    public WordDictionary() {
        stopHere = false;
        cmap = new HashMap<>();
    }

    private WordDictionary addChar(char c) {
        if (cmap.containsKey(c)) {
            return cmap.get(c);
        } else {
            WordDictionary w = new WordDictionary();
            cmap.put(c, w);
            return w;
        }
    }

    public void addWord(String word) {
        WordDictionary w = this;
        for (char c : word.toCharArray()) {
            w = w.addChar(c);
        }
        w.stopHere = true;
    }

    private boolean subSearch(char[] word, int p) {
        if (p >= word.length) {
            return false;
        }
        if (word[p] == '.') {
            for (WordDictionary w : cmap.values()) {
                if (!w.subSearch(word, p + 1)) {
                    return false;
                }
            }
            return true;
        } else if (!this.cmap.containsKey(word[p])) {
            return false;
        } else {
            if (p + 1 >= word.length) {
                return true;
            } else {
                return cmap.get(word[p]).subSearch(word, p + 1);
            }
        }

    }

    public boolean search(String word) {
        return subSearch(word.toCharArray(), 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (char c : cmap.keySet()) {
            sb.append(c).append("->(").append(cmap.get(c).toString()).append("),");
        }
        return sb.toString();
    }
}
