// https://leetcode.com/problems/additive-number/

public class m_306 {
    public static boolean isAdditiveNumber(String num) {
        char[] nums = num.toCharArray();
        long a = 0, b;
        for (int i = 0; i < nums.length / 2; i++) {
            a = a * 10 + nums[i] - '0';
            for (int j = i + 1; j < nums.length - 1; j++) {
                int nextIndex = j + 1;
                b = 0;
                if (nums[i + 1] == '0') {
                    if(isAdditiveWith(a,0,nextIndex,nums)){
                        return true;
                    }
                    break;
                } else {
                    for (int k = i + 1; k <= j; k++) {
                        b = b * 10 + nums[k] - '0';
                    }
                    if (isAdditiveWith(a, b, nextIndex, nums)) {
                        return true;
                    }
                }
            }
            if (i == 0 && nums[i] == '0') {
                return false;
            }
        }
        return false;
    }

    public static boolean isAdditiveWith(long a, long b, int nextIndex, char[] nums) {
        if (nextIndex >= nums.length) {
            return true;
        }

        if (nums[nextIndex] == '0' && a + b != 0) {
            return false;
        }

        for (char c : String.valueOf(a + b).toCharArray()) {
            if (nextIndex >= nums.length || nums[nextIndex] != c) {
                return false;
            } else {
                nextIndex++;
            }
        }


        return isAdditiveWith(b, a + b, nextIndex, nums);
    }


    public static void main(String[] args) {

        System.out.println(isAdditiveNumber("121474836472147483648"));

        System.out.println(isAdditiveNumber("112358"));         //T
        System.out.println(isAdditiveNumber("199100199"));      //T
        System.out.println(isAdditiveNumber("191029"));         //T
        System.out.println(isAdditiveNumber("1910191"));        //T
        System.out.println(isAdditiveNumber("000"));            //T
        System.out.println(isAdditiveNumber("198019823962"));//T
        System.out.println("---------------------------------------------------------");
        System.out.println(isAdditiveNumber("1910192"));        //F
        System.out.println(isAdditiveNumber("19101910"));       //F
        System.out.println(isAdditiveNumber("0001"));           //F
        System.out.println(isAdditiveNumber("1023"));           //F
        System.out.println(isAdditiveNumber("0235813"));           //F
        System.out.println(isAdditiveNumber("10101"));

    }

}
