//https://leetcode.com/problems/shifting-letters/


public class m_848 {
    public static String shiftingLetters(String s, int[] shifts) {
        int priv = 0;
        char[] cs = s.toCharArray();


        for (int j = shifts.length - 1; j >= 0; j--) {

            priv = (priv + shifts[j]) % 26;
            cs[j] += priv;
            if (cs[j] > 'z') {
                cs[j] -= 26;
            }
        }


        return new String(cs);
    }


    public static void main(String[] args) {


//        System.out.println(shiftingLetters("aaa",new int[]{1,2,3}));
//        System.out.println(shiftingLetters("abc",new int[]{3,5,9}));
        System.out.println(shiftingLetters("abc", new int[]{260003, 260005, 260009}));
    }

}
