//https://leetcode.com/problems/simplify-path/


import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class m_71 {
    public static String simplifyPath(String path) {
        String[] sub = path.split("/");
        String[] named = new String[sub.length];
        int available = 0;
        for (String s : sub) {
            if (s.equals("..")) {
                available--;
                available = Math.max(available, 0);
            } else if (!s.equals(".") && s.length() > 0) {
                named[available] = s;
                available++;
            }
        }
        StringBuilder p = new StringBuilder();
        if(available==0) return "/";
        for(int i=0;i<available;i++){
            p.append("/");
            p.append(named[i]);
        }
        return p.toString();
    }


    public static void main(String[] args) {


        System.out.println(simplifyPath("/a//b////c/d//././/.."));
        System.out.println(simplifyPath("/home/"));
        System.out.println(simplifyPath("/../"));
        System.out.println(simplifyPath("/home//foo/"));
        System.out.println(simplifyPath("/a/./b/../../c/"));
    }

}
