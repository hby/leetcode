//https://leetcode.com/problems/divide-two-integers/

public class m_29 {
    public static int divide(int dividend, int divisor) {
        //may cause overflow
        int pad = 0;
        if (dividend == Integer.MIN_VALUE) {
            if (divisor == -1) {
                return Integer.MAX_VALUE;
            } else if (divisor > 1) {
                pad = -1;
                dividend = dividend + divisor;
            } else if (divisor == 1) {
                return dividend;
            } else if (divisor == Integer.MIN_VALUE) {
                return 1;
            } else {
                pad = 1;
                dividend = dividend - divisor;
            }
        }
        if (divisor == Integer.MIN_VALUE) {
            return 0;
        }

        int s = 0; // s is result

        //Judge neg, only calculate positive / positive
        boolean neg = (dividend > 0) ^ (divisor > 0);
        if (dividend < 0) {
            dividend = -dividend;
        }
        if (divisor < 0) {
            divisor = -divisor;
        }

        // get bit length of dividend and divisor
        int dividendBit = 0, divisorBit = 0;
        int dividendCopy = dividend, divisorCopy = divisor;
        while (dividendCopy != 0) {
            dividendCopy = dividendCopy >> 1;
            dividendBit++;
        }
        while (divisorCopy != 0) {
            divisorCopy = divisorCopy >> 1;
            divisorBit++;
        }

        // do divide calculation
        int diffBits = dividendBit - divisorBit;
        dividendCopy = dividend;
        while (diffBits >= 0) {
            if ((dividendCopy >> (diffBits)) >= divisor) {
                dividendCopy = (((dividendCopy >> diffBits) - divisor) << diffBits) + ((dividendCopy << (32 - diffBits)) >>> (32 - diffBits));
                s = (s << 1) + 1;
            } else {
                s = s << 1;
            }

            dividendBit--;
            diffBits = dividendBit - divisorBit;

        }


        return pad + (neg ? -s : s);
    }


    public static void main(String[] args) {

//        System.out.println(divide(Integer.MIN_VALUE, -1) );
//        System.out.println(Integer.MIN_VALUE / -1);

        System.out.println(divide(10, 1) == (10 / 1));
        System.out.println(divide(10, 3) == (10 / 3));
        System.out.println(divide(7, -3) == (7 / -3));
        System.out.println(divide(7, -1) == (7 / -1));
        System.out.println(divide(-7, -1) == (-7 / -1));
        System.out.println(divide(0, 1) == (0 / 1));
        System.out.println(divide(1, 1) == (1 / 1));
        System.out.println(divide(Integer.MIN_VALUE, 1) == (Integer.MIN_VALUE / 1));
        System.out.println(divide(Integer.MIN_VALUE, 2) == (Integer.MIN_VALUE / 2));
        System.out.println(divide(Integer.MIN_VALUE, -1) == (Integer.MIN_VALUE / -1));
        System.out.println(divide(Integer.MIN_VALUE, Integer.MIN_VALUE) == (Integer.MIN_VALUE / Integer.MIN_VALUE));
        System.out.println(divide(Integer.MIN_VALUE, Integer.MAX_VALUE) == (Integer.MIN_VALUE / Integer.MAX_VALUE));
        System.out.println(divide(Integer.MAX_VALUE, Integer.MIN_VALUE) == (Integer.MAX_VALUE / Integer.MIN_VALUE));
        System.out.println(divide(Integer.MAX_VALUE, Integer.MAX_VALUE) == (Integer.MAX_VALUE / Integer.MAX_VALUE));
        System.out.println(divide(Integer.MIN_VALUE, -2) == (Integer.MIN_VALUE / -2));
        System.out.println(divide(Integer.MAX_VALUE, -1) == (Integer.MAX_VALUE / -1));
    }

}
