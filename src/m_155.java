//https://leetcode.com/problems/min-stack/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class m_155 {
    public static void main(String[] args) {

        MinStack obj = new MinStack();
        obj.push(-2);
        obj.push(0);
        obj.push(-1);
        System.out.println(obj.getMin());
        System.out.println(obj.top());
        obj.pop();
        System.out.println(obj.getMin());


    }

}

class MinStack {
    int[] stack;
    int[] arr;
    int len;


    /**
     * initialize your data structure here.
     */
    public MinStack() {

        this.stack=new int[30000];
        this.arr = new int[30000];
        this.len = 0;
    }

    public void push(int val) {
        this.stack[this.len]=val;

        if (this.len == 0) {
            this.arr[0] = val;
            this.len = 1;
        } else {
            int putTo;
            for (putTo = 0; putTo < len; putTo++) {
                if (val <= arr[putTo]) break;
            }
            if (this.len + 1 - putTo >= 0) System.arraycopy(this.arr, putTo, this.arr, putTo + 1, this.len + 1 - putTo);
            this.arr[putTo] = val;
            this.len++;
        }
    }

    public void pop() {
        if(this.len==1){
            this.len=0;
        }else {
            int removeFrom=Arrays.binarySearch(arr,0,len+1,this.top());
            if (this.len + 1 - removeFrom >= 0)
                System.arraycopy(this.arr, removeFrom + 1, this.arr, removeFrom, this.len + 1 - removeFrom);

            this.len--;
        }
    }

    public int top() {
        return this.stack[this.len-1];
    }

    public int getMin() {
        return this.arr[0];
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */