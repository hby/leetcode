//https://leetcode.com/problems/valid-number/

import java.util.Arrays;

public class m_65 {
    public static boolean isNumber(String s) {
        char[] ss = s.toCharArray();
        int decimal = jumpDecimal(ss, 0);
        int inte = jumpInteger(ss, 0);

        int next = -1;
        if (decimal == ss.length || inte == ss.length) {
            return true;
        } else if (inte == -1 && decimal == -1) {
            return false;
        } else if (inte == -1) {
            next = decimal;
        } else if (decimal == -1) {
            next = inte;
        } else {
            next = decimal;
        }
        if (ss[next] == 'E' || ss[next] == 'e') {
            next = next + 1;
            inte = jumpInteger(ss, next);
            if (inte - next < 1) {
                return false;
            } else {
                return inte == ss.length;
            }
        } else {
            return false;
        }
    }

    public static int jumpDecimal(char[] s, int p) {
        if (p >= s.length) {
            return p;
        } else {
            if (s[p] == '+' || s[p] == '-') {
                p++;
            }
            if (s[p] == '.') {
                p=p+1;
                int next = jumpDigit(s, p);
                if (next - p < 1) {
                    return -1;
                } else {
                    return next;
                }
            } else {
                int next = jumpDigit(s, p);
                if (next - p < 1) {
                    return -1;
                } else {
                    p = next;
                    if (p == s.length) {
                        return p;
                    } else if (s[p] == '.') {
                        p = p + 1;
                        next = jumpDigit(s, p);
                        if (next == -1) {
                            return p;
                        } else {
                            return next;
                        }
                    } else {
                        return -1;
                    }
                }
            }
        }
    }

    public static int jumpDigit(char[] s, int p) {
        if (p >= s.length) {
            return p;
        } else {
            while (p < s.length) {
                if (s[p] < '0' || s[p] > '9') {
                    return p;
                }
                p++;
            }
            return p;
        }
    }

    public static int jumpInteger(char[] s, int p) {
        if(p>=s.length){
            return p;
        }
        if (s[p] == '+' || s[p] == '-') {
            p = p + 1;
        }
        int next = jumpDigit(s, p);
        if (next - p < 1) {
            return -1;
        } else {
            return next;
        }
    }


    public static void main(String[] args) {
        System.out.println(isNumber("."));

        String[] trues = new String[]{"2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789", "3."};
        String[] falses = new String[]{"abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53", ".", "..2"};

        for (String s : trues) {
            System.out.println(isNumber(s) + "----" + s);
        }
        System.out.println("-----------------------");


        for (String s : falses) {
            System.out.println(isNumber(s) + "----" + s);
        }

    }

}
