//https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/


public class m_153 {

    public static int sub(int[] nums, int startIndex, int endIndex) {
        if (startIndex == endIndex) {
            return nums[startIndex];
        } else if (endIndex - startIndex == 1) {
            return Math.min(nums[startIndex], nums[endIndex]);
        } else {
            int cut = startIndex + (endIndex - startIndex) / 2;

            if (nums[startIndex] < nums[cut]) { // 旋转点在 [cut,end] 内，则start -> cut递增
                return sub(nums, cut, endIndex);

//                if (nums[cut] > target && nums[startIndex] < target) { // target 在 start-> cut 递增区间
//                    return subSearch(nums, startIndex, cut, target);
//                } else { // target 在 cut_2 -> end 的不单调区间
//                    return subSearch(nums, cut, endIndex, target);
//                }
//            } else if (nums[startIndex] == nums[cut]) {
//                // 若旋转点在 [start,cut] 上，则start -> cut 不单调
//                // 若旋转点在 [cut,end] 上，则start -> cut 所有值相等
//                // 若旋转点在 [cut,end] 上且target处于[start,cut] 区间，则nums[cut]=target与前面逻辑相悖
//
//                // 故target可能存在于[start,cut]的不单调区间或[cut,end]的不单调区间
//                return subSearch(nums, startIndex, cut, target) || subSearch(nums, cut, endIndex, target);

            } else { // cut_2 -> end 递增   start -> cut_1 不递增
                return sub(nums, startIndex, cut);
//                if (nums[cut] < target && nums[endIndex] > target) { // target 在递增区间
//                    return subSearch(nums, cut, endIndex, target);
//                } else { // target 在不递增区间
//                    return subSearch(nums, startIndex, cut, target);
//                }
            }


        }
    }

    public static int findMin(int[] nums) {
        return sub(nums, 0, nums.length - 1);
    }


    public static void main(String[] args) {


        System.out.println(findMin(new int[]{
                1,2,3,4,5
        }));
    }

}
