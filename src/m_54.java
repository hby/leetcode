//https://leetcode.com/problems/spiral-matrix/


import java.util.ArrayList;
import java.util.List;

public class m_54 {
    public static List<Integer> spiralOrder(int[][] matrix) {
        int cols = matrix[0].length, rows = matrix.length;
        int arrow = 0; // 0:to right  1:to down  2: to left 3: to up
        int layer = 0;
        int r = 0, c = 0; // current row and col
        List<Integer> results = new ArrayList<>(cols * rows);

        for (int i = 0; i < cols * rows; i++) {
            results.add(matrix[r][c]);
            switch (arrow) {
                case 0:
                    if (c + 1 >= cols - layer) {
                        arrow = 1;
                        r++;
                    } else {
                        c++;
                    }
                    break;
                case 1:
                    if (r + 1 >= rows - layer) {
                        arrow = 2;
                        c--;
                    } else {
                        r++;
                    }
                    break;
                case 2:
                    if (c - 1 < layer) {
                        arrow = 3;
                        r--;
                    } else {
                        c--;
                    }
                    break;
                case 3:
                    if (r - 1 <=layer) {
                        arrow = 0;
                        c++;
                        layer++;
                    } else {
                        r--;
                    }
                    break;
            }
        }
        return results;

    }


    public static void main(String[] args) {
        for (int i : spiralOrder(new int[][]{new int[]{1}})) {
//            for (int i : spiralOrder(new int[][]{new int[]{1, 2, 3,4}, new int[]{5,6,7,8}, new int[]{9,10,11,12}})) {
//            for (int i : spiralOrder(new int[][]{new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{7, 8, 9}})) {
            System.out.print(i+",");
        }


    }

}
