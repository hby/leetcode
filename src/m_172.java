//https://leetcode.com/problems/factorial-trailing-zeroes/


public class m_172 {
    public static int trailingZeroes(int n) {
        return n/5+n/25+n/125+n/625+n/3125;
    }


    public static void main(String[] args) {


        System.out.println(trailingZeroes(500));
    }

}
