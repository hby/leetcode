//https://leetcode.com/problems/copy-list-with-random-pointer/


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class m_138 {
    public static Node copyRandomList(Node head) {
        if (head == null) {
            return null;
        }

        Node orig = head;
        Node cloneHead = new Node(head.val);
        Node clone = cloneHead;

        int index = 0;
        orig.val = index;
        orig = orig.next;

        List<Node> clones = new ArrayList<>(1000);
        clones.add(clone);

        while (orig != null) {
            clone.next = new Node(orig.val);
            clones.add(clone.next);
            index++;
            orig.val = index;
            orig = orig.next;
            clone = clone.next;
        }

        orig = head;
        clone = cloneHead;
        while (orig != null) {
            if (orig.random != null) {
                clone.random = clones.get(orig.random.val);
            }
            orig = orig.next;
            clone = clone.next;
        }

        return cloneHead;
    }


    public static void main(String[] args) {
        Node n1 = new Node(7);
        Node n2 = new Node(13);
        Node n3 = new Node(11);
        Node n4 = new Node(10);
        Node n5 = new Node(1);

        n1.next = n2;
        n1.random = null;

        n2.next = n3;
        n2.random = n1;

        n3.next = n4;
        n3.random = n5;

        n4.next = n5;
        n4.random = n3;

        n5.random = n1;

        Node clone = copyRandomList(n1);

        System.out.println("Hello World");
    }

}
