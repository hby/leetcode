//https://leetcode.com/problems/valid-sudoku/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class m_36 {
    public static boolean isValidSudoku(char[][] board) {
        List<Set<Character>> rows = new ArrayList<>();
        List<Set<Character>> cols = new ArrayList<>();
        List<Set<Character>> sqars = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            rows.add(new HashSet<>());
            cols.add(new HashSet<>());
            sqars.add(new HashSet<>());
        }

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                int sqar = row / 3 * 3 + col / 3;
                char c = board[row][col];
                if (c != '.') {
                    if (rows.get(row).contains(c)) {
                        return false;
                    } else {
                        rows.get(row).add(c);
                    }

                    if (cols.get(col).contains(c)) {
                        return false;
                    } else {
                        cols.get(col).add(c);
                    }

                    if (sqars.get(sqar).contains(c)) {
                        return false;
                    } else {
                        sqars.get(sqar).add(c);
                    }

                }
            }
        }
        return true;
    }

    public static boolean isValidSudoku2(char[][] board) {
        int[] rows = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0}, cols = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0}, sqars = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                int sqar = row / 3 * 3 + col / 3;
                char c = board[row][col];
                if (c != '.') {
                    int val = c - '0';
                    if ((rows[row] & (1 << val)) != 0 || (cols[col] & (1 << val)) != 0 || (sqars[sqar] & (1 << val)) != 0) {
                        return false;
                    }
                    rows[row] = rows[row] | (1 << val);
                    cols[col] = cols[col] | (1 << val);
                    sqars[sqar] = sqars[sqar] | (1 << val);
                }
            }
        }

        return true;
    }


    public static void main(String[] args) {
        char[][] boards = new char[][]{
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'},
        };


        System.out.println(isValidSudoku(boards));
        System.out.println(isValidSudoku2(boards));
    }

}
