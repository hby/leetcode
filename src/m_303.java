// https://leetcode.com/problems/range-sum-query-immutable/

public class m_303 {
    static class NumArray {
        int[] sumNums;

        public NumArray(int[] nums) {
            this.sumNums = new int[nums.length+1];

            for(int i=0;i<nums.length;i++){
                this.sumNums[i+1]=this.sumNums[i]+nums[i];
            }

        }

        public int sumRange(int left, int right) {
            return this.sumNums[right+1]-this.sumNums[left];
        }
    }


    public static void main(String[] args) {
        int[] nums = new int[]{-2, 0, 3, -5, 2, -1};
        NumArray obj = new NumArray(nums);

        System.out.println(obj.sumRange(0, 2));
        System.out.println(obj.sumRange(2, 5));
        System.out.println(obj.sumRange(0, 5));
    }

}
