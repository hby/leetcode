// https://leetcode.com/problems/arithmetic-slices/

public class m_413 {
    public static int numberOfArithmeticSlices(int[] nums) {
        if(nums.length<3){
            return 0;
        }

        int[] diff_1 = new int[nums.length - 1];
        for (int i = 0; i < diff_1.length; i++) {
            diff_1[i] = nums[i + 1] - nums[i];
        }

        int[] diff_2 = new int[diff_1.length - 1];
        for (int i = 0; i < diff_2.length; i++) {
            diff_2[i] = diff_1[i + 1] - diff_1[i];
        }

        int count = 0;

        int zeroFrom = -1;
        for (int i = 0; i < diff_2.length; i++) {
            if (diff_2[i] == 0) {
                if (zeroFrom == -1) {
                    zeroFrom = i;
                }
            } else {
                if (zeroFrom != -1) {
                    int d = i - zeroFrom;
                    count = count + (d + 1) * d / 2;
                    zeroFrom = -1;
                }
            }
        }
        if (zeroFrom != -1) {
            int d = diff_2.length - zeroFrom;
            count = count + (d + 1) * d / 2;
        }
        return count;
    }


    public static void main(String[] args) {


        System.out.println(numberOfArithmeticSlices(new int[]{1}));
        System.out.println(numberOfArithmeticSlices(new int[]{1, 2, 3}));
        System.out.println(numberOfArithmeticSlices(new int[]{1, 2, 3,4}));
        System.out.println(numberOfArithmeticSlices(new int[]{1, 2, 3, 4, 5, 7, 8, 9}));
    }

}
