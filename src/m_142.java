//https://leetcode.com/problems/linked-list-cycle-ii/


import java.util.HashSet;
import java.util.Set;

public class m_142 {
    public static ListNode detectCycle(ListNode head) {
        //copy of https://leetcode.com/problems/linked-list-cycle-ii/discuss/44793/O(n)-solution-by-using-two-pointers-without-change-anything
        ListNode first = head, second = head;
        boolean isCycle = false;
        while (first != null && second != null) {
            first = first.next;
            if (second.next == null) {
                return null;
            }
            second = second.next.next;
            if (first == second) {
                isCycle = true;
                break;
            }
        }
        if (!isCycle) {
            return null;
        }
        first = head;
        while (first != second) {
            first = first.next;
            second = second.next;
        }


        return first;
    }


    public static void main(String[] args) {
        ListNode n = new ListNode(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        n.next.next.next.next.next.next.next.next.next.next = n.next.next;


        System.out.println(detectCycle(n));
    }

}
