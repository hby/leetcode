//https://leetcode.com/problems/binary-tree-paths/

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class m_257 {
    public static List<String> binaryTreePaths(TreeNode root) {

        if (root == null) {
            return new LinkedList<>();
        } else if (root.left == null && root.right == null) {
            return new LinkedList<>() {{
                add(String.valueOf(root.val));
            }};
        } else if (root.left == null) {
            List<String> sub = binaryTreePaths(root.right);
            return sub.stream().map(s->root.val + "->" + s).collect(Collectors.toList());
        } else {
            List<String> sub = binaryTreePaths(root.left);
            sub.addAll(binaryTreePaths(root.right));
            return sub.stream().map(s->root.val + "->" + s).collect(Collectors.toList());
        }

    }


    public static void main(String[] args) {


        System.out.println(binaryTreePaths(TreeNode.gen(1, 2, 3, null, 5)));
        System.out.println(binaryTreePaths(TreeNode.gen(1)));
    }

}
