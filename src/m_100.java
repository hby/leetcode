//https://leetcode.com/problems/same-tree/


public class m_100 {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if(p==null||q==null){
            return p==null&&q==null;
        }else{
            return isSameTree(p.left,q.left)&&isSameTree(p.right,q.right)&&p.val==q.val;
        }

    }


    public static void main(String[] args){



        System.out.println("Hello World");
    }

}
