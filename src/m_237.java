// https://leetcode.com/problems/delete-node-in-a-linked-list/


public class m_237 {
    public static void deleteNode(ListNode node) {
        ListNode next = node.next;

        node.next = next.next;
        node.val = next.val;
    }


    public static void main(String[] args) {


        System.out.println("Hello World");
    }

}
