//https://leetcode.com/problems/roman-to-integer/
public class m_13 {


    public static int romanToInt(String s) {
        int len = s.length();
        int r = 0;
        for (int i = len - 1; i >= 0; i--) {
            switch (s.charAt(i)) {
                case 'I':
                    if (r >= 5) {
                        r = r - 1;
                    } else {
                        r = r + 1;
                    }
                    break;
                case 'V':
                    r = r + 5;
                    break;
                case 'X':
                    if (r >= 50) {
                        r = r - 10;
                    } else {
                        r = r + 10;
                    }
                    break;
                case 'L':
                    r = r + 50;
                    break;
                case 'C':
                    if (r >= 500) {
                        r = r - 100;
                    } else {
                        r = r + 100;
                    }
                    break;
                case 'D':
                    r = r + 500;
                    break;
                case 'M':
                    r = r + 1000;
                    break;
            }
        }

        return r;
    }

    public static void main(String[] args) {
        for(int i=1;i<=3999;i++){
            if(i!=romanToInt(m_12.intToRoman(i))){
                System.out.println(i);
            }
        }
    }
}
