//https://leetcode.com/problems/merge-two-sorted-lists/

public class m_21 {
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        } else if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        } else {
            if (l1.val > l2.val) {
                return new ListNode(l2.val, mergeTwoLists(l1, l2.next));
            } else {
                return new ListNode(l1.val, mergeTwoLists(l1.next, l2));
            }
        }
    }


    public static void main(String[] args) {


        System.out.println(mergeTwoLists(
                new ListNode(new int[]{}),
                new ListNode(new int[]{})
        ));
    }

}
