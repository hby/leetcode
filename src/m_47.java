//https://leetcode.com/problems/permutations-ii/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_47 {
    public static void swap(int[] nums, int a, int b) {
        if (a != b) {
            nums[a] = nums[a] + nums[b];
            nums[b] = nums[a] - nums[b];
            nums[a] = nums[a] - nums[b];
        }
    }

    public static void sub(int[] nums, int start, List<List<Integer>> arr, double[] candicates) {
        if (start == nums.length - 1) {
            List<Integer> x = new ArrayList<>(nums.length);
            double candicate = 0;
            int i = 0;
            for (int num : nums) {
                x.add(num);
                num += 10;
                candicate += num << (5 * i);
                i++;
            }
            for (i = 0; i < candicates.length; i++) {
                if (candicates[i] == 0) {
                    arr.add(x);
                    candicates[i] = candicate;
                    break;
                } else if (candicates[i] == candicate) {
                    break;
                }
            }
        } else {
            for (int i = start; i < nums.length; i++) {
                swap(nums, i, start);
                sub(nums, start + 1, arr, candicates);
                swap(nums, start, i);
            }
        }
    }

    public static List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> arr = new LinkedList<>();
        int possibles = 1;
        for (int i = 1; i <= nums.length; i++) {
            possibles *= i;
        }
        double[] candicates = new double[possibles];
        sub(nums, 0, arr, candicates);
        return arr;
    }


    public static void main(String[] args) {
        int[] nums = new int[]{1, 1, 2};
        List<List<Integer>> x = permuteUnique(nums);

        System.out.print("[");
        for (List<Integer> c : x) {
            System.out.print("[");
            for (int i : c) {
                System.out.print(i + ",");
            }
            System.out.print("],");
        }
        System.out.print("]");
        System.out.println("");


    }

}
