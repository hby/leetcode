//https://leetcode.com/problems/minimum-path-sum/


public class m_64 {
    public static int minPathSum(int[][] grid) {
        int x = grid.length;
        int y = grid[0].length;

        int[][] sum = new int[x][y];
        for (int i = x - 1; i >= 0; i--) {
            for (int j = y - 1; j >= 0; j--) {
                if (i == x - 1 && j == y - 1) {
                    sum[i][j] = grid[i][j];
                } else if (i == x - 1) {
                    sum[i][j] = grid[i][j] + sum[i][j + 1];
                } else if (j == y - 1) {
                    sum[i][j] = grid[i][j] + sum[i + 1][j];
                } else {
                    sum[i][j] = grid[i][j] + Math.min(sum[i + 1][j], sum[i][j + 1]);
                }
            }
        }
        return sum[0][0];
    }


    public static void main(String[] args) {


        System.out.println(minPathSum(new int[][]{
                new int[]{1, 2, 3},
                new int[]{4, 5, 6},
        }));
    }

}
