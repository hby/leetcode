//https://leetcode.com/problems/house-robber/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class m_198 {
    public static int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        } else if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        } else {
            int[] cache = new int[nums.length];
            Arrays.fill(cache, -1);
            cache[0] = nums[0];
            cache[1] = Math.max(nums[0], nums[1]);
            return robUntil(nums, nums.length - 1, cache);
        }

    }

    public static int robUntil(int[] nums, int p, int[] cache) {
        if (cache[p] != -1) {
            return cache[p];
        } else {
            int u = Math.max(robUntil(nums, p - 1, cache), robUntil(nums, p - 2, cache) + nums[p]);
            cache[p] = u;
            return u;
        }


    }


    public static void main(String[] args) {


        System.out.println(rob(new int[]{100, 0, 0, 100}));
//        System.out.println(rob(new int[]{1, 2, 3, 1}));
//        System.out.println(rob(new int[]{2, 7, 9, 3, 1}));
    }

}
