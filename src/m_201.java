//https://leetcode.com/problems/bitwise-and-of-numbers-range/


public class m_201 {
    public static int highestBit(int n) { // 求n的最高比特位，相当于 log2(n) 的整数部分
        for (int i = 31; i >= 0; i--) {
            if ((n << (31 - i)) >>> 31 == 1) {
                return i;
            }
        }
        return 0;
    }

    public static int rangeBitwiseAnd(int left, int right) {
        int hl = highestBit(left), hr = highestBit(right);
        if (hl != hr) {
            return 0;
        } else if (hl == 0) {
            int p = -1;
            for (int i = left; i <= right; i++) {
                p &= i;
            }
            return p;
        } else {
            int k = 1 << hl;
            return k | rangeBitwiseAnd(left - k, right - k);
        }

    }


    public static void main(String[] args) {
        System.out.println(rangeBitwiseAnd(1, 1));
//        System.out.println(rangeBitwiseAnd(1, 2147483647));
    }

}
