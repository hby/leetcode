//https://leetcode.com/problems/gray-code/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_89 {
    public static List<Integer> grayCode(int n) {
        Integer[] x = new Integer[1 << n];
        x[0]=0;
        for (int i = 0; i < n; i++) {
            int m = 1 << i;
            for (int j = 0; j < m; j++) {
                x[m + j] = m + x[m - 1 - j];
            }
        }

        return Arrays.asList(x);
    }


    public static void main(String[] args) {

        System.out.println(Arrays.toString(grayCode(5).toArray()));
    }

}
