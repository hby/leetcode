//https://leetcode.com/problems/counting-bits/submissions/


import java.util.Arrays;

public class m_338 {


    public static int[] countBits(int num) {
        int[] p = new int[num + 1];
        p[0] = 0;
        if (num >= 1) {
            p[1] = 1;
        }

        int high = 1;
        for (int i = 2; i <= num; i++) {
            if (i == (1 << (high + 1))) {
                high++;
            }
            p[i] = 1 + p[i - (1 << high)];
        }

        return p;
    }


    public static void main(String[] args) {
        System.out.println(Arrays.toString(countBits(10)));


//        System.out.println(Arrays.toString(countBits(85723)));
    }

}
