//https://leetcode.com/problems/minimum-depth-of-binary-tree/

public class m_111 {
    public static int getMinHeight(TreeNode root) {
        if (root == null) {
            return 0;
        } else if (root.left == null && root.right == null) {
            return 1;
        } else if (root.left == null || root.right == null) {
            int singleHeight;
            if (root.left == null) {
                singleHeight = getMinHeight(root.right);
            } else {
                singleHeight = getMinHeight(root.left);
            }
            return singleHeight+1;

        } else {
            int leftHeight = getMinHeight(root.left);
            int rightHeight = getMinHeight(root.right);
            return Math.min(leftHeight,rightHeight)+1;

        }
    }

    public static int minDepth(TreeNode root) {
        return getMinHeight(root);
    }


    public static void main(String[] args){



        System.out.println("Hello World");
    }

}
