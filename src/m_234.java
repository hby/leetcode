// https://leetcode.com/problems/palindrome-linked-list/


import java.util.LinkedList;
import java.util.Objects;

public class m_234 {
    public static boolean isPalindrome(ListNode head) {

        LinkedList<Integer> l = new LinkedList<>();

        ListNode p = head;
        while (p != null) {
            l.add(p.val);
            p = p.next;
        }

        while (l.size() >= 2) {
            if (!Objects.equals(l.pollFirst(), l.pollLast())) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {


        System.out.println(isPalindrome(new ListNode(1, 2, 2, 1)));
    }

}
