//https://leetcode.com/problems/intersection-of-two-linked-lists/


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class m_160 {

    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        int aLen = 0;

        ListNode a = headA;
        while (a != null) {
            aLen++;
            a = a.next;
        }

        int[] aset = new int[aLen];
        a = headA;
        for (int i = 0; i < aLen; i++) {
            aset[i] = System.identityHashCode(a);
            a = a.next;
        }
        Arrays.sort(aset);

        a = headB;
        while ((Arrays.binarySearch(aset, System.identityHashCode(a)) <0) && a != null) {
            System.out.println(Arrays.binarySearch(aset, System.identityHashCode(a)));
            a = a.next;
        }
        return a;
    }


    public static void main(String[] args) {
        ListNode tog = new ListNode(8, 4, 5);
        ListNode a = new ListNode(4, new ListNode(1, tog));
        ListNode b = new ListNode(5, new ListNode(6, new ListNode(1, tog)));


        System.out.println(getIntersectionNode(a, b));
    }

}
