//https://leetcode.com/problems/missing-number/

public class m_268 {
    public static int missingNumber(int[] nums) {
        boolean[] checked = new boolean[nums.length+1];
        for(int num:nums){
            checked[num]=true;
        }
        for(int i=0;i< checked.length;i++){
            if(!checked[i]){
                return i;
            }
        }

        return 0;
    }


    public static void main(String[] args) {


        System.out.println(missingNumber(new int[]{9,6,4,2,3,5,7,0,1}));
    }

}
