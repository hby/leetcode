//https://leetcode.com/problems/plus-one/


public class m_66 {
    public static int[] plusOne(int[] digits) {
        int borrow = 0;
        for (int i = digits.length - 1; i >= 0; i--) {
            if (i == digits.length - 1) {
                digits[i] = digits[i] + 1;
            }

            if (digits[i] + borrow >= 10) {
                digits[i] = (digits[i] + borrow) % 10;
                borrow = 1;
            } else {
                digits[i] = digits[i] + borrow;
                borrow = 0;
                break;
            }
        }
        if (borrow == 1) {
            int[] r = new int[digits.length + 1];
            r[0] = 1;
            System.arraycopy(digits, 0, r, 1, digits.length);
            return r;
        } else {
            return digits;
        }
    }


    public static void main(String[] args) {

        System.out.print("[");
        for (int i : plusOne(new int[]{0})) {
            System.out.print(i + ",");
        }
        System.out.print("]");
    }

}
