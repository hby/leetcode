//https://leetcode.com/problems/search-in-rotated-sorted-array

public class m_33 {
    public static int subSearch(int[] nums, int startIndex, int endIndex, int target) {
        if (endIndex - startIndex <= 1) {
            if (nums[endIndex] == target) {
                return endIndex;
            } else if (nums[startIndex] == target) {
                return startIndex;
            } else {
                return -1;
            }
        } else {
            int cut = (endIndex - startIndex) / 2 + startIndex;
            if (nums[cut] > nums[startIndex]) { // start -> cut单调， cut -> end 不单调
                if (nums[cut] >= target && nums[startIndex] <= target) { // target 在 start-> cut 单调区间
                    return subSearch(nums, startIndex, cut, target);
                } else { // target 在 cut -> end 的不单调区间
                    return subSearch(nums, cut, endIndex, target);
                }
            } else { // cut -> end 单调   start -> cut不单调
                if (nums[cut] <= target && nums[endIndex] >= target) { // target 在单调区间
                    return subSearch(nums, cut, endIndex, target);
                } else { // target 在不单调区间
                    return subSearch(nums, startIndex, cut, target);
                }
            }
        }


    }

    public static int search(int[] nums, int target) {
        return subSearch(nums, 0, nums.length - 1, target);
    }


    public static void main(String[] args) {
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 4));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 5));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 6));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 7));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 0));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 1));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 2));
        System.out.println(search(new int[]{3,4, 5, 6, 7, 0, 1, 2}, 3));
        System.out.println(search(new int[]{3}, 3));
        System.out.println(search(new int[]{3}, 4));
    }

}
