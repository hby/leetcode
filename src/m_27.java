//https://leetcode.com/problems/remove-element/

public class m_27 {
    public static int removeElement(int[] nums, int val) {
        if (nums.length == 0) {
            return 0;
        }
        int rightP = nums.length - 1;
        while (rightP >= 0 && val == nums[rightP]) {
            rightP--;
        }
        if(rightP==-1){
            return 0;
        }
        int i;
        for (i = 0; i < rightP; i++) {
            if (nums[i] == val) {
                nums[i] = nums[rightP];
                nums[rightP] = val;
                while (rightP >= 0 && val == nums[rightP]) {
                    rightP--;
                }
            }
        }


        return rightP+1;


    }


    public static void main(String[] args) {
        int[] nums = new int[]{1};
        int val = 2;
//        int[] nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

        System.out.println(removeElement(nums, val));
        for (int num : nums) {
            System.out.print(num + ",");
        }
        System.out.println("");


    }

}
