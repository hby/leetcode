//https://leetcode.com/problems/unique-paths-ii/


import java.util.HashMap;
import java.util.Map;

public class m_63 {
    public static Map<Integer, Integer> cache = new HashMap<>();


    public static int uniquePathsWithObstacles(int[][] obstacleGrid, int x, int y) {
        if (obstacleGrid[x][y] == 1) {
            return 0;
        }
        if(x==obstacleGrid.length-1){
            for(int o:obstacleGrid[x]){
                if(o==1){
                    return 0;
                }
            }
            return 1;
        }else if(y == obstacleGrid[0].length - 1){
            for(int[] o:obstacleGrid){
                if(o[y]==1){
                    return 0;
                }
            }
            return 1;
        }else {
            int xx = x + 1, yy = y + 1;

            int key = xx * 1000 + y;
            int p1, p2;

            if (cache.containsKey(key)) {
                p1 = cache.get(key);
            } else {
                p1 = uniquePathsWithObstacles(obstacleGrid, xx, y);
                cache.put(key, p1);
            }

            key = x * 1000 + yy;
            if (cache.containsKey(key)) {
                p2 = cache.get(key);
            } else {
                p2 = uniquePathsWithObstacles(obstacleGrid, x, yy);
                cache.put(key, p2);
            }
            return p1 + p2;
        }


    }


    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        return uniquePathsWithObstacles(obstacleGrid, 0, 0);
    }


    public static void main(String[] args) {


        System.out.println(uniquePathsWithObstacles(new int[][]{
                new int[]{0, 1},
                new int[]{0, 0},
        }));
    }

}
