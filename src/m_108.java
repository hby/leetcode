//https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/


public class m_108 {
    public static TreeNode sortedArrayToBST(int[] nums) {
        return sub(nums, 0, nums.length - 1);
    }

    public static TreeNode sub(int[] nums, int start, int end) {
        if (end - start < 0) {
            return null;
        } else {
            int mid = (end - start) / 2 + start;
            return new TreeNode(nums[mid], sub(nums, start, mid - 1), sub(nums, mid + 1, end));
        }
    }


    public static void main(String[] args) {

        TreeNode t1 = sortedArrayToBST(new int[]{0, 1, 2, 3, 4, 5});
        TreeNode t2 = sortedArrayToBST(new int[]{1, 2, 3, 4, 5, 6, 7});


        System.out.println("Hello World");
    }

}
