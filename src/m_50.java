//https://leetcode.com/problems/powx-n/


import java.util.HashMap;
import java.util.Map;

public class m_50 {


    public static double subPow(double x, int n, Map<Integer, Double> cached) {
        if (cached.containsKey(n)) {
            return cached.get(n);
        }
        if (n == 0) {
            return 1;
        } else if (n == 1) {
            return x;
        } else {
            int m = n / 2;
            int p = n - m;
            double my = subPow(x, m, cached);
            double mp = subPow(x, p, cached);
            cached.put(m, my);
            cached.put(p, mp);
            return my * mp;
        }

    }

    public static double myPow(double x, int n) {
        double overflow = n == Integer.MIN_VALUE ? 1 / x : 1;
        n = n == Integer.MIN_VALUE ? n + 1 : n;
        boolean isNeg = n < 0;
        n = isNeg ? -n : n;
        Map<Integer, Double> cache = new HashMap<>();
        double y = subPow(x, n, cache);

        return overflow * (isNeg ? 1 / y : y);


    }


    public static void main(String[] args) {


        System.out.println(myPow(1.0, 2147483647));
//        System.out.println(myPow(2.0, 10));
//        System.out.println(myPow(2.1, 3));
//        System.out.println(myPow(2.0, -2));
//        System.out.println(myPow(2.0, 0));
//        System.out.println(myPow(-2.0, 0));
    }

}
