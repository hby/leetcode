//https://leetcode.com/problems/next-permutation

//TODO ????

import java.util.Arrays;

public class m_31 {
    public static void nextPermutation(int[] nums) {


        int p = nums.length - 1;
        while (p >= 1) {
            if (nums[p] > nums[p - 1]) {
                nums[p] = nums[p] + nums[p - 1];
                nums[p - 1] = nums[p] - nums[p - 1];
                nums[p] = nums[p] - nums[p - 1];
                break;
            }
            p--;
        }

        if (p == 0) {
            Arrays.sort(nums);
        } else {
            while (p < nums.length - 1) {
                if (nums[p] > nums[p + 1]) {
                    nums[p] = nums[p] + nums[p + 1];
                    nums[p + 1] = nums[p] - nums[p + 1];
                    nums[p] = nums[p] - nums[p + 1];
                    break;
                }
                p++;
            }
        }


    }


    public static void main(String[] args) {
        int[] arr = new int[]{3, 2, 1};
        nextPermutation(arr);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ",");
        }
        System.out.println();
    }

}
