//https://leetcode.com/problems/permutations/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_46 {
    public static void swap(int[] nums, int a, int b) {
        if(a!=b){
            nums[a] = nums[a] + nums[b];
            nums[b] = nums[a] - nums[b];
            nums[a] = nums[a] - nums[b];
        }
    }

    public static void sub(int[] nums, int start, List<List<Integer>> arr) {
        if (start == nums.length - 1) {
            List<Integer> x = new ArrayList<>(nums.length);
            for (int num : nums) {
                x.add(num);
            }
            arr.add(x);
        } else {
            for (int i = start; i < nums.length; i++) {
                swap(nums, i, start);
                sub(nums, start+1, arr);
                swap(nums, start, i);
            }
        }
    }

    public static List<List<Integer>> permute(int[] nums) {
        int numLen = nums.length;
        List<List<Integer>> arr = new LinkedList<>();
        sub(nums, 0, arr);


        return arr;
    }


    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5,6};
        List<List<Integer>> x = permute(nums);

        System.out.print("[");
        for(List<Integer> c:x){
            System.out.print("[");
            for(int i:c){
                System.out.print(i+",");
            }
            System.out.print("],");
        }
        System.out.print("]");
        System.out.println("");


    }

}
