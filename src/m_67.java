//https://leetcode.com/problems/add-binary/


public class m_67 {
    public static String addBinary(String a, String b) {
        char[] as = a.toCharArray();
        char[] bs = b.toCharArray();
        char[] cs;

        if (as.length < bs.length) {
            cs = as;
            as = bs;
            bs = cs;
        }
        int aLen = as.length, bLen = bs.length;
        cs = new char[aLen + 1];

        int p = 0;
        boolean borrow = false;
        while (p <= bLen - 1) {
            char ac = as[aLen - 1 - p], bc = bs[bLen - 1 - p];
            if (ac == '0' && bc == '0') {
                if (borrow) {
                    cs[p] = '1';
                    borrow = false;
                } else {
                    cs[p] = '0';
                    borrow = false;
                }
            } else if (ac == '0' && bc == '1') {
                if (borrow) {
                    cs[p] = '0';
                    borrow = true;
                } else {
                    cs[p] = '1';
                    borrow = false;
                }
            } else if (ac == '1' && bc == '0') {
                if (borrow) {
                    cs[p] = '0';
                    borrow = true;
                } else {
                    cs[p] = '1';
                    borrow = false;
                }
            } else if (ac == '1' && bc == '1') {
                if (borrow) {
                    cs[p] = '1';
                    borrow = true;
                } else {
                    cs[p] = '0';
                    borrow = true;
                }
            }

            p++;
        }

        while (p <= aLen - 1) {
            char ac = as[aLen - 1 - p];
            if (ac == '0') {
                if (borrow) {
                    cs[p] = '1';
                    borrow = false;
                } else {
                    cs[p] = '0';
                    borrow = false;
                }
            } else if (ac == '1') {
                if (borrow) {
                    cs[p] = '0';
                    borrow = true;
                } else {
                    cs[p] = '1';
                    borrow = false;
                }
            }
            p++;
        }

        StringBuilder sb = new StringBuilder();

        if(borrow){
            sb.append("1");
        }
        for(int i=p-1;i>=0;i--){
            sb.append(cs[i]);
        }
        return sb.toString();


    }


    public static void main(String[] args) {


//        System.out.println(addBinary("0", "0"));
        System.out.println(addBinary("11", "1"));
//        System.out.println(addBinary("1010", "0"));
//        System.out.println(addBinary("1010", "1011"));
//        System.out.println(addBinary("1111", "1111"));
    }

}
