//https://leetcode.com/problems/unique-paths/


import java.util.HashMap;
import java.util.Map;

public class m_62 {
    public static Map<Integer,Integer> cache = new HashMap<>();
    public static int uniquePaths(int m, int n) {
        if (m == 1 || n == 1) {
            return 1;
        } else {
            int mm = m-1,nn = n-1;

            int larger = Math.max(m-1,n),smaller = Math.min(m-1,n);
            int key = larger*1000+smaller;
            int p1,p2;

            if(cache.containsKey(key)){
                p1 = cache.get(key);
            }else {
                p1 = uniquePaths(m - 1, n);
                cache.put(key,p1);
            }

            larger = Math.max(m,n-1);
            smaller = Math.min(m,n-1);
            key = larger*1000+smaller;

            if(cache.containsKey(key)){
                p2 = cache.get(key);
            }else {
                p2 = uniquePaths(m , n-1);
                cache.put(key,p2);
            }


            return p1+p2;
        }
    }


    public static void main(String[] args) {


        System.out.println(uniquePaths(51, 9));
    }

}
