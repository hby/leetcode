//https://leetcode.com/problems/n-queens/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_51 {
    public static boolean check(int[] fit, int n, int pointer) {

        for (int f : fit) {
            if (f == 0) {
                return true;
            } else {
                int p = f - 1;
                if ((pointer / n == p / n) //同一条横线上
                        || (pointer % n == p % n) //同一条竖线上
                        || ((pointer / n - p / n) == (pointer % n - p % n))
                        || ((pointer / n - p / n) == (p % n - pointer % n))) {//同一条斜线上（即两个点之间斜率为正负1）
                    return false;
                }
            }
        }
        return true;
    }

    public static void subSolve(int n, int[] fit, int start, int remain, List<List<String>> res) {
        if (remain == 0) {
            int fp = 0;
            List<String> r = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < n; j++) {
                    int pp = fp < n ? fit[fp] - 1 : n * n;
                    if (i == pp / n && j == pp % n) {
                        sb.append('Q');
                        fp++;
                    } else {
                        sb.append('.');
                    }
                }
                r.add(sb.toString());
            }
            res.add(r);
            return;
        }

        for (int i = start; i < n * n; i++) {
            if (check(fit, n, i)) {
                fit[n - remain] = i + 1;
                subSolve(n, fit, i + 1, remain - 1, res);
                fit[n - remain] = 0;
            }
        }
    }


    public static List<List<String>> solveNQueens(int n) {
        List<List<String>> res = new LinkedList<>();

        for (int first = 0; first < n * n; first++) {
            int[] fit = new int[n];
            fit[0] = first + 1;
            subSolve(n, fit, first, n-1, res);
            if (fit[n - 1] != 0) {
                System.out.println(Arrays.toString(fit));
            }
        }

        return res;
    }


    public static void main(String[] args) {


        List<List<String>> arr = solveNQueens(9);


        for (List<String> ar : arr) {
            System.out.print("[");
            for (String a : ar) {
                System.out.print(a + ",");
            }
            System.out.println("],");
        }

    }

}
