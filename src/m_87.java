//https://leetcode.com/problems/scramble-string/


import java.util.Arrays;
import java.util.stream.IntStream;

public class m_87 {
    public static boolean containsAll(byte[] s1c, byte[] s2c) {
        if (s1c.length != s2c.length) {
            return false;
        } else if (s1c.length == 0) {
            return true;
        }

        int i = s1c.length;
        int counter = 0;
        for (byte b : s1c) {
            for (int k = 0; k < i; k++) {
                if (s2c[k] == b) {
                    counter++;
                    s2c[k] = 0;
                    break;
                }
            }
        }
        return counter == i;
    }

    public static int[][] findCut(String s1, String s2) {
        int len = s1.length();
        int found = 0;
        int[][] cutPositions = new int[len][3];
        for (int i = 1; i < len; i++) {
            byte[] s1c = Arrays.copyOfRange(s1.getBytes(), 0, i);
            byte[] s2c_1 = Arrays.copyOfRange(s2.getBytes(), 0, i);
            byte[] s2c_2 = Arrays.copyOfRange(s2.getBytes(), len - i, len);
            if (containsAll(s1c, s2c_1)) {
                cutPositions[found] = new int[]{i, i, 0}; //0:正向切割
                found++;
            } else if (containsAll(s1c, s2c_2)) {
                cutPositions[found] = new int[]{i, len - i, 1};//1:反向切割
                found++;
            }
        }
        return Arrays.copyOfRange(cutPositions, 0, found);
    }

    public static boolean isScramble(String s1, String s2) {
        int len = s1.length();
        if (len <= 1) {
            return s1.equals(s2);
        } else if (s1.equals(s2)) {
            return true;
        } else {
            int[][] cutPositions = findCut(s1, s2);

            for (int[] i : cutPositions) {
                String s1l = s1.substring(0, i[0]), s1r = s1.substring(i[0], len);
                String s2l = s2.substring(0, i[1]), s2r = s2.substring(i[1], len);

                if (i[2] == 1) {
                    if (isScramble(s1l, s2r) && isScramble(s1r, s2l)) {
                        return true;
                    }
                } else {
                    if (isScramble(s1l, s2l) && isScramble(s1r, s2r)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println(isScramble("great", "rgeat"));  //T
        System.out.println(isScramble("greataa", "rgeataa"));//T
        System.out.println(isScramble("abcde", "caebd"));//F
        System.out.println(isScramble("a", "a"));//T
        System.out.println(isScramble("abc", "cba"));//T
        System.out.println(isScramble("aa", "ab"));//F
        System.out.println(isScramble("abcdefghijklmnopq", "efghijklmnopqcadb"));//F
        System.out.println(isScramble("oatzzffqpnwcxhejzjsnpmkmzngneo", "acegneonzmkmpnsjzjhxwnpqffzzto"));//T

    }

}
