//https://leetcode.com/problems/excel-sheet-column-title/


public class m_168 {
    public static String convertToTitle(int columnNumber) {
        StringBuilder sb = new StringBuilder();
        while (columnNumber > 0) {
            int mod = columnNumber % 26;
            mod = mod == 0 ? 26 : mod;
            sb.append((char) ('A' - 1 + mod));
            columnNumber -= mod;
            columnNumber /= 26;
        }

        return sb.reverse().toString();
    }


    public static void main(String[] args) {


        System.out.println(convertToTitle(1));
        System.out.println(convertToTitle(26));
        System.out.println(convertToTitle(28));
        System.out.println(convertToTitle(701));
        System.out.println(convertToTitle(2147483647));
    }

}
