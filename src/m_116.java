//https://leetcode.com/problems/populating-next-right-pointers-in-each-node/


public class m_116 {


    public static Node connect(Node root) {
        if (root == null) {
            return null;
        }

        if (root.left != null && root.right != null) {

            root.left.next = root.right;
            if (root.next == null) {
                root.right.next = null;
            } else {
                root.right.next = root.next.left;
            }
            connect(root.left);
            connect(root.right);
        }


        return root;
    }

//    public static Node connect(Node root) {
//        return sub(root);
//    }


    public static void main(String[] args) {

        Node node = new Node(1,
                new Node(2, new Node(4), new Node(5), null),
                new Node(3, new Node(6), new Node(7), null),
                null);
        connect(node);

        System.out.println("Hello World");
    }

}
