//https://leetcode.com/problems/maximum-depth-of-binary-tree/


public class m_104 {
    public static int maxDepth(TreeNode root) {
        if(root==null){
            return 0;
        }else {
            return 1+Math.max(maxDepth(root.left),maxDepth((root.right)));
        }
    }


    public static void main(String[] args){



        System.out.println("Hello World");
    }

}
