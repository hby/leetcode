//https://leetcode.com/problems/edit-distance/


import java.util.Arrays;

public class m_72 {


    public static int minDistance(String word1, String word2) {
        int lena = word1.length(), lenb = word2.length();
        int[][] dp = new int[lena + 1][lenb + 1];
        char[] a = word1.toCharArray(), b = word2.toCharArray();

        for (int i = 1; i <= lena; i++) dp[i][0] = i;
        for (int j = 1; j <= lenb; j++) dp[0][j] = j;
        for (int i = 1; i <= lena; i++)
            for (int j = 1; j <= lenb; j++)
                if (a[i - 1] == b[j - 1])
                    dp[i][j] = dp[i - 1][j - 1];
                else
                    dp[i][j] = Math.min(dp[i - 1][j - 1], Math.min(dp[i][j - 1], dp[i - 1][j])) + 1;

        return dp[lena][lenb];

    }


    public static void main(String[] args) {


        System.out.println(minDistance("intention", "execution"));
    }

}
