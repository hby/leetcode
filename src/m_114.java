//https://leetcode.com/problems/flatten-binary-tree-to-linked-list/


public class m_114 {
    public static TreeNode sub(TreeNode root) {
        if (root != null) {
            if (root.left != null && root.right != null) {
                TreeNode flattenLeft = sub(root.left);
                TreeNode flattenRight = sub(root.right);


                flattenLeft.right = root.right;
                root.right = root.left;
                root.left = null;
                return flattenRight;
            } else if (root.left != null) {
                TreeNode flattenLeft = sub(root.left);
                root.right = root.left;
                root.left = null;
                return flattenLeft;
            } else if(root.right!=null){
                return sub(root.right);
            }else {
                return root;
            }
        } else {
            return null;
        }
    }

    public static void flatten(TreeNode root) {
        sub(root);
    }


    public static void main(String[] args) {
        TreeNode x = TreeNode.gen(1,2,5,3,4,null,6);
        flatten(x);

        System.out.println("Hello World");
    }

}
