//https://leetcode.com/problems/interleaving-string/


import java.util.HashMap;
import java.util.Map;

public class m_97 {

    public static boolean isInterleave(char[] cs1, int start1,
                                       char[] cs2, int start2,
                                       char[] cs3, int start3,
                                       Map<Integer, Boolean> cache) {
        int key = start1 * 1000 + start2;
        if (cache.containsKey(key)) {
            return cache.get(key);
        }
        boolean b;

        if (start1 == cs1.length && start2 == cs2.length) {
            b = true;
        } else if (start1 == cs1.length) {
            b = cs2[start2] == cs3[start3] && isInterleave(cs1, start1, cs2, start2 + 1, cs3, start3 + 1, cache);
        } else if (start2 == cs2.length) {
            b = cs1[start1] == cs3[start3] && isInterleave(cs1, start1 + 1, cs2, start2, cs3, start3 + 1, cache);
        } else {
            if (cs1[start1] == cs3[start3] && cs2[start2] == cs3[start3]) {
                b = isInterleave(cs1, start1 + 1, cs2, start2, cs3, start3 + 1, cache)
                        || isInterleave(cs1, start1, cs2, start2 + 1, cs3, start3 + 1, cache);
            } else if (cs1[start1] == cs3[start3] && cs2[start2] != cs3[start3]) {
                b = isInterleave(cs1, start1 + 1, cs2, start2, cs3, start3 + 1, cache);
            } else if (cs1[start1] != cs3[start3] && cs2[start2] == cs3[start3]) {
                b = isInterleave(cs1, start1, cs2, start2 + 1, cs3, start3 + 1, cache);
            } else {
                b = false;
            }
        }
        cache.put(key, b);

        return b;
    }

    public static boolean isInterleave(String s1, String s2, String s3) {
        if (s1.length() + s2.length() != s3.length()) {
            return false;
        }
        Map<Integer, Boolean> cache = new HashMap<>();

        return isInterleave(s1.toCharArray(), 0, s2.toCharArray(), 0, s3.toCharArray(), 0, cache);
    }


    public static void main(String[] args) {


        System.out.println(isInterleave("aabcc", "dbbca", "aadbbcbcac"));
        System.out.println(isInterleave("aabcc", "dbbca", "aadbbbaccc"));
    }

}
