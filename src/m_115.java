//https://leetcode.com/problems/distinct-subsequences/


import java.util.HashMap;
import java.util.Map;

public class m_115 {


    public static int sub(char[] sc, char[] tc, int sp, int tp, Map<Integer, Integer> cache) {

        int key = sp * 10000 + tp;
        if (cache.containsKey(key)) {
            return cache.get(key);
        } else {
            if (sc.length - sp < tc.length - tp || sp >= sc.length) {
                cache.put(key, 0);
                return 0;
            } else {
                if (tp == tc.length - 1) {
                    if (sc[sp] == tc[tp]) {
                        int r = 1 + sub(sc, tc, sp + 1, tp, cache);
                        cache.put(key, r);
                        return r;
                    } else {
                        int r = sub(sc, tc, sp + 1, tp, cache);
                        cache.put(key, r);
                        return r;
                    }
                } else {
                    if (sc[sp] == tc[tp]) {
                        int r = sub(sc, tc, sp + 1, tp + 1, cache) + sub(sc, tc, sp + 1, tp, cache);
                        cache.put(key, r);
                        return r;
                    } else {
                        int r = sub(sc, tc, sp + 1, tp, cache);
                        cache.put(key, r);
                        return r;
                    }
                }

            }
        }
    }

    public static int numDistinct(String s, String t) {
        if(t.length()==0){
            return 1;
        }
        return sub(s.toCharArray(), t.toCharArray(), 0, 0, new HashMap<>());
    }


    public static void main(String[] args) {


        System.out.println(numDistinct("ab", "ab"));
        System.out.println(numDistinct("aa", "a"));
        System.out.println(numDistinct("rabbbit", "rabbit"));
        System.out.println(numDistinct("babgbag", "bag"));
    }

}
