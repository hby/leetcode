//https://leetcode.com/problems/jump-game-ii


public class m_45 {
    public static int jumpFrom(int[] nums, int from) {
        int next = from + 1;
        int maxStep = nums[from];

        if (from + maxStep >= nums.length - 1) {
            return 0;
        } else {
            for (int i = 1; i <= maxStep; i++) {
                if (i + nums[from + i] > next - from + nums[next]) {
                    next = from + i;
                }
            }
            return 1 + jumpFrom(nums, next);
        }
    }

    public static int jump(int[] nums) {
        if(nums.length==1){
            return 0;
        }else {
            return 1 + jumpFrom(nums, 0);
        }
    }


    public static void main(String[] args) {


//        System.out.println(jump(new int[]{2, 3, 1, 1, 4}));
//        System.out.println(jump(new int[]{2, 3, 0, 1, 4}));
//        System.out.println(jump(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1,}));
//        System.out.println(jump(new int[]{1,2,1,1,1})); //3
        System.out.println(jump(new int[]{2,3,1,1,4}));  //2
    }

}
