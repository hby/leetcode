//https://leetcode.com/problems/text-justification/


import java.util.ArrayList;
import java.util.List;

public class m_68 {
    public static String concatWords(List<String> words, boolean endLine, int maxWidth) {
        StringBuilder sb = new StringBuilder();
        int wordLen = 0;
        if (endLine) {
            for (int i = 0; i < words.size() - 1; i++) {
                sb.append(words.get(i));
                sb.append(" ");
                wordLen += (words.get(i).length() + 1);
            }
            sb.append(words.get(words.size() - 1));
            wordLen += (words.get(words.size() - 1).length());
            sb.append(" ".repeat(Math.max(0, maxWidth - wordLen)));
        } else {
            if (words.size() == 1) {
                sb.append(words.get(0));
                sb.append(" ".repeat(Math.max(0, maxWidth - words.get(0).length())));
            } else {
                int wordsTotalLen = 0;
                for (String w : words) {
                    wordsTotalLen += w.length();
                }

                int spacesLeast = (maxWidth - wordsTotalLen) / (words.size() - 1);
                int needMore = maxWidth - wordsTotalLen - spacesLeast * (words.size() - 1);

                for (int i = 0; i < words.size(); i++) {
                    sb.append(words.get(i));
                    if (i < needMore) {
                        sb.append(" ".repeat(spacesLeast + 1));
                    } else if (i < words.size() - 1) {
                        sb.append(" ".repeat(spacesLeast));
                    }
                }

            }

        }
        return sb.toString();
    }

    public static List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result = new ArrayList<>();

        int currentLen = -1;
        List<String> currentWords = new ArrayList<>();

        for (int i = 0; i < words.length; i++) {
            if (currentLen + 1 + words[i].length() <= maxWidth) {
                currentLen += (1 + words[i].length());
                currentWords.add(words[i]);
            } else {
                if (i == words.length - 1) {
                    result.add(concatWords(currentWords, false, maxWidth));
                    String lastWord = words[i];
                    result.add(concatWords(new ArrayList<>(){{add(lastWord);}},true,maxWidth));
                    currentWords = new ArrayList<>();
                } else {
                    result.add(concatWords(currentWords, false, maxWidth));
                    currentLen = -1;
                    currentWords = new ArrayList<>();
                    i--;
                }

            }
        }
        if (!currentWords.isEmpty()) {
            result.add(concatWords(currentWords, true, maxWidth));
        }


        return result;
    }


    public static void main(String[] args) {

        List<String> strings = fullJustify(new String[]{"Here", "is", "an", "example", "of", "text", "justification."}, 14);
        for (String s : strings) {
            System.out.println("[" + s + "]");
        }

    }

}
