//https://leetcode.com/problems/wildcard-matching
//TODO ???


public class m_44 {
//    public static boolean minimalMatch(String s, String p) {
//        if (s.equals(p)) {
//            return true;
//        } else {
//            for (int i = 0; i < s.length(); i++) {
//                if (p.charAt(i) != '?' && p.charAt(i) != s.charAt(i)) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//
//    public static boolean subMatch(String s, int sp, LinkedList<String> pQueue) {
//
//        if (sp == s.length()) { //当s已经完成匹配而pattern队列还有值时
//            for (String pp : pQueue) { //若队列中存在 任意值(即:"")以外的值
//                if (!pp.equals("")) {
//                    return false; //不匹配
//                }
//            }
//            return true; //否则认为已经匹配
//        }
//
//        if (pQueue.isEmpty() && sp < s.length()) { //pattern队列已经空了，但s未完成匹配
//            return false;
//        }
//
//        String pTask = pQueue.peek();
//        while (pTask != null && pTask.equals("")) { //忽略掉所有的 任意值(即:"")
//            pTask = pQueue.poll();
//        }
//        if (pTask == null) { //最后什么都不剩了
//            return true;
//        }
//        s += pTask.length();
//
//        int ssp = s.length(); //让ssp尽量地远
//        while (!subMatch(s, ssp, pQueue) && ssp > sp) {
//
//            ssp--;
//        }
//
//        if (sp >= 0 && sp + pTask.length() <= s.length()) { //可以取到 s[sp:sp+pTask.lenth()] 这一段的字符串
//            String sub = s.substring(sp, sp + pTask.length());
//            if (minimalMatch(sub, pTask)) { //如果截取的两个字符串相同，继续下组比较
//                return subMatch(s, sp + pTask.length(), pQueue);
//            } else { //否则，不匹配
//                return false;
//            }
//        } else { //无法取到这么长的字符串，说明不匹配
//            return false;
//        }
//
//    }
//
//    public static boolean isMatch(String s, String p) {
//
//        String[] pw = p.split("\\*");
//        if (p.length() > 0 && pw.length == 0) {
//            return true;
//        }
//        if (s.length() > 0 && p.length() == 0) {
//            return false;
//        }
//        LinkedList<String> pQueue = new LinkedList<>(Arrays.asList(pw));
//        if (p.endsWith("*")) {
//            pQueue.add("");
//        }
//        return subMatch(s, 0, pQueue);
//    }

    public static boolean isMatch(String s,String p){
        String dp = p.replaceAll("\\*\\*","*");
        while (!dp.equals(p)){
            p = dp;
            dp = p.replaceAll("\\*\\*","*");
        }
        return doMatch(s,p);
    }
    public static boolean doMatch(String s, String p) {
        if (s.equals("")) {
            for(int i=0;i<p.length();i++){
                if(p.charAt(i)!='*'){
                    return false;
                }
            }
            return true;
        }



        if (p.length() == 0) {
            return s.length() == 0;
        } else if (p.length() == 1) {
            if (p.equals("*")) {
                return true;
            }
            if (p.equals("?")) {
                return s.length() == 1;
            }
            return s.equals(p);
        } else {
            if (p.charAt(0) == '*') {
                int pp = 1;
                while (pp<p.length()-1){
                    if(p.charAt(pp)!='*'){
                        break;
                    }
                    pp++;
                }
                for (int sp = 0; sp < s.length(); sp++) {
                    if (doMatch(s.substring(sp), p.substring(pp))) {
                        return true;
                    }
                }

                return false;
            } else if (p.charAt(0) == '?') {
                return s.length() >= 1 && doMatch(s.substring(1), p.substring(1));
            } else {
                return s.length() >= 1 && s.charAt(0) == p.charAt(0) && doMatch(s.substring(1), p.substring(1));
            }
        }
    }

    public static void main(String[] args) {
//        System.out.println(isMatch("aa", "**"));

//        System.out.println(isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", "a*******b"));

//        System.out.println(isMatch("acdcb", "a*c?b"));
        System.out.println(isMatch(
                "abbabaaabbabbaababbabbbbbabbbabbbabaaaaababababbbabababaabbababaabbbbbbaaaabababbbaabbbbaabbbbababababbaabbaababaabbbababababbbbaaabbbbbabaaaabbababbbbaababaabbababbbbbababbbabaaaaaaaabbbbbaabaaababaaaabb",
                "**aa*****ba*a*bb**aa*ab****a*aaaaaa***a*aaaa**bbabb*b*b**aaaaaaaaa*a********ba*bbb***a*ba*bb*bb**a*b*bb")
        );

        System.out.println("----------");

        System.out.println(isMatch("abefcdgiescdfimde", "ab*cd?i*de"));
        System.out.println(isMatch("aa", "a*"));
        System.out.println(isMatch("aa", "*a"));
        System.out.println(isMatch("aa", "*a*"));
        System.out.println(isMatch("aa", "a*a"));
        System.out.println(isMatch("aa", "aa*"));
        System.out.println(isMatch("aa", "*aa"));
        System.out.println(isMatch("aa", "*aa*"));


        System.out.println(isMatch("", "*"));
        System.out.println(isMatch("", "**"));
        System.out.println(isMatch("aa", "*"));
        System.out.println(isMatch("aa", "a*"));
        System.out.println(isMatch("aa", "**"));
        System.out.println(isMatch("adceb", "*a*b"));
        System.out.println("----------");
        System.out.println(isMatch("aa", ""));
        System.out.println(isMatch("aa", "a"));
        System.out.println(isMatch("cb", "?a"));
        System.out.println(isMatch("acdcb", "a*c?b"));
        System.out.println(isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", "a*******b"));

        System.out.println(isMatch(
                "abbabaaabbabbaababbabbbbbabbbabbbabaaaaababababbbabababaabbababaabbbbbbaaaabababbbaabbbbaabbbbababababbaabbaababaabbbababababbbbaaabbbbbabaaaabbababbbbaababaabbababbbbbababbbabaaaaaaaabbbbbaabaaababaaaabb",
                "**aa*****ba*a*bb**aa*ab****a*aaaaaa***a*aaaa**bbabb*b*b**aaaaaaaaa*a********ba*bbb***a*ba*bb*bb**a*b*bb")
        );
    }

}
