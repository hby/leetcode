//https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array

public class m_34 {

    public static int[] subSearch(int[] nums, int target, int startIndex, int endIndex) {
        if (endIndex - startIndex < 0) {
            return new int[]{-1, -1};
        } else if (endIndex - startIndex == 0) {
            if (nums[startIndex] == target) {
                return new int[]{startIndex, startIndex};
            } else {
                return new int[]{-1, -1};
            }
        } else if (endIndex - startIndex == 1) {
            if (nums[startIndex] == target && nums[endIndex] == target) {
                return new int[]{startIndex, endIndex};
            } else if (nums[startIndex] == target && nums[endIndex] != target) {
                return new int[]{startIndex, startIndex};
            } else if (nums[startIndex] != target && nums[endIndex] == target) {
                return new int[]{endIndex, endIndex};
            } else {
                return new int[]{-1, -1};
            }
        } else {
            int cut = startIndex + (endIndex - startIndex) / 2;
            if (nums[cut] == target || nums[cut + 1] == target) {
                if (nums[cut] != target && nums[cut + 1] == target) {
                    cut = cut + 1;
                }
                int start = cut, end = cut;
                while (start >= 1) {
                    if (nums[start - 1] == target) {
                        start--;
                    } else {
                        break;
                    }
                }
                while (end + 1 < nums.length) {
                    if (nums[end + 1] == target) {
                        end++;
                    } else {
                        break;
                    }
                }
                return new int[]{start, end};
            } else {
                if (nums[startIndex] <= target && target < nums[cut]) {
                    return subSearch(nums, target, startIndex, cut);
                } else if (nums[cut] < target && target <= nums[endIndex]) {
                    return subSearch(nums, target, cut, endIndex);
                } else {
                    return new int[]{-1, -1};

                }

            }
        }

    }

    public static int[] searchRange(int[] nums, int target) {
        return subSearch(nums, target, 0, nums.length - 1);

    }


    public static void main(String[] args) {
        int[] nums = new int[]{0,1,2,3,3,4,4,5,5,6,6,7,7,7,9,9,11,11,11,12,12,12,12};
        int[] results = searchRange(nums, 12);


        System.out.println(results[0]);
        System.out.println(results[1]);
    }

}
