//https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/

public class m_80 {
    public static int removeDuplicates(int[] nums) {
        if (nums.length <= 2) {
            return nums.length;
        }

        boolean duplicated = nums[0] == nums[1];
        int last = 1, p = 2;
        while (p < nums.length) {
            if (nums[p] == nums[last]) {
                if (duplicated) {
                    while (p < nums.length && nums[p] <= nums[last]) {
                        p++;
                    }
                    if (p < nums.length) {
                        last++;
                        duplicated = false;

                        int t = nums[p];
                        nums[p] = nums[last];
                        nums[last] = t;
                    }


                } else {
                    last++;
                    duplicated = true;

                    int t = nums[p];
                    nums[p] = nums[last];
                    nums[last] = t;
                }
            } else if (nums[p] > nums[last]) {
                last++;
                duplicated = false;

                int t = nums[p];
                nums[p] = nums[last];
                nums[last] = t;
            }

            p++;
        }


        return last+1;
    }


    public static void main(String[] args) {
        int[] nums;
        int len;

        nums = new int[]{1, 1, 1, 2, 2, 3};
        len = removeDuplicates(nums);
        for (int i = 0; i <= len; i++) {
            System.out.print(nums[i] + ",");
        }
        System.out.println("");

        nums = new int[]{0, 0, 1, 1, 1, 1, 2, 3, 3};
        len = removeDuplicates(nums);
        for (int i = 0; i < len; i++) {
            System.out.print(nums[i] + ",");
        }
        System.out.println("");
    }

}
