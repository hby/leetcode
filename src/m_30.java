//https://leetcode.com/problems/substring-with-concatenation-of-all-words/

//TODO time
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_30 {
    public static List<Integer> findSubstring(String s, String[] words) {
        List<String> arr1 = new LinkedList<>();
        List<String> arr2 = new LinkedList<>();
        boolean findInArr1 = true;
        List<Integer> results = new LinkedList<>();

        for (String word : words) {
            arr1.add(word);
        }

        int everyWordLen = words[0].length();
        int wordsLen = words.length;

        for (int i = 0; i + everyWordLen * wordsLen < s.length() + 1; i++) {
            boolean notBreak = true;
            for (int j = 0; j < wordsLen; j++) {
                String subStr = s.substring(i + j * everyWordLen, i + j * everyWordLen + everyWordLen);
                if (findInArr1) {
                    if (!arr1.remove(subStr)) {
                        findInArr1 = false;
                        notBreak = false;
                        break;
                    } else {
                        arr2.add(subStr);
                    }
                } else {
                    if (!arr2.remove(subStr)) {
                        findInArr1 = true;
                        notBreak = false;
                        break;
                    } else {
                        arr1.add(subStr);
                    }
                }
            }
            if (notBreak) {
                results.add(i);
            }
            if (findInArr1) {
                while (arr2.size() > 0) {
                    arr1.add(arr2.get(0));
                    arr2.remove(0);
                }

            } else {
                while (arr1.size() > 0) {
                    arr2.add(arr1.get(0));
                    arr1.remove(0);
                }

            }
        }
        return results;
    }


    public static void main(String[] args) {


        System.out.println(findSubstring("barfoofoobarthefoobarman", new String[]{"bar", "foo", "the"}));
        System.out.println(findSubstring("wordgoodgoodgoodbestword", new String[]{"word","good","best","word"}));
        System.out.println(findSubstring("barfoothefoobarman", new String[]{"bar", "foo"}));
        System.out.println(findSubstring("a", new String[]{"a", "a"}));
        System.out.println(findSubstring("a", new String[]{"a"}));
        System.out.println(findSubstring("ab", new String[]{"a"}));
        System.out.println(findSubstring("a", new String[]{"ab"}));
        System.out.println(findSubstring("aaaaaaaaaaaaa", new String[]{"a"}));
        System.out.println(findSubstring("aaa", new String[]{"aa"}));
        System.out.println(findSubstring("aaaaaaaaaaaaa", new String[]{"aa"}));
        System.out.println(findSubstring("aaaaaaaaaaaaa", new String[]{"aa","aa"}));
    }

}
