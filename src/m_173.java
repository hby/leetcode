//https://leetcode.com/problems/binary-search-tree-iterator/


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class m_173 {


    public static void main(String[] args) {
        BSTIterator bSTIterator = new BSTIterator(TreeNode.gen(7, 3, 15, null, null, 9, 20));
        System.out.println(bSTIterator.next());    // return 3
        System.out.println(bSTIterator.next());    // return 7
        System.out.println(bSTIterator.hasNext()); // return True
        System.out.println(bSTIterator.next());    // return 9
        System.out.println(bSTIterator.hasNext()); // return True
        System.out.println(bSTIterator.next());    // return 15
        System.out.println(bSTIterator.hasNext()); // return True
        System.out.println(bSTIterator.next());    // return 20
        System.out.println(bSTIterator.hasNext()); // return False


        System.out.println("Hello World");
    }

}

class BSTIterator {
    private Queue<Integer> queue;
    public BSTIterator(TreeNode root){
        this.queue=new LinkedList<>();
        walk(root);
    }
    private void walk(TreeNode node){
        if(node!=null){
            walk(node.left);
            this.queue.add(node.val);
            walk(node.right);
        }
    }
    public int next() {
        return queue.poll();
    }

    public boolean hasNext() {
        return !queue.isEmpty();

    }
}