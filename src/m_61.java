//https://leetcode.com/problems/rotate-list/


public class m_61 {
    public static ListNode rotateRight(ListNode head, int k) {
        if (head == null || head.next == null || k == 0) return head;
        ListNode tail = head;
        int len = 1;
        while (tail.next != null) {
            tail = tail.next;
            len++;
        }
        k = k % len;
        if (k == 0) return head;
        int cut = len - k;

        ListNode cutted = head;
        for (int i = 1; i < cut; i++) {
            cutted = cutted.next;
        }
        ListNode r = cutted.next;
        cutted.next = null;
        tail.next = head;


        return r;
    }


    public static void main(String[] args) {

        System.out.println(rotateRight(new ListNode(1, 2), 0));
        System.out.println(rotateRight(new ListNode(1, 2), 1));
        System.out.println(rotateRight(new ListNode(1, 2), 2));
        System.out.println(rotateRight(new ListNode(1, 2), 3));
    }

}
