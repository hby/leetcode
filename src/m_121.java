//https://leetcode.com/problems/best-time-to-buy-and-sell-stock/


public class m_121 {
    public static int maxProfit(int[] prices) {
        int min = prices[0], profit = 0;
        for (int price : prices) {
            if (price <= min) {
                min = price;
            } else {
                profit = Math.max(profit, price - min);
            }
        }
        return profit;
    }


    public static void main(String[] args) {


        System.out.println(maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

}
