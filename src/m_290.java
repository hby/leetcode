// https://leetcode.com/problems/word-pattern/


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class m_290 {
    public static boolean wordPattern(String pattern, String s) {
        char[] pc = pattern.toCharArray();
        char[] sc = s.toCharArray();

        int pp = 0, sp = 0;
        int pl = pattern.length(), sl = s.length();

        Map<Character, String> m = new HashMap<>();
        Set<String> rs = new HashSet<>();

        while (pp < pl && sp < sl) {
            int ssp = sp;
            while (sp < sl && sc[sp] != ' ') {
                sp++;
            }
            String sInM = m.get(pc[pp]);
            String sInS = s.substring(ssp, sp);

            if (sInM == null) {
                if (rs.contains(sInS)) {
                    return false;
                } else {
                    m.put(pc[pp], sInS);
                    rs.add(sInS);
                }

            } else {
                if (!sInM.equals(sInS)) {
                    return false;
                }
            }
            pp++;
            sp++;
        }


        return pp == pl && sp == sl + 1;
    }


    public static void main(String[] args) {


        System.out.println(wordPattern("abba", "dog cat cat dog"));
        System.out.println(wordPattern("abba", "dog cat cat fish"));
        System.out.println(wordPattern("aaaa", "dog cat cat dog"));
        System.out.println(wordPattern("abba", "dog dog dog dog"));
    }

}
