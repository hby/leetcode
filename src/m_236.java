// https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/


import java.util.LinkedList;
import java.util.List;

public class m_236 {
    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        LinkedList<TreeNode> ppath = new LinkedList<>(), qpath = new LinkedList<>();
        ppath.add(root);
        qpath.add(root);

        walkTo(ppath, root, p);
        walkTo(qpath, root, q);

        TreeNode lca = root;

        TreeNode pp = ppath.pollFirst();
        TreeNode qp = qpath.pollFirst();

        while (pp != null && pp.equals(qp)) {
            lca = pp;
            pp = ppath.pollFirst();
            qp = qpath.pollFirst();
        }


        return lca;
    }

    public static boolean walkTo(LinkedList<TreeNode> path, TreeNode current, TreeNode target) {
        if (target.equals(current)) {
            return true;
        } else {
            if (current == null) {
                return false;
            } else {
                path.add(current.left);
                if (walkTo(path, current.left, target)) {
                    return true;
                } else {
                    path.removeLast();
                    path.add(current.right);
                    if (walkTo(path, current.right, target)) {
                        return true;
                    } else {
                        path.removeLast();
                        return false;
                    }
                }
            }
        }
    }


    public static void main(String[] args) {
        TreeNode root;

        root = TreeNode.gen(3, 5, 1, 6, 2, 0, 8, null, null, 7, 4);
        System.out.println(lowestCommonAncestor(root, root.left, root.left.right.right));

    }

}
