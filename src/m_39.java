//https://leetcode.com/problems/combination-sum/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class m_39 {


    public static List<List<Integer>> subSum(int[] candidates, int target, int endIndex, boolean mustUseLast) {
        List<List<Integer>> l = new ArrayList<>();

        if (endIndex < 0) {
            return null;
        } else if (endIndex == 0) {
            if (candidates[0] == target) {
                return new ArrayList<>() {{
                    add(
                            new ArrayList<>() {{
                                add(candidates[endIndex]);
                            }}
                    );
                }};
            } else if (target == 0) {
                return new ArrayList<>() {{
                    add(new ArrayList<>());
                }};
            }
        }

        if (mustUseLast) {
            if (candidates[endIndex] > target) {
                return null;
            } else if (candidates[endIndex] == target) {
                return new ArrayList<>() {{
                    add(
                            new ArrayList<>() {{
                                add(candidates[endIndex]);
                            }}
                    );
                }};
            }
        }


        List<List<Integer>> l1 = subSum(candidates, target - candidates[endIndex], endIndex, true);
        List<List<Integer>> l2 = subSum(candidates, target - candidates[endIndex], endIndex - 1, false);

        if (l1 != null) {
            for (List<Integer> ll1 : l1) {
                ll1.add(candidates[endIndex]);
                l.add(ll1);
            }
        }

        if (l2 != null) {
            for (List<Integer> ll2 : l2) {
                ll2.add(candidates[endIndex]);
                l.add(ll2);
            }
        }
        if(!mustUseLast){
            List<List<Integer>> l3 = subSum(candidates, target, endIndex - 1, false);

            if (l3 != null) {
                l.addAll(l3);
            }
        }


        return l;
    }


    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
//        Arrays.sort(candidates);
        return subSum(candidates, target, candidates.length - 1, false);
    }


    public static void main(String[] args) {

        List<List<Integer>> results = combinationSum(new int[]{30,34,25,24,29,38,36,42,45,44,31,28,26,37,23,20,47,40,49,46,39,43,33,41,27,32,35,48}, 54);
        for (List<Integer> r1 : results) {
            System.out.print("[");
            for (int r2 : r1) {
                System.out.print(r2 + ",");
            }
            System.out.print("]");
        }

    }

}
