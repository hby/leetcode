//https://leetcode.com/problems/remove-linked-list-elements/


public class m_203 {
    public static ListNode removeElements(ListNode head, int val) {
        while (head != null && head.val == val) {
            head = head.next;
        }
        if(head==null){
            return null;
        }
        ListNode priv = head;
        ListNode current = head.next;
        while (current!=null){
            if(current.val==val){
                current=current.next;
                priv.next=current;
            }else {
                priv=current;
                current=current.next;
            }
        }

        return head;
    }


    public static void main(String[] args) {

        ListNode n = new ListNode(1, 2, 6, 3, 4, 5, 6);

        System.out.println(removeElements(n, 6));
    }

}
