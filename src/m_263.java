//https://leetcode.com/problems/ugly-number/

public class m_263 {
    public static boolean isUgly(int n) {
        if (n <= 0) {
            return false;
        } else if (n <= 6) {
            return true;
        } else if (n % 2 == 0) {
            return isUgly(n / 2);
        } else if (n % 3 == 0) {
            return isUgly(n / 3);
        } else if (n % 5 == 0) {
            return isUgly(n / 5);
        } else {
            return false;
        }
    }


    public static void main(String[] args) {


        System.out.println(isUgly(6));
    }

}
