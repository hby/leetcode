//https://leetcode.com/problems/search-a-2d-matrix/


public class m_74 {
    public static boolean searchMatrix(int[][] matrix, int target) {
        int iLen = matrix.length, jLen = matrix[0].length;
        int i, j;
        for (i = 0; i < iLen; i++) {
            if (target < matrix[i][0]) {
                return false;
            } else if (target >= matrix[i][0] && target <= matrix[i][jLen - 1]) {
                break;
            }
        }
        if (i == iLen) {
            return false;
        }
        for (j = 0; j < jLen; j++) {
            if (target == matrix[i][j]) {
                return true;
            } else if (target < matrix[i][j]) {
                return false;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println(searchMatrix(new int[][]{
                new int[]{1},
        }, 1));
    }

}
