//https://leetcode.com/problems/insert-interval/


import java.util.Arrays;
import java.util.Comparator;

public class m_57 {
    public static boolean merged(int[] orig, int[] newInterval) {
        int low = Math.min(orig[0], newInterval[0]);
        int high = Math.max(orig[1], newInterval[1]);

        if (
                (newInterval[0] >= orig[0] && newInterval[0] <= orig[1]) ||
                        (newInterval[1] >= orig[0] && newInterval[1] <= orig[1]) ||
                        (newInterval[0] <= orig[0] && newInterval[1] >= orig[1])

        ) {
            orig[0] = low;
            orig[1] = high;
            return true;
        }

        return false;
    }

    public static int[][] insert(int[][] intervals, int[] newInterval) {
        boolean hasMerged = false;
        int i = 0, end = 0;
        for (; i < intervals.length; i++) {
            if (!hasMerged) {
                hasMerged = merged(intervals[i], newInterval);
                end = i;
            } else {
                if (!merged(intervals[end], intervals[i])) {
                    break;
                }
            }
        }
        if (!hasMerged) {
            int[][] inserted = new int[intervals.length + 1][2];
            System.arraycopy(intervals, 0, inserted, 0, intervals.length);
            inserted[intervals.length] = newInterval;
            Arrays.sort(inserted, Comparator.comparingInt(o -> o[0]));
            return inserted;
        } else {
            int[][] inserted = new int[intervals.length + 1 - (i - end)][2];
            System.arraycopy(intervals, 0, inserted, 0, end + 1);
            System.arraycopy(intervals, i, inserted, end + 1, intervals.length - i);
            Arrays.sort(inserted, Comparator.comparingInt(o -> o[0]));
            return inserted;
        }
    }


    public static void main(String[] args) {

        for (int[] ints : insert(new int[][]{
                new int[]{1, 3},
                new int[]{6, 9},
        }, new int[]{2, 5})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{1, 2},
                new int[]{3, 5},
                new int[]{6, 7},
                new int[]{8, 10},
                new int[]{12, 16},
        }, new int[]{4, 8})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
        }, new int[]{5, 7})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{1, 5},
        }, new int[]{2, 3})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{1, 5},
        }, new int[]{2, 7})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{1, 5},
        }, new int[]{0, 6})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{1, 5},
        }, new int[]{0, 0})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : insert(new int[][]{
                new int[]{3, 5},
        }, new int[]{12, 15})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");
        for (int[] ints : insert(new int[][]{
                new int[]{3, 5},
                new int[]{12, 15},
        }, new int[]{6, 6})) {
            System.out.print("[");
            for (int anInt : ints) {
                System.out.print(anInt + ",");
            }
            System.out.print("]");
        }
        System.out.println("");
    }

}
