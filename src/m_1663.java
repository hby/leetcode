// https://leetcode.com/problems/smallest-string-with-a-given-numeric-value/

import java.util.Arrays;

public class m_1663 {
    public static String getSmallestString(int n, int k) {
        char[] cs = new char[n];
        for (int i = 0; i < n; i++) {
            int maxPossibleRemainK = (n - i - 1) * 26;
            if (k > maxPossibleRemainK) {
                cs[i] = (char) ('a' - 1 + k - maxPossibleRemainK);
                k = maxPossibleRemainK;
            } else {
                cs[i] = 'a';
                k -= 1;
            }
            if (k == maxPossibleRemainK) {
                for (int j = i + 1; j < n; j++) {
                    cs[j] = 'z';
                }
                return String.valueOf(cs);
            }
        }
        return String.valueOf(cs);
    }


    public static void main(String[] args) {


        System.out.println(getSmallestString(3, 27));
        System.out.println(getSmallestString(5, 73));
    }

}
