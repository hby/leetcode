import java.util.ArrayList;
import java.util.List;
/* TODO: 第10题 */
/* https://leetcode.com/problems/regular-expression-matching/ */
public class m_10 {


    public static boolean isMatch(String s, String p) {
        int sLen = s.length(), pLen = p.length();
        int sp = sLen - 1, pp = pLen - 1;

        char pChar = p.charAt(pp);
        boolean isWild;
        if (pChar == '*') {
            isWild = true;
            pp -= 1;
            pChar = p.charAt(pp);
        } else {
            isWild = false;
        }

        while (pp >= 0 && sp >= 0) {
            if (isWild) { //是  X*  形式
                if (pChar == '.') { // 是  .*  形式
                    while (pp - 1 >= 0 && p.charAt(pp - 1) == '*' && p.charAt(pp - 2) == '.') { // 向前找到所有 .* 并略过
                        pp -= 2;
                    }

                    if (pp > 0) {
                        if (p.charAt(pp - 1) == '*') { // 找到 X* 或者 X
                            pp -= 1;
                            pChar = p.charAt(pp);
                        } else {
                            isWild = false;
                            pChar = p.charAt(pp);
                        }
                    } else {
                        return true;
                    }

                } else {
                    int minRepeatTimes = 0;
                    boolean f = false;
                    while (pp >= 0 && pChar == p.charAt(pp)) {
                        minRepeatTimes++;
                        pp--;
                        f = true;
                    }
                    if (f) {
                        pp++;
                    }
                    minRepeatTimes--;

                    int repeatTimes = 0;
                    while (sp >= 0 && pChar == s.charAt(sp)) {
                        repeatTimes++;
                        sp--;
                    }

                    if (minRepeatTimes > repeatTimes) {
                        return false;

                    }
                }
            } else {
                if (pChar != s.charAt(sp) && pChar != '.') {
                    return false;
                }
            }

            sp--;
            pp--;
            if (pp >= 0) {
                pChar = p.charAt(pp);
                if (pChar == '*') {
                    isWild = true;
                    pp -= 1;
                    pChar = p.charAt(pp);
                } else {
                    isWild = false;
                }
            }
        }

        if (isWild) {
            while (pp - 1 >= 0 && p.charAt(pp - 1) == '*') {
                pp -= 2;
            }
            isWild = p.charAt(pp + 1) == '*';
        }


        return sp < 0 && (isWild || pp < 0);
    }

    public static void main(String[] args) {


        System.out.println(isMatch("aa", "a"));//F
        System.out.println(isMatch("mississippi", "mis*is*p*."));//F
        System.out.println(isMatch("ab", ".*c"));//F
        System.out.println(isMatch("a", "ab*a"));//F
        System.out.println(isMatch("aa", "a*"));//T
        System.out.println(isMatch("ab", ".*"));//T
        System.out.println(isMatch("aab", "c*a*b"));//T
        System.out.println(isMatch("a", "ab*"));//T
        System.out.println(isMatch("aaa", "a*a"));//T
    }
}
