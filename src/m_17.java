import java.util.*;


//https://leetcode.com/problems/letter-combinations-of-a-phone-number/

public class m_17 {
    public static List<String> letterCombinations(String digits) {
        Map<Character, char[]> keyboard = new HashMap<>() {
            {
                put('2', new char[]{'a', 'b', 'c'});
            }

            {
                put('3', new char[]{'d', 'e', 'f'});
            }

            {
                put('4', new char[]{'g', 'h', 'i'});
            }

            {
                put('5', new char[]{'j', 'k', 'l'});
            }

            {
                put('6', new char[]{'m', 'n', 'o'});
            }

            {
                put('7', new char[]{'p', 'q', 'r', 's'});
            }

            {
                put('8', new char[]{'t', 'u', 'v'});
            }

            {
                put('9', new char[]{'w', 'x', 'y', 'z'});
            }
        };
        LinkedList<String> strings = new LinkedList<String>();
//        List<String> strings = new ArrayList<>();
        int len = digits.length();
        if (len == 0) {
            return strings;
        } else if (len == 1) {
            for (char c1 : keyboard.get(digits.charAt(0))) {
                strings.add(String.valueOf(c1));
            }
        } else if (len == 2) {
            for (char c1 : keyboard.get(digits.charAt(0))) {
                for (char c2 : keyboard.get(digits.charAt(1))) {
                    strings.add("" + c1 + c2);
                }
            }

        } else if (len == 3) {
            for (char c1 : keyboard.get(digits.charAt(0))) {
                for (char c2 : keyboard.get(digits.charAt(1))) {
                    for (char c3 : keyboard.get(digits.charAt(2))) {
                        strings.add("" + c1 + c2 + c3);
                    }
                }
            }

        } else {
            for (char c1 : keyboard.get(digits.charAt(0))) {
                for (char c2 : keyboard.get(digits.charAt(1))) {
                    for (char c3 : keyboard.get(digits.charAt(2))) {
                        for (char c4 : keyboard.get(digits.charAt(3))) {
                            strings.add("" + c1 + c2 + c3 + c4);
                        }
                    }
                }
            }
        }

        return strings;

    }


    public static void main(String[] args) {
        System.out.println(letterCombinations("2345"));
    }
}
