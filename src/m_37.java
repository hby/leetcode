//https://leetcode.com/problems/valid-sudoku/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class m_37 {
    public static boolean isValidSudoku(int[][] board) {
        int[] rows = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0}, cols = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0}, sqars = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                int sqar = row / 3 * 3 + col / 3;
                int c = board[row][col];
                if (c != 0) {
                    if ((rows[row] & (1 << c)) != 0 || (cols[col] & (1 << c)) != 0 || (sqars[sqar] & (1 << c)) != 0) {
                        return false;
                    }
                    rows[row] = rows[row] | (1 << c);
                    cols[col] = cols[col] | (1 << c);
                    sqars[sqar] = sqars[sqar] | (1 << c);
                }
            }
        }
        return true;
    }

    public static boolean isValidSudokuOnlyCheck(int[][] board, int row, int col) {
        int rows = 0, cols = 0, sqars = 0;
        int sqar = row / 3 * 3 + col / 3;
        int v = board[row][col];

        for (int r = 0; r < 9; r++) {
            if (r != row) {
                int t = board[r][col];
                if (t != 0) {
                    rows = rows | (1 << t);
                }
            }
        }
        if ((rows & (1 << v)) != 0) {
            return false;
        }

        for (int c = 0; c < 9; c++) {
            if (c != col) {
                int t = board[row][c];
                if (t != 0) {
                    cols = cols | (1 << t);
                }
            }
        }
        if ((cols & (1 << v)) != 0) {
            return false;
        }

        for (int r = sqar / 3 * 3; r < sqar / 3 *3 + 3; r++) {
            for (int c = 3 * (sqar % 3); c < 3 * (sqar % 3) + 3; c++) {
                if (c != col && r != row) {
                    int t = board[r][c];
                    if (t != 0) {
                        sqars = sqars | (1 << t);
                    }
                }
            }
        }
        if ((sqars & (1 << v)) != 0) {
            return false;
        }

        return true;
    }

    public static boolean subSolve(int[][] board, int remain) {
        if (remain == 0) {
            return true;
        }
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] == 0) {
                    for (int t = 1; t <= 9; t++) {
                        board[i][j] = t;
                        if (isValidSudokuOnlyCheck(board, i, j)) {
                            if (subSolve(board, remain - 1)) {
                                return true;
                            }
                        }
                    }
                    board[i][j] = 0;
                    return false;
                }
            }
        }
        return false;
    }

    public static void solveSudoku(char[][] board) {
        int remain = 0;
        int[][] intBoard = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] == '.') {
                    remain++;
                    intBoard[i][j] = 0;
                } else {
                    intBoard[i][j] = board[i][j] - '0';
                }
            }
        }
        subSolve(intBoard, remain);

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                board[i][j] = (char) ('0' + intBoard[i][j]);
            }
        }

    }


    public static void main(String[] args) {
        char[][] boards = new char[][]{
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'},
        };

        solveSudoku(boards);
    }

}
