// https://leetcode.com/problems/is-graph-bipartite/

import java.util.*;

public class m_785 {
    public static boolean isBipartite(int[][] graph) {
        int[] rs = new int[graph.length]; //  0:   not initial   -:  in left set   +: in right set
        List<Set<Integer>> rm = new ArrayList<>(rs.length);
        for (int i = 0; i < rs.length; i++) {
            rm.add(new HashSet<>());
        }

        for (int i = 0; i < graph.length; i++) {
            for (int v : graph[i]) {
                if (v > i) {
                    rm.get(i).add(v);
                } else {
                    rm.get(v).add(i);
                }
            }
        }

        for (int i = 0; i < rs.length; i++) {
            int childVal = 0;
            if (rs[i] != 0) {
                childVal = -rs[i];
            }

            for (Integer c : rm.get(i)) {
                if (childVal == 0) {
                    childVal = rs[c];
                } else {
                    if (rs[c] == 0) {
                        rs[c] = childVal;
                    } else if (rs[c] != childVal) {
                        return false;
                    } else { // do nothing
                        continue;
                    }
                }
            }

            if (rs[i] == 0 && childVal == 0) {
                rs[i] = -1;
                childVal = 1;
                for (Integer c : rm.get(i)) {
                    rs[c] = childVal;
                }
            } else if (rs[i] == 0 && childVal != 0) {
                rs[i] = -childVal;
            }
        }
        return true;
    }

    public static int[][] IntArrayParser2D(String s) {
        s = s.replaceAll(" ", "");
        assert s.startsWith("[");
        assert s.endsWith("]");

        s = s.substring(1, s.length() - 1);
        String[] cs = s.split("]");
        int[][] i = new int[cs.length][];
        for (int p = 0; p < cs.length; p++) {
            if (cs[p].startsWith(",[")) {
                cs[p] = cs[p].substring(1);
            }
            cs[p] = cs[p] + "]";

            i[p] = IntArrayParser1D(cs[p]);
        }

        return i;
    }

    public static int[] IntArrayParser1D(String s) {
        assert s.startsWith("[");
        assert s.endsWith("]");

        s = s.substring(1, s.length() - 1);
        String[] cs = s.split(",");
        int[] i = new int[cs.length];
        for (int p = 0; p < cs.length; p++) {
            i[p] = Integer.parseInt(cs[p]);
        }
        return i;
    }


    public static void main(String[] args) {


//        System.out.println(isBipartite(IntArrayParser2D("[[1,2,3],[0,2],[0,1,3],[0,2]]")));//false
//        System.out.println(isBipartite(IntArrayParser2D("[[1,3],[0,2],[1,3],[0,2]]")));//true
        System.out.println(isBipartite(IntArrayParser2D("[[3],[2,4],[1],[0,4],[1,3]]")));//true
    }

}
