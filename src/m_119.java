//https://leetcode.com/problems/pascals-triangle-ii/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class m_119 {
    public static List<Integer> getRow(int rowIndex) {
        Integer[] results = new Integer[rowIndex + 1];
        results[0] = 1;
        results[rowIndex] = 1;

        for (int i = 1; i < rowIndex / 2 + 1; i++) {
            long t = rowIndex + 1 - i;
            t = t*results[i - 1]/i;
            results[i] = (int) t;
            results[rowIndex - i] = (int)t;
        }

        return Arrays.asList(results);
    }


    public static void main(String[] args) {

//        System.out.println(getRow(2));

//        System.out.println(getRow(0));
//        System.out.println(getRow(1));
//        System.out.println(getRow(2));
        System.out.println(getRow(3));
        System.out.println(getRow(4));
        System.out.println(getRow(5));
        System.out.println(getRow(6));
        System.out.println(getRow(7));
//        System.out.println(getRow(8));
//        System.out.println(getRow(9));
//        System.out.println(getRow(10));
    }

}
