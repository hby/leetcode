import java.util.ArrayList;
import java.util.List;

//TODO: 优化
//https://leetcode.com/problems/container-with-most-water/
public class m_11 {
    public static int maxArea(int[] height) {
        int len = height.length;
//        int start = 0, end = 1;
//        int area = (end - start) * Math.min(height[start], height[end]);
        int area =  Math.min(height[0], height[1]);
        for (int p = 0; p < len; p++) {
            for (int q = p + 1; q < len; q++) {
//                for (int q = (Math.max(p + 1, end)); q < len; q++) {
                if ((q - p) * Math.min(height[p], height[q]) > area) {
                    area = (q - p) * Math.min(height[p], height[q]);
//                    start = p;
//                    end = q;
                }
            }
        }


        return area;
    }

    public static void main(String[] args) {
        System.out.println(maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
        System.out.println(maxArea(new int[]{1, 1}));
        System.out.println(maxArea(new int[]{4, 3, 2, 1, 4}));
        System.out.println(maxArea(new int[]{1, 2, 1}));
        System.out.println(maxArea(new int[]{2, 3, 4, 5, 18, 17, 6}));
    }
}
