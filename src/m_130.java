//https://leetcode.com/problems/surrounded-regions/


import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class m_130 {
    public static void solve(char[][] board) {
        int m = board.length, n = board[0].length;

        List<Integer> privResults = new LinkedList<>();

        boolean[] osetb = new boolean[m * n];
        boolean[] results = new boolean[m*n];

        for (int i = 1; i < m - 1; i++) {
            for (int j = 1; j < n - 1; j++) {
                osetb[i * n + j] = board[i][j] == 'O';
            }
        }
        for (int i = 0; i < m; i++) {
            if (board[i][0] == 'O') {
                results[i*n]=true;
                privResults.add(i * n);
            }
            if (board[i][n - 1] == 'O') {
                results[i * n + (n - 1)]=true;
                privResults.add(i * n + (n - 1));
            }
        }
        for (int j = 0; j < n; j++) {
            if (board[0][j] == 'O') {
                results[j]=true;
                privResults.add(j);
            }
            if (board[m - 1][j] == 'O') {
                results[(m - 1) * n + j]=true;
                privResults.add((m - 1) * n + j);
            }
        }

        while (!privResults.isEmpty()) {
            List<Integer> currentResults = new LinkedList<>();
            for (Integer pos : privResults) {
                int a = pos + n, b = pos - n, c = pos + 1, d = pos - 1;
                if (a < m * n && osetb[a]) {
                    currentResults.add(a);
                    results[a]=true;
                    osetb[a] = false;
                }
                if (b > 0 && osetb[b]) {
                    currentResults.add(b);
                    results[b]=true;
                    osetb[b] = false;

                }
                if (c < m * n && osetb[c]) {
                    currentResults.add(c);
                    results[c]=true;
                    osetb[c] = false;

                }
                if (d > 0 && osetb[d]) {
                    currentResults.add(d);
                    results[d]=true;
                    osetb[d] = false;
                }
            }
            privResults = currentResults;
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j]=results[i * n + j]?'O':'X';
            }
        }
    }


    public static void main(String[] args) {
//        char[][] board = new char[][]{
//                new char[]{'X', 'X', 'X', 'X', 'X'},
//                new char[]{'X', 'O', 'O', 'X', 'X'},
//                new char[]{'X', 'X', 'O', 'X', 'X'},
//                new char[]{'X', 'O', 'X', 'X', 'X'},
//        };
//        char[][] board = new char[][]{
//                new char[]{'O', 'X', 'X', 'O', 'X'},
//                new char[]{'X', 'O', 'O', 'X', 'O'},
//                new char[]{'X', 'O', 'X', 'O', 'X'},
//                new char[]{'O', 'X', 'O', 'O', 'O'},
//                new char[]{'X', 'X', 'O', 'X', 'O'},
//        };
        char[][] board = new char[][]{
                new char[]{'X', 'O', 'X', 'O', 'X', 'O'},
                new char[]{'O', 'X', 'O', 'X', 'O', 'X'},
                new char[]{'X', 'O', 'X', 'O', 'X', 'O'},
                new char[]{'O', 'X', 'O', 'X', 'O', 'X'},

        };
        for (char[] b : board) {
            for (char c : b) {
                System.out.print(c + ",");
            }
            System.out.println();
        }
        System.out.println();

        solve(board);

        for (char[] b : board) {
            for (char c : b) {
                System.out.print(c + ",");
            }
            System.out.println();
        }


    }

}
