//https://leetcode.com/problems/multiply-strings


import java.util.*;

public class m_43 {
//    public static void pushTo(List<Integer>r,int index,int num){
//        int c = r.get(index);
//        if(c+num<10){
//            r.set(index,c+num);
//        }else {
//
//        }
//    }

    public static String multiply(String num1, String num2) {
        if (Objects.equals(num1, "0") || Objects.equals(num2, "0")) {
            return "0";
        }
        int num1Len = num1.length(), num2Len = num2.length();
        int[] r = new int[num1Len + num2Len];
//        List<Integer> r = new ArrayList<>(Collections.nCopies(num1Len + num2Len, 0));
        int overflow = 0;
        for (int i = num1Len - 1; i >= 0; i--) {
            for (int j = num2Len - 1; j >= 0; j--) {
                int position = (num1Len - 1 - i) + (num2Len - 1 - j);
                r[position] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
//                r.set(position, r.get(position) + (num1.charAt(i) - '0') * (num2.charAt(j) - '0'));
            }
        }
        for (int i = 0; i < num1Len + num2Len; i++) {
            int c = overflow + r[i];
            r[i] = c % 10;
            overflow = c / 10;
        }
        StringBuilder sb = new StringBuilder();
        boolean start = false;
        for (int i = num1Len + num2Len - 1; i >= 0; i--) {
            if (start || r[i] != 0) {
                sb.append(r[i]);
                start = true;
            }
        }
        return sb.toString();

    }


    public static void main(String[] args) {


        System.out.println(multiply("123", "456"));
    }

}
