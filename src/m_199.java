//https://leetcode.com/problems/binary-tree-right-side-view/


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class m_199 {
    public static void bfs(List<Integer>results,TreeNode node){

    }

    public static List<TreeNode> saw(List<TreeNode> nodes){
        List<TreeNode> sees = new ArrayList<>(100);
        for(TreeNode node:nodes){
            if(node.left!=null){
                sees.add(node.left);
            }
            if(node.right!=null){
                sees.add(node.right);
            }
        }
        return sees;
    }

    public static List<Integer> rightSideView(TreeNode root) {
        if(root==null){
            return new LinkedList<>();
        }
        List<Integer> results = new ArrayList<>(100);
        List<TreeNode> levels = new ArrayList<>(1);
        levels.add(root);
        while (levels.size()!=0){
            results.add(levels.get(levels.size()-1).val);
            levels = saw(levels);
        }
        return results;

    }


    public static void main(String[] args){



        System.out.println(rightSideView(TreeNode.gen(1,2,3,null,5,null,4)));
    }

}
