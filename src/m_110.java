//https://leetcode.com/problems/balanced-binary-tree/


public class m_110 {
    public static int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        } else if (root.left == null && root.right == null) {
            return 1;
        } else if (root.left == null || root.right == null) {
            int singleHeight;
            if (root.left == null) {
                singleHeight = getHeight(root.right);
            } else {
                singleHeight = getHeight(root.left);
            }
            if (singleHeight == -1) {
                return -1;
            } else if (singleHeight > 1) {
                return -1;
            } else {
                return singleHeight+1;
            }
        } else {
            int leftHeight = getHeight(root.left);
            int rightHeight = getHeight(root.right);
            if(leftHeight==-1||rightHeight==-1){
                return -1;
            }else if(Math.abs(leftHeight-rightHeight)>1) {
                return -1;
            }else {
                return Math.max(leftHeight,rightHeight)+1;
            }
        }
    }

    public static boolean isBalanced(TreeNode root) {
        return getHeight(root)!=-1;

    }

    public static void main(String[] args) {

        boolean b = isBalanced(TreeNode.gen(1, 2, 2, 3, null, null, 3, 4, null, null, 4));

        System.out.println(b);
    }

}
