//https://leetcode.com/problems/triangle/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_120 {
    public static int minimumTotal(List<List<Integer>> triangle) {
        int size = triangle.size();

        Integer[] priv = new Integer[size];
        triangle.get(size - 1).toArray(priv);

        for (int i = size - 2; i >= 0; i--) {
            int p = 0;
            List<Integer> l = triangle.get(i);
            for (int c : l) {
                priv[p] = c + Math.min(priv[p], priv[p + 1]);
                p++;
            }
        }
        return priv[0];
    }


    public static void main(String[] args) {
        List<List<Integer>> l = new ArrayList<>();
        l.add(Arrays.asList(2));
        l.add(Arrays.asList(3, 4));
        l.add(Arrays.asList(6, 5, 7));
        l.add(Arrays.asList(4, 1, 8, 3));


        System.out.println(minimumTotal(l));
    }

}
