// https://leetcode.com/problems/merge-k-sorted-lists/

import java.util.*;

public class m_23 {
    static class DualLink {
        ListNode val;
        DualLink priv;
        DualLink next;

        DualLink(ListNode val) {
            this.val = val;
            this.priv = null;
            this.next = null;
        }

        void destroy() {
            if (this.next != null) {
                this.next.priv = this.priv;
            }
            if (this.priv != null) {
                this.priv.next = this.next;
            }
            this.priv = null;
            this.next = null;
        }

        boolean deeper() {
            if (this.val != null && this.val.next != null) {
                this.val = this.val.next;
                return true;
            } else {
                return false;
            }
        }

        int peek() {
            return this.val.val;
        }

        // 在当前位置追加一个节点，并返回到当前位置
        DualLink append(ListNode n) {
            DualLink l = new DualLink(n);
            if (this.next == null) {
                l.priv = this;
                this.next = l;
            } else {
                l.priv = this;
                l.next = this.next;
                this.next.priv = l;
                this.next = l;
            }
            return l;
        }

        public String toString() {
            List<String> strings = new ArrayList<>();
            DualLink link = this;
            while (link != null) {
                strings.add(link.val.toString());
                link = link.next;
            }
            return strings.toString();
        }
    }

    public static ListNode mergeKLists(ListNode[] lists) {
        if (lists == null) {
            return null;
        }
        List<ListNode> nodes = new ArrayList<>();
        for (ListNode l : lists) {
            if (l != null) {
                nodes.add(l);
            }
        }
        if (nodes.size() == 0) {
            return null;
        } else if (nodes.size() == 1) {
            return nodes.get(0);
        }

        nodes.sort(Comparator.comparingInt(o -> o.val));

        ListNode node = nodes.get(0);
        DualLink link = new DualLink(node);
        DualLink startLink = link;
        DualLink endLink;
        for (int i = 1; i < nodes.size(); i++) {
            link = link.append(nodes.get(i));
        }

        endLink = link;
        link = startLink;
        ListNode startNode = null;
        ListNode privListNode = null;
        while (link != null) {
            node = new ListNode(link.peek());

            if (startNode == null) {
                startNode = node;
            } else {
                privListNode.next = node;
            }
            privListNode = node;


            if (link.deeper()) {

                int v = link.peek();
                link = link.next;
                while (link != null && link.peek() < v) {
                    link = link.next;
                }
                if (link != null) {
                    link = link.priv;
                }

                if (link != startLink && startLink.next != null) {
                    DualLink origStart = startLink;
                    startLink = startLink.next;
                    origStart.destroy();

                    if (link == null) {
                        endLink = endLink.append(origStart.val);
                    } else {
                        link.append(origStart.val);
                    }
                }

            } else {
                DualLink origStart = startLink;
                startLink = startLink.next;
                origStart.destroy();
            }
            link = startLink;


        }


        return startNode;
    }


    public static void main(String[] args) {


        System.out.println(mergeKLists(new ListNode[]{
                null
        }));
    }

}
