// https://leetcode.com/problems/summary-ranges/


import java.util.ArrayList;
import java.util.List;

public class m_228 {
    public static List<String> summaryRanges(int[] nums) {
        List<String> r = new ArrayList<>();
        if (nums.length == 0) {
            return r;
        }

        int start = nums[0];
        int priv = nums[0];

        for (int i = 1; i < nums.length; i++) {
            int num = nums[i];

            if (priv + 1 != num) {
                if (start != priv) {
                    r.add(start + "->" + priv);
                } else {
                    r.add(String.valueOf(start));
                }
                start = num;
            }
            priv = num;
        }

        if (start != priv) {
            r.add(start + "->" + priv);
        } else {
            r.add(String.valueOf(start));
        }



        return r;
    }


    public static void main(String[] args) {


        System.out.println(summaryRanges(new int[]{-1}));
        System.out.println(summaryRanges(new int[]{-2,-1,0,1,2}));
        System.out.println(summaryRanges(new int[]{-100,-1,4}));
        System.out.println(summaryRanges(new int[]{0, 1, 2, 4, 5, 7}));
        System.out.println(summaryRanges(new int[]{0, 1, 2, 4, 5, 7}));
        System.out.println(summaryRanges(new int[]{0,2,3,4,6,8,9}));
        System.out.println(summaryRanges(new int[]{}));
        System.out.println(summaryRanges(new int[]{0,1,2,3,4,6,8,9}));
    }

}
