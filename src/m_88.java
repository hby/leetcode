//https://leetcode.com/problems/merge-sorted-array/


public class m_88 {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int mm=m-1,nn=n-1;
        for(int p=m+n-1;p>=0;p--){
            if(mm<0){
                nums1[p]=nums2[nn];
                nn--;
            }else if(nn<0){
                nums1[p]=nums1[mm];
                mm--;
            }else {
                if(nums1[mm]<nums2[nn]){
                    nums1[p]=nums2[nn];
                    nn--;
                }else {
                    nums1[p]=nums1[mm];
                    mm--;
                }
            }
        }

    }


    public static void main(String[] args) {
        int[] nums1 = new int[]{1};
        int[] nums2 = new int[]{};
        merge(nums1, 1, nums2, 0);
        for (int i : nums1) {
            System.out.print(i + ",");
        }

    }

}
