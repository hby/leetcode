//https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/

import java.util.Arrays;

public class m_105 {
    public static TreeNode subBuild(int[] preorder, int[] inorder,
                                    int preOffset, int preLast,
                                    int inOffset, int inLast) {

        if (preLast - preOffset == 1) {
            return new TreeNode(preorder[preOffset]);
        } else if (preLast - preOffset > 1) {
            TreeNode root = new TreeNode();
            root.val = preorder[preOffset];

            int itemsInLeft = 0;
            while (inorder[inOffset + itemsInLeft] != root.val) {
                itemsInLeft++;
            }

            root.left = subBuild(
                    preorder,
                    inorder,
                    preOffset + 1, preOffset + itemsInLeft + 1,
                    inOffset, inOffset + itemsInLeft
            );

            root.right = subBuild(
                    preorder,
                    inorder,
                    preOffset + itemsInLeft + 1, preLast,
                    inOffset + itemsInLeft + 1, inLast
            );

            return root;
        } else {
            return null;
        }

    }


    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        return subBuild(preorder, inorder, 0, preorder.length, 0, inorder.length);
    }


    public static void main(String[] args) {

//        TreeNode x = buildTree(new int[]{3, 9, 20, 15, 7}, new int[]{9, 3, 15, 20, 7});
        TreeNode x = buildTree(new int[]{1, 2, 3}, new int[]{3, 2, 1});

        System.out.println("Hello World");
    }

}
