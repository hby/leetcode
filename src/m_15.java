import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//TODO: 时间太差，换个实现方法
//https://leetcode.com/problems/3sum/

public class m_15 {
    public static void quickSort(int[] arr, int low, int high) {
        int p, i, j, temp;

        if (low >= high) {
            return;
        }
        //p就是基准数,这里就是每个数组的第一个
        p = arr[low];
        i = low;
        j = high;
        while (i < j) {
            //右边当发现小于p的值时停止循环
            while (arr[j] >= p && i < j) {
                j--;
            }

            //这里一定是右边开始，上下这两个循环不能调换（下面有解析，可以先想想）

            //左边当发现大于p的值时停止循环
            while (arr[i] <= p && i < j) {
                i++;
            }


            temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }
        arr[low] = arr[i];//这里的arr[i]一定是停小于p的，经过i、j交换后i处的值一定是小于p的(j先走)
        arr[i] = p;
        quickSort(arr, low, j - 1);  //对左边快排
        quickSort(arr, j + 1, high); //对右边快排

    }


    public static List<List<Integer>> threeSum(int[] nums) {
        if (nums.length < 3) {
            return new ArrayList<List<Integer>>();
        }
        quickSort(nums, 0, nums.length - 1);

        int lastNegtive = -1, firstPositve = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < 0) {
                lastNegtive = i;
            } else if (nums[i] > 0) {
                firstPositve = i;
                break;
            }
        }

        if ((lastNegtive == -1 && firstPositve >= 3) || (firstPositve == -1 && lastNegtive + 3 < nums.length)) {
            return new ArrayList<List<Integer>>() {{
                add(new ArrayList<>() {{
                    add(0);
                    add(0);
                    add(0);
                }});
            }};
        } else if (lastNegtive == -1 || firstPositve == -1) {
            return new ArrayList<List<Integer>>();
        }

        int a = -1, b = -1, c = -1;
        List<List<Integer>> r = new ArrayList<List<Integer>>();

        if (firstPositve - lastNegtive > 3) { // 有超过3个0
            r.add(new ArrayList<>() {{
                add(0);
                add(0);
                add(0);
            }});
        }

        for (a = lastNegtive; a >= 0; a--) {
            for (c = firstPositve; c < nums.length; c++) {
                if (nums[a] + nums[c] == 0) { // a+c 已经为0
                    if (firstPositve - lastNegtive > 1) { // 有0
                        if ((a == 0 || nums[a - 1] != nums[a]) && (c == nums.length - 1 || nums[c + 1] != nums[c])) {
                            List<Integer> rr = new ArrayList<>();
                            rr.add(nums[a]);
                            rr.add(0);
                            rr.add(nums[c]);
                            r.add(rr);
                        }
                    }
                } else { // nums[a] + nums[c]不为0,
                    // 只处理最外侧的nums[a]和nums[c]，例如   -5,-5, -1, 0, 5时，只处理第1个-5
                    if ((a == 0 || nums[a - 1] != nums[a]) && (c == nums.length - 1 || nums[c + 1] != nums[c])) {
                        int need = -(nums[a] + nums[c]);
                        if (need > 0) { // 在  0到c之间寻找need
                            // 3个数字为  -2x  x   x 的关系时，只处理  nums[c]在最后一个x的情况。
                            // 某不为该关系，则可以处理
                            if (need != nums[c] || (c == nums.length - 1 || nums[c + 1] != need)) {
                                for (b = firstPositve; b < c; b++) {
                                    if (nums[b] == need && (b == c - 1 || nums[b] != nums[b + 1])) {
                                        List<Integer> rr = new ArrayList<>();
                                        rr.add(nums[a]);
                                        rr.add(nums[b]);
                                        rr.add(nums[c]);
                                        r.add(rr);
                                    }
                                }
                            }
                        } else { // 在 a到0之间寻找need
                            // 3个数字为 -x -x 2x 的关系时，只处理  nums[a]在第1个-x的情况。
                            // 某不为该关系，则可以处理
                            if (need != nums[a] || (a == 0 || nums[a - 1] != need)) {
                                for (b = lastNegtive; b > a; b--) {
                                    if (nums[b] == need && (b == a + 1 || nums[b] != nums[b - 1])) {
                                        List<Integer> rr = new ArrayList<>();
                                        rr.add(nums[a]);
                                        rr.add(nums[b]);
                                        rr.add(nums[c]);
                                        r.add(rr);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        return r;

    }

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6}));
    }
}
