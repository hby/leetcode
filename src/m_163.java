//https://leetcode.com/problems/compare-version-numbers/


public class m_163 {
    public static int compareVersion(String version1, String version2) {
        char[] v1 = version1.toCharArray(), v2 = version2.toCharArray();
        int l1 = version1.length(), l2 = version2.length();
        int s1, s2;
        int i1 = 0, i2 = 0;
        while (i1 < l1 || i2 < l2) {
            s1 = 0;
            s2 = 0;
            while (i1 < l1 && v1[i1] != '.') {
                s1 = s1 * 10 + (v1[i1] - '0');
                i1++;
            }
            while (i2 < l2 && v2[i2] != '.') {
                s2 = s2 * 10 + (v2[i2] - '0');
                i2++;
            }
            if (s1 > s2) {
                return 1;
            } else if (s1 < s2) {
                return -1;
            }
            i1++;
            i2++;
        }
        return 0;
    }


    public static void main(String[] args) {


        System.out.println(compareVersion("1.0.0", "1.0.01"));
    }

}
