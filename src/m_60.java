//https://leetcode.com/problems/permutation-sequence/


import java.util.ArrayList;
import java.util.List;

public class m_60 {

    public static String g(int n, int k) {
        int[] magic = new int[]{1, 1, 2, 6, 24, 120, 720, 5040, 40320}; // 0! 1! 2! 3! ... 8!
        List<Integer> remain = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            remain.add(i);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = n; i > 0; i--) {
            int p = k / magic[i - 1];
            if (p * magic[i - 1] == k && k != 0) {
                p--;
            }
            sb.append(remain.get(p));
            remain.remove(p);
            k = k - p * magic[i - 1];
        }
        return sb.toString();


    }


    public static void main(String[] args) {

//        System.out.println(g(9, 99999));
//        System.out.println(g(3, 3));
//        System.out.println(g(4, 9));
//        System.out.println(g(3, 1));
        System.out.println(g(3, 2));
    }

}
