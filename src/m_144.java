//https://leetcode.com/problems/binary-tree-preorder-traversal/


import java.util.LinkedList;
import java.util.List;

public class m_144 {
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> results = new LinkedList<>();
        sub(root, results);
        return results;
    }

    public static void sub(TreeNode root, List<Integer> results) {
        if (root != null) {
            results.add(root.val);
            sub(root.left, results);
            sub(root.right, results);
        }
    }

    public static void main(String[] args) {


        System.out.println(preorderTraversal(TreeNode.gen(1, null, 2, 3)));
    }

}
