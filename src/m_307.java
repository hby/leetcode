// TODO: https://leetcode.com/problems/range-sum-query-mutable/

public class m_307 {
    static class NumArray {
        public int[] sumNums;

        public int[] origNums;
        public int[] diffs;

        public NumArray(int[] nums) {
            this.origNums = nums;
            this.diffs = new int[nums.length];

            this.sumNums = new int[nums.length];
            this.sumNums[0] = nums[0];
            for (int i = 1; i < this.sumNums.length; i++) {
                this.sumNums[i] = this.sumNums[i - 1] + this.origNums[i];
            }
        }

        public void update(int index, int val) {
            diffs[index] = val - this.origNums[index];
            this.origNums[index] = val;
        }

        public int sumRange(int left, int right) {
            int diff = 0;

            for (int i = left; i <= right; i++) {
                if (diffs[i] != 0) {
                    diff += diffs[i];
                }
            }

            if (left == 0) {
                return diff + this.sumNums[right];
            } else {
                return diff + this.sumNums[right] - this.sumNums[left - 1];
            }
        }
    }


    public static void main(String[] args) {
        NumArray obj;

//        obj = new NumArray(new int[]{9, -8});
//        obj.update(0, 3);
//
//        System.out.println(obj.sumRange(1, 1)); // -8
//        System.out.println(obj.sumRange(0, 1));//-5
//        obj.update(1, -3);
//        System.out.println(obj.sumRange(0, 1));//0


        obj = new NumArray(new int[]{7, 2, 7, 2, 0});
        obj.update(4, 6);
        obj.update(0, 2);
        obj.update(0, 9);

//        System.out.println(obj.sumRange(4, 4)); // 6

        obj.update(3, 8);
        System.out.println(obj.sumRange(0, 4));//32

        obj.update(4, 1);
        System.out.println(obj.sumRange(0, 3));//26
        System.out.println(obj.sumRange(0, 4));//27
        obj.update(0, 4);

    }

}
