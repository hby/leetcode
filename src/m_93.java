//https://leetcode.com/problems/restore-ip-addresses/


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class m_93 {
    public static boolean isValid(char[] cs, int from, int to) {
        if (to - from > 2) {
            return false;
        } else if (to - from == 2) {
            if (cs[from] == '1') {
                return true;
            } else if (cs[from] == '2') {
                if (cs[from + 1] < '5') {
                    return true;
                } else if (cs[from + 1] == '5') {
                    return cs[from + 2] < '6';
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if (to - from == 1) {
            return cs[from] != '0';
        } else {
            return true;
        }
    }

    public static int[] possible(char[] cs, int from, int to) {
        int[] results = new int[to - from];
        results[0] = -1;

        int p = 0;
        for (int cut = from; cut < to; cut++) {
            if (isValid(cs, from, cut) && isValid(cs, cut + 1, to)) {
                results[p] = cut;
                p++;
            }
        }
        return results;
    }

    public static List<String> restoreIpAddresses(String s) {
        List<String> arr = new LinkedList<>();
        char[] cs = s.toCharArray();
        int len = cs.length;
        if (len >= 4 && len <= 12) {
            for (int cut = 1; cut < len - 2; cut++) {
                int[] left = possible(cs, 0, cut);
                int[] right = possible(cs, cut+1, len - 1);

                if (left[0] != -1 && right[0] != -1) {
                    for (int i = 0; i < left.length; i++) {
                        if ((i > 0 && left[i] == 0)) {
                            break;
                        }
                        for (int j = 0; j < right.length; j++) {
                            if ((j > 0 && right[j] == 0)) {
                                break;
                            }
                            String sb = String.valueOf(cs, 0, left[i]+1) + '.' +
                                    String.valueOf(cs, left[i]+1, cut - left[i]) + '.' +
                                    String.valueOf(cs, cut+1, right[j] - cut) + '.' +
                                    String.valueOf(cs, right[j]+1, len - right[j]-1);
                            arr.add(sb);
                        }
                    }
                }
            }
        }
        return arr;

    }


    public static void main(String[] args) {


        System.out.println(restoreIpAddresses("25525511135").toString());
    }

}
