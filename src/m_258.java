//https://leetcode.com/problems/add-digits/description/

public class m_258 {
    public static int addDigits(int num) {
        if (num < 10) {
            return num;
        } else {
            return addDigits(num % 10 + addDigits(num / 10));
        }
    }


    public static void main(String[] args) {


        System.out.println(addDigits(38));
    }

}
