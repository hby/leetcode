//https://leetcode.com/problems/max-points-on-a-line/


import java.util.HashMap;
import java.util.Map;


public class m_149 {
    public static int maxCommonDivisor(int m, int n) {
        if (m < n) {// 保证m>n,若m<n,则进行数据交换
            int temp = m;
            m = n;
            n = temp;
        }
        while (m % n != 0) {// 在余数不能为0时,进行循环
            int temp = m % n;
            m = n;
            n = temp;
        }
        return n;// 返回最大公约数
    }


    public int maxPoints(int[][] points) {
        if (points.length == 1) {
            return 1;
        }
        int sameLineCount = 1;
        Map<Integer, Map<Integer, Integer>> kbmap = new HashMap<>();
        Map<Integer, Integer> xmap = new HashMap<>();

        for (int i = 0; i < points.length; i++) { // x1: points[i][0]   y1: points[i][1]
            for (int j = i + 1; j < points.length; j++) { // x2:  points[j][0]   y2: points[j][1]
                int x1 = points[i][0], x2 = points[j][0];
                int y1 = points[i][1], y2 = points[j][1];

                if (x1 == x2) {
                    if (xmap.containsKey(x1)) {
                        int l = 1 + xmap.get(x1);
                        xmap.put(x1, l);
                        if (l > sameLineCount) {
                            sameLineCount = l;
                        }
                    } else {
                        xmap.put(x1, 1);
                    }
                } else {
                    int k = 0;
                    if (y1 != y2) {
                        int p = maxCommonDivisor(Math.abs(y1 - y2), Math.abs(x1 - x2));
                        k = Math.abs(y1 - y2) / p * 100000 + Math.abs(x1 - x2) / p;
                        if ((y1 - y2) * (x1 - x2) < 0) {
                            k = -k;
                        }
                    }
                    int aa = (x2 - x1) * y2 - (y2 - y1) * x2, bb = x2 - x1;
                    int b=0;
                    if(aa!=0){
                        int p = maxCommonDivisor(Math.abs(aa), Math.abs(bb));
                        b = Math.abs(aa) / p * 100000 + Math.abs(bb) / p;
                        if (aa * bb < 0) {
                            b = -b;
                        }
                    }


                    if (!kbmap.containsKey(k)) {
                        Map<Integer, Integer> m = new HashMap<>();
                        m.put(b, 1);
                        kbmap.put(k, m);
                    } else if (!kbmap.get(k).containsKey(b)) {
                        kbmap.get(k).put(b, 1);
                    } else {
                        int l = 1 + kbmap.get(k).get(b);
                        kbmap.get(k).put(b, l);
                        if (l > sameLineCount) {
                            sameLineCount = l;
                        }
                    }
                }


            }
        }

        return ((int) (1 + Math.sqrt(1 + 8 * sameLineCount))) / 2;

    }


    public static void main(String[] args) {
        m_149 instance = new m_149();


        System.out.println(instance.maxPoints(new int[][]{
                new int[]{1, 3},
                new int[]{2, 3},
                new int[]{5, 3},
        }));


//        System.out.println(maxPoints(new int[][]{
//                new int[]{1,2},
//        }));
    }

}
