//https://leetcode.com/problems/subsets-ii/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class m_90 {
    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        // every number in `nums` range from -10 ~ +10
        // numUseLimit[0] = count for 10, numUseLimit[1] = count for 9 ... numUseLimit[20] = count for -10
        int[] numUseLimit = new int[21];
        for (int num : nums) {
            numUseLimit[10 - num]++;
        }

        //Then, we change numUseLimit represents for how many possible combinations
        //if use no greater than i
        numUseLimit[20] += 1;
        for (int i = 19; i >= 0; i--) {
            numUseLimit[i] = (numUseLimit[i] + 1) * numUseLimit[i + 1];
        }

        List<List<Integer>> r = new ArrayList<>(numUseLimit[0]);
        for (int j = 0; j < numUseLimit[0]; j++) {
            List<Integer> a = new ArrayList<>();

            int remain = j;
            for (int i = 0; i < 21 && remain > 0; i++) {
                int b = remain / numUseLimit[i];
                while (b > 0) {
                    a.add(11 - i);
                    b--;
                }
                remain -= ((remain / numUseLimit[i]) * numUseLimit[i]);
            }
            r.add(a);
        }
        return r;
    }


    public static void main(String[] args) {
//        List<List<Integer>> a = subsetsWithDup(new int[]{1, 2, 2});
        List<List<Integer>> a = subsetsWithDup(new int[]{-9, -8, -8, -7, -7, -7,});
        System.out.println(a.size());
        for (List<Integer> x : a) {
            System.out.println(Arrays.toString(x.toArray()));
        }


    }

}
