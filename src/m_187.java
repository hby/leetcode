//https://leetcode.com/problems/repeated-dna-sequences/


import java.util.*;

public class m_187 {
    public static String convert(int i) {
        char[] cs = new char[10];
        for (int j = 0; j < 10; j++) {
            int p = (i << (12 + j * 2)) >>> 30;
            switch (p) {
                case 0:
                    cs[j] = 'A';
                    break;
                case 1:
                    cs[j] = 'C';
                    break;
                case 2:
                    cs[j] = 'G';
                    break;
                case 3:
                    cs[j] = 'T';
                    break;
                default:
                    cs[j] = '?';
            }
        }
        return new String(cs);
    }

    public static List<String> findRepeatedDnaSequences(String s) {
        if (s.length() <= 10) {
            return new LinkedList<>();
        }

        Set<Integer> sequences = new HashSet<>();
        Set<String> rs = new HashSet<>();

        int window = 0;
        for (int i = 0; i < 10; i++) {
            switch (s.charAt(i)) {
                case 'A':
                    window += 0;
                    break;
                case 'C':
                    window += 1;
                    break;
                case 'G':
                    window += 2;
                    break;
                case 'T':
                    window += 3;
                    break;
            }
            window = window << 2;
        }
        window = window>>2;
        sequences.add(window);

        for (int j = 10; j < s.length(); j++) {
            window = (window << 14) >>> 12;
            switch (s.charAt(j)) {
                case 'A':
                    window += 0;
                    break;
                case 'C':
                    window += 1;
                    break;
                case 'G':
                    window += 2;
                    break;
                case 'T':
                    window += 3;
                    break;
            }
            if (sequences.contains(window)) {
                rs.add(convert(window));
            } else {
                sequences.add(window);
            }
        }
        return new LinkedList<>(rs);
    }


    public static void main(String[] args) {


        System.out.println(findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"));
    }

}
