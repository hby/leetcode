import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

class Result {

    public static int stockPairs(List<Integer> stocksProfit, long target) {
        // Write your code here
        List<Integer> arrayed = new ArrayList<>(stocksProfit);
        Collections.sort(arrayed);

        return subPairs(arrayed, 0, arrayed.size() - 1, target);
    }

    public static boolean quickSearch(List<Integer> stocksProfit, int fromIndex, int toIndex, long target) {
        if (toIndex < fromIndex) {
            return false;
        } else if (toIndex == fromIndex) {
            return stocksProfit.get(fromIndex) == target;
        }
        if (toIndex - fromIndex == 1) {
            return stocksProfit.get(fromIndex) == target || stocksProfit.get(toIndex) == target;
        } else {
            int cut = (fromIndex + toIndex) / 2;
            int cutVal = stocksProfit.get(cut);
            if (cutVal > target) {
                return quickSearch(stocksProfit, fromIndex, cut, target);
            } else if (cutVal < target) {
                return quickSearch(stocksProfit, cut, toIndex, target);
            } else {
                return true;
            }
        }
    }

    public static int subPairs(List<Integer> stocksProfit, int fromIndex, int toIndex, long target) {
        int total = 0;

        for (int cut = fromIndex; cut <= toIndex; cut++) {
            if (cut + 1 <= toIndex && stocksProfit.get(cut).equals(stocksProfit.get(cut + 1))) {
                continue;
            }
            if (quickSearch(stocksProfit, cut + 1, toIndex, target - stocksProfit.get(cut))) {
                total++;
            }
        }
        if(quickSearch(stocksProfit,fromIndex,toIndex,target/2)){
            total++;
        }
        return total;

    }
}

public class A {
    public static void main(String[] args) {
//        System.out.println(Result.stockPairs(List.of(1, 3, 46, 1, 3, 9), 47));
        System.out.println(Result.stockPairs(List.of(6,12,3,9,3,5,1), 12));
//        System.out.println(Result.stockPairs(List.of(407, 1152, 403, 1419, 689, 1029, 108, 128, 1307, 300, 775, 622, 730, 978,
//                526, 943, 127, 566, 869, 715, 983, 820, 1394, 901, 606, 497, 98, 1222, 843, 600, 1153, 302, 1450, 1457, 973, 1431, 217, 936, 958, 1258,
//                970, 1155, 1061, 1341, 657, 333, 1151, 790, 101, 588, 263, 101, 534, 747, 405, 585, 111, 849, 695, 1256, 1508, 139, 336, 1430, 615, 1295,
//                550, 783, 575, 992, 709, 828, 1447, 1457, 738, 1024, 529, 406, 164, 994, 1008, 50, 811, 564, 580, 952, 768, 863, 1225, 251, 1032, 1460), 1558));
    }
}
