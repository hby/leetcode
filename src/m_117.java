//https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/


import java.util.ArrayList;
import java.util.List;

public class m_117 {



    public static void appendCousin(List<Node> childCousins, Node p) {
        if (p != null) {
            childCousins.add(p);
        }
    }

    public static void sub(Node root, List<Node> cousins) {
        if (root == null) {
            return;
        }

        if (cousins.size() > 0) {
            root.next = cousins.get(0);
        }

        List<Node> childCousins = new ArrayList<>();

        List<Node> forLeft = new ArrayList<>();
        if (root.left != null && root.right != null) {
            appendCousin(forLeft, root.right);
            for (Node cousin : cousins) {
                appendCousin(childCousins, cousin.left);
                appendCousin(childCousins, cousin.right);
                appendCousin(forLeft, cousin.left);
                appendCousin(forLeft, cousin.right);
            }
        } else {
            for (Node cousin : cousins) {
                appendCousin(childCousins, cousin.left);
                appendCousin(childCousins, cousin.right);
            }
        }


        if (root.left != null && root.right != null) {
            sub(root.left, forLeft);
            sub(root.right, childCousins);
        } else if (root.left != null) { // && root.right==null
            sub(root.left, childCousins);
        } else if (root.right != null) { // && root.left == null
            sub(root.right, childCousins);
        }


    }

    public static Node connect(Node root) {
        sub(root, new ArrayList<>());
        return root;
    }


    public static void main(String[] args) {
        Node node = new Node(1,
                new Node(2, new Node(4, new Node(7), null, null), new Node(5), null),
                new Node(3, null, new Node(6, null, new Node(8), null), null),
                null);
        connect(node);

        System.out.println("Hello World");
    }

}
