//https://leetcode.com/problems/length-of-last-word/


public class m_58 {
    public static int lengthOfLastWord(String s) {
        byte[] bytes = s.getBytes();
        int len = 0;
        for (int i = bytes.length - 1; i >= 0; i--) {
            if (bytes[i] == ' ' && len > 0) {
                return len;
            } else if (bytes[i] != ' ') {
                len++;
            }
        }
        return len;
    }


    public static void main(String[] args) {


        System.out.println(lengthOfLastWord("Hello World"));
        System.out.println(lengthOfLastWord(""));
        System.out.println(lengthOfLastWord(" "));
        System.out.println(lengthOfLastWord("a "));
        System.out.println(lengthOfLastWord(" a "));
    }

}
