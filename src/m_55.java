//https://leetcode.com/problems/jump-game/
//TODO

import java.util.List;

public class m_55 {

    public static boolean sub(int[] nums, int from, int longest) {
        if (from + longest >= nums.length-1) {
            return true;
        } else if (longest == 0) {
            return false;
        } else {
            for (int i = from + longest; i > from; i--) {
                if (sub(nums, i, nums[i])) {
                    return true;
                }
            }
            return false;
        }
    }

    public static boolean canJump(int[] nums) {
        return sub(nums, 0, nums[0]);
    }


    public static void main(String[] args) {


//        System.out.println(canJump(new int[]{1, 3, 2, 1}));
//        System.out.println(canJump(new int[]{3,2,1,0,4}));
        System.out.println(canJump(new int[]{1, 1, 1, 0}));
//        System.out.println(canJump(new int[]{0}));
    }

}
