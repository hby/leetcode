//https://leetcode.com/problems/maximum-number-of-balloons/


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class m_1189 {
    public static int maxNumberOfBalloons(String text) {
        int[] arr = new int[5];
        for (char c : text.toCharArray()) {
            switch (c) {
                case 'b':
                    arr[0]++;
                    break;
                case 'a':
                    arr[1]++;
                    break;
                case 'l':
                    arr[2]++;
                    break;
                case 'o':
                    arr[3]++;
                    break;
                case 'n':
                    arr[4]++;
                    break;
                default:
                    break;

            }
        }
        arr[2] /= 2;
        arr[3] /= 2;
        int min = 1000000;
        for(int i=0;i<5;i++){
            min = Math.min(min, arr[i]);
        }
        return min;

    }


    public static void main(String[] args) {


        System.out.println(maxNumberOfBalloons("loonbalxballpoon"));
    }

}
