//https://leetcode.com/problems/search-insert-position/

public class m_35 {
    public static int subSearch(int[] nums, int target, int startIndex, int endIndex) {
        if (endIndex == startIndex) {
            if (nums[startIndex] < target) {
                return startIndex + 1;
            } else {
                return startIndex;
            }
        } else if (endIndex - startIndex == 1) {
            if (nums[endIndex] < target) {
                return endIndex + 1;
            } else if (nums[startIndex] < target) {
                return endIndex;
            } else {
                return startIndex;
            }
        } else {
            int cut = startIndex + (endIndex - startIndex) / 2;
            if (target < nums[startIndex]) {
                return 0;
            } else if (nums[startIndex] <= target && target < nums[cut]) {
                return subSearch(nums, target, startIndex, cut);
            } else if (nums[cut] <= target && target < nums[endIndex]) {
                return subSearch(nums, target, cut, endIndex);
            } else if (target == nums[endIndex]) {
                return endIndex;
            } else {
                return endIndex + 1;
            }

        }
    }

    public static int searchInsert(int[] nums, int target) {
        return subSearch(nums, target, 0, nums.length - 1);
    }


    public static void main(String[] args) {


        System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 5));//2
        System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 2));//1
        System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 7));//4
        System.out.println(searchInsert(new int[]{1, 3, 5, 6}, 0));//0
        System.out.println(searchInsert(new int[]{1}, 0));//0
        System.out.println(searchInsert(new int[]{1}, 2));//1
    }

}
