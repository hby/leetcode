//https://leetcode.com/problems/reverse-only-letters/


public class m_917 {
    public static String reverseOnlyLetters(String s) {
        char[] cs = s.toCharArray();
        int i = 0, j = cs.length - 1;
        while (i < cs.length && j >= 0 && i <= j) {
            if (!((cs[i] >= 'a' && cs[i] <= 'z') || (cs[i] >= 'A' && cs[i] <= 'Z'))) {
                i++;
            } else if (!((cs[j] >= 'a' && cs[j] <= 'z') || (cs[j] >= 'A' && cs[j] <= 'Z'))) {
                j--;
            } else {
                char c = cs[i];
                cs[i] = cs[j];
                cs[j] = c;
                i++;
                j--;
            }
        }
        return new String(cs);
    }


    public static void main(String[] args) {


        System.out.println(reverseOnlyLetters("Test1ng-Leet=code-Q!"));
    }

}
