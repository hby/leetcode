// https://leetcode.com/problems/search-a-2d-matrix-ii/


public class m_240 {
    public static boolean searchMatrix(int[][] matrix, int target) {
        int cc = matrix.length, rc = matrix[0].length;

        return subSearch(matrix, 0, cc - 1, 0, rc - 1, target);
    }

    public static boolean subSearch(int[][] matrix, int colFrom, int colTo, int rowFrom, int rowTo, int target) {

        if (matrix[colFrom][rowFrom] > target || matrix[colTo][rowTo] < target) {
            return false;
        }

        if (colTo - colFrom <= 1 && rowTo - rowFrom <= 1) { // a square (length=1) or a single point
            return matrix[colFrom][rowFrom] == target || matrix[colFrom][rowTo] == target ||
                    matrix[colTo][rowFrom] == target || matrix[colTo][rowTo] == target;
        }

        int colCut = (colFrom + colTo) / 2, rowCut = (rowFrom + rowTo) / 2;

        if (subSearch(matrix, colCut, colTo, rowFrom, rowCut, target) || subSearch(matrix, colFrom, colCut, rowCut, rowTo, target)) {
            return true;
        } else {
            if (matrix[colCut][rowCut] < target) {
                return subSearch(matrix, colCut, colTo, rowCut, rowTo, target);
            } else {
                return subSearch(matrix, colFrom, colCut, rowFrom, rowCut, target);
            }
        }


    }


    public static void main(String[] args) {

        System.out.println(searchMatrix(
                new int[][]{
                        new int[]{5, 6, 10, 14},
                        new int[]{6, 10, 13, 18},
                        new int[]{10, 13, 18, 19},
                }, 14));

//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{1, 2, 3, 4, 5},
//                        new int[]{6, 7, 8, 9, 10},
//                        new int[]{11, 12, 13, 14, 15},
//                        new int[]{16, 17, 18, 19, 20},
//                        new int[]{21, 22, 23, 24, 25},
//                }, 5));

//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{1, 2, 3, 4, 5},
//                        new int[]{6, 7, 8, 9, 10},
//                        new int[]{11, 12, 13, 14, 15},
//                        new int[]{16, 17, 18, 19, 20},
//                        new int[]{21, 22, 23, 24, 25},
//                }, 19));


//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{-1, 3},
//                }, 1));
//
//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{1, 4, 7, 11, 15},
//                        new int[]{2, 5, 8, 12, 19},
//                        new int[]{3, 6, 9, 16, 22},
//                        new int[]{10, 13, 14, 17, 24},
//                        new int[]{18, 21, 23, 26, 30},
//                }, 5));
//
//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{1, 4, 7, 11},
//                        new int[]{2, 5, 8, 12},
//                        new int[]{3, 6, 9, 16},
//                        new int[]{10, 13, 14, 17},
//                        new int[]{18, 21, 23, 26},
//                }, 5));
//
//
//        System.out.println(searchMatrix(
//                new int[][]{
//                        new int[]{1, 4, 7, 11},
//                        new int[]{2, 5, 8, 12},
//                }, 5));

    }

}
