//https://leetcode.com/problems/path-sum-ii/


import java.util.*;

public class m_113 {
    public static boolean isLeaf(TreeNode root) {
        if (root != null) {
            return root.left == null && root.right == null;
        } else {
            return false;
        }
    }

    public static boolean sub(TreeNode root, int targetSum, List<Integer> path, List<List<Integer>> results) {
        boolean b;
        path.add(root.val);
        if (isLeaf(root)) {
            b = targetSum == root.val;
            if (b) {
                Integer[] l = new Integer[path.size()];
                results.add(Arrays.asList(path.toArray(l)));
            }
        } else {
            if (root.left == null && root.right != null) {
                b = sub(root.right, targetSum - root.val, path, results);
            } else if (root.right == null) {
                b = sub(root.left, targetSum - root.val, path, results);
            } else {
                boolean b1 = sub(root.left, targetSum - root.val, path, results);
                boolean b2 = sub(root.right, targetSum - root.val, path, results);
                b = b1 || b2;
            }
        }

        path.remove(path.size() - 1);
        return b;
    }


    public static List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return new ArrayList<>();
        } else {
            List<List<Integer>> results = new LinkedList<>();
            sub(root, targetSum, new ArrayList<>(), results);
            return results;
        }

    }


    public static void main(String[] args) {
        System.out.println(pathSum(TreeNode.gen(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1), 22));
        System.out.println(pathSum(TreeNode.gen(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1), 22));


        System.out.println("Hello World");
    }

}
