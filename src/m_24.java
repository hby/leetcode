//https://leetcode.com/problems/swap-nodes-in-pairs/

public class m_24 {
    public static void swap(ListNode a, ListNode b) {
        int t = a.val;
        a.val = b.val;
        b.val = t;
    }

    public static ListNode swapPairs(ListNode head) {

        ListNode current = head;
        ListNode priv = null;

        int counter = 0;

        while (current != null) {
            if (counter % 2 == 1) {
                swap(priv, current);
            } else {
                priv = current;
            }
            counter++;
            current = current.next;
        }

        return head;
    }

    public static void main(String[] args) {


        System.out.println(swapPairs(new ListNode(1,2,3,4,5,6,7)));
    }

}
