//https://leetcode.com/problems/pascals-triangle/


import java.util.*;

public class m_118 {
    public static List<Integer> sub(List<Integer> priv) {
        List<Integer> curr = new ArrayList<>();

        curr.add(1);
        for (int i = 0; i < priv.size() - 1; i++) {
            curr.add(priv.get(i) + priv.get(i + 1));
        }
        curr.add(1);

        return curr;
    }

    public static List<List<Integer>> generate(int numRows) {

        List<List<Integer>> results = new LinkedList<>();
        List<Integer> priv = new ArrayList<>() {{
            add(1);
        }};
        results.add(priv);
        for (int i = 1; i < numRows; i++) {
            List<Integer> curr = sub(priv);
            results.add(curr);
            priv = curr;
        }
        return results;

    }


    public static void main(String[] args) {


        System.out.println(generate(1));
        System.out.println(generate(2));
        System.out.println(generate(3));
        System.out.println(generate(4));
    }

}
