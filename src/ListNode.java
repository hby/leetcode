import java.util.Arrays;

public class ListNode {
    public int val;
    public ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    ListNode(int... arrs) {
        if (arrs.length == 1) {
            this.val = arrs[0];
            this.next = null;
        } else if (arrs.length >= 1) {
            this.val = arrs[0];
            this.next = new ListNode(Arrays.copyOfRange(arrs, 1, arrs.length));
        }
    }

    public String toString() {
//        return String.valueOf(this.val);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        ListNode node = this;
        while (node != null && i < 30) {
            sb.append(node.val);
            sb.append(',');
            node = node.next;
            i++;
        }
        return sb.toString();
    }
}