//https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m_82 {
    public static ListNode firstUnique(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        if (head.val != head.next.val) {
            return head;
        }

        int v = head.val;
        while (head != null && head.val == v) {
            head = head.next;
        }
        if (head == null || head.next == null) {
            return head;
        } else if (head.val == head.next.val) {
            return firstUnique(head);
        } else {
            return head;
        }
    }


    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        head = firstUnique(head);
        if (head != null) {
            head.next = deleteDuplicates(head.next);
        }

        return head;
    }


    public static void main(String[] args) {


        System.out.println(deleteDuplicates(new ListNode(1, 2, 3, 3, 4, 4, 5)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 3)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 3, 3)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 2, 3, 3)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 2, 2, 2, 3, 3, 3)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5)));

    }

}
