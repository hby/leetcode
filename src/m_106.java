//https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/


public class m_106 {
    public static TreeNode buildTree(int[] inorder, int[] postorder) {
        return sub(inorder, postorder,
                0, inorder.length,
                0, postorder.length);
    }

    public static TreeNode sub(int[] inorder, int[] postorder,
                               int inOffset, int inLast,
                               int postOffset, int postLast) {

        if (inLast - inOffset == 1) {
            return new TreeNode(inorder[inOffset]);
        } else if (inLast - inOffset > 1) {
            int rootVal = postorder[postLast-1];

            int leftCount = 0;
            while (inOffset + leftCount<inLast-1 && inorder[inOffset + leftCount] != rootVal) {
                leftCount++;
            }
            TreeNode left = sub(inorder, postorder, inOffset, inOffset + leftCount, postOffset, postOffset + leftCount);
            TreeNode root = new TreeNode(rootVal);

            int rightCount = 0;
            while (postorder[postOffset + leftCount + rightCount] != inorder[inOffset + leftCount]) {
                rightCount++;
            }
            TreeNode right = sub(inorder, postorder,
                    inOffset + leftCount+1, inOffset + leftCount + rightCount+1,
                    postOffset + leftCount, postOffset + leftCount + rightCount);
            root.left = left;
            root.right = right;
            return root;


        } else {
            return null;
        }
    }


    public static void main(String[] args) {

//        TreeNode x = buildTree(new int[]{3, 9, 20, 15, 7}, new int[]{9, 3, 15, 20, 7});
//        TreeNode x = buildTree(new int[]{9, 3, 15, 20, 7}, new int[]{9, 15, 7, 20, 3});
        TreeNode x = buildTree(new int[]{2,3,1}, new int[]{3,2,1});

        System.out.println("Hello World");
    }

}
