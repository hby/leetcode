//https://leetcode.com/problems/find-peak-element/


public class m_162 {
    public static int sub(int[] nums,int start,int end){
        if(start==end){
            return start;
        }else {
            int m = (start+end)/2;
            if(nums[m]<nums[m+1]){
                return sub(nums,m+1,end);
            }else {
                return sub(nums,start,m);
            }
        }
    }

    public static int findPeakElement(int[] nums) {
        return sub(nums,0,nums.length-1);
    }


    public static void main(String[] args){



        System.out.println(findPeakElement(new int[]{1,2,1,3,5,6,4}));
    }

}
