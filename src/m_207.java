//https://leetcode.com/problems/course-schedule/


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class m_207 {
    public static boolean finishCourse(boolean[] finished, Map<Integer, Set<Integer>> reqs,Set<Integer> preCourseNeed, int targetCourse) {
        if (!finished[targetCourse]) {
            if (!reqs.containsKey(targetCourse)) {
                finished[targetCourse] = true;
            } else {
                Set<Integer> thisCourseNeed = reqs.get(targetCourse);
                for(int courseNeed:thisCourseNeed){
                    if(preCourseNeed.contains(courseNeed)){
                        return false;
                    }
                    preCourseNeed.add(targetCourse);
                    if(!finishCourse(finished,reqs,preCourseNeed,courseNeed)){
                        return false;
                    }
                }

            }
        }
        preCourseNeed.clear();

        return true;
    }

    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, Set<Integer>> req = new HashMap<>();
        boolean[] finished = new boolean[numCourses];
        for (int[] pre : prerequisites) {
            if (req.containsKey(pre[0])) {
                Set<Integer> rs = req.get(pre[0]);
                rs.add(pre[1]);
            } else {
                Set<Integer> rs = new HashSet<>() {{
                    add(pre[1]);
                }};
                req.put(pre[0], rs);
            }
        }

        for (int i = 0; i < numCourses; i++) {
            if(!finishCourse(finished,req,new HashSet<>(),i)){
                return false;
            }else {
                req.remove(i);
            }
        }


        return true;
    }


    public static void main(String[] args) {


        System.out.println(canFinish(100, new int[][]{
                new int[]{1,0},new int[]{2,0},new int[]{2,1},new int[]{3,1},new int[]{3,2},
                new int[]{4,2},new int[]{4,3},new int[]{5,3},new int[]{5,4},new int[]{6,4},
                new int[]{6,5},new int[]{7,5},new int[]{7,6},new int[]{8,6},new int[]{8,7},
                new int[]{9,7},new int[]{9,8},new int[]{10,8},new int[]{10,9},new int[]{11,9},
                new int[]{11,10},new int[]{12,10},new int[]{12,11},new int[]{13,11},new int[]{13,12},
                new int[]{14,12},new int[]{14,13},new int[]{15,13},new int[]{15,14},new int[]{16,14},
                new int[]{16,15},new int[]{17,15},new int[]{17,16},new int[]{18,16},new int[]{18,17},
                new int[]{19,17},new int[]{19,18},new int[]{20,18},new int[]{20,19},new int[]{21,19},
                new int[]{21,20},new int[]{22,20},new int[]{22,21},new int[]{23,21},new int[]{23,22},
                new int[]{24,22},new int[]{24,23},new int[]{25,23},new int[]{25,24},new int[]{26,24},
                new int[]{26,25},new int[]{27,25},new int[]{27,26},new int[]{28,26},new int[]{28,27},
                new int[]{29,27},new int[]{29,28},new int[]{30,28},new int[]{30,29},new int[]{31,29},
                new int[]{31,30},new int[]{32,30},new int[]{32,31},new int[]{33,31},new int[]{33,32},
                new int[]{34,32},new int[]{34,33},new int[]{35,33},new int[]{35,34},new int[]{36,34},
                new int[]{36,35},new int[]{37,35},new int[]{37,36},new int[]{38,36},new int[]{38,37},
                new int[]{39,37},new int[]{39,38},new int[]{40,38},new int[]{40,39},new int[]{41,39},
                new int[]{41,40},new int[]{42,40},new int[]{42,41},new int[]{43,41},new int[]{43,42},
                new int[]{44,42},new int[]{44,43},new int[]{45,43},new int[]{45,44},new int[]{46,44},
                new int[]{46,45},new int[]{47,45},new int[]{47,46},new int[]{48,46},new int[]{48,47},
                new int[]{49,47},new int[]{49,48},new int[]{50,48},new int[]{50,49},new int[]{51,49},
                new int[]{51,50},new int[]{52,50},new int[]{52,51},new int[]{53,51},new int[]{53,52},
                new int[]{54,52},new int[]{54,53},new int[]{55,53},new int[]{55,54},new int[]{56,54},
                new int[]{56,55},new int[]{57,55},new int[]{57,56},new int[]{58,56},new int[]{58,57},
                new int[]{59,57},new int[]{59,58},new int[]{60,58},new int[]{60,59},new int[]{61,59},
                new int[]{61,60},new int[]{62,60},new int[]{62,61},new int[]{63,61},new int[]{63,62},
                new int[]{64,62},new int[]{64,63},new int[]{65,63},new int[]{65,64},new int[]{66,64},
                new int[]{66,65},new int[]{67,65},new int[]{67,66},new int[]{68,66},new int[]{68,67},
                new int[]{69,67},new int[]{69,68},new int[]{70,68},new int[]{70,69},new int[]{71,69},
                new int[]{71,70},new int[]{72,70},new int[]{72,71},new int[]{73,71},new int[]{73,72},
                new int[]{74,72},new int[]{74,73},new int[]{75,73},new int[]{75,74},new int[]{76,74},
                new int[]{76,75},new int[]{77,75},new int[]{77,76},new int[]{78,76},new int[]{78,77},
                new int[]{79,77},new int[]{79,78},new int[]{80,78},new int[]{80,79},new int[]{81,79},
                new int[]{81,80},new int[]{82,80},new int[]{82,81},new int[]{83,81},new int[]{83,82},
                new int[]{84,82},new int[]{84,83},new int[]{85,83},new int[]{85,84},new int[]{86,84},
                new int[]{86,85},new int[]{87,85},new int[]{87,86},new int[]{88,86},new int[]{88,87},
                new int[]{89,87},new int[]{89,88},new int[]{90,88},new int[]{90,89},new int[]{91,89},
                new int[]{91,90},new int[]{92,90},new int[]{92,91},new int[]{93,91},new int[]{93,92},
                new int[]{94,92},new int[]{94,93},new int[]{95,93},new int[]{95,94},new int[]{96,94},
                new int[]{96,95},new int[]{97,95},new int[]{97,96},new int[]{98,96},new int[]{98,97},
                new int[]{99,97}
        }));
    }

}
