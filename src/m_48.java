//https://leetcode.com/problems/rotate-image/


public class m_48 {
    public static void rotateLayer(int[][] matrix, int layer) {
        int l = matrix.length - (layer - 1) * 2;
        int up = layer - 1, left = layer - 1, right = matrix.length - layer, down = matrix.length - layer;
        for (int i = 1; i < l; i++) {
            int col_1 = up, row_1 = left + (i - 1);
            int col_2 = up + (i - 1), row_2 = right;
            int col_3 = down, row_3 = right - (i - 1);
            int col_4 = down - (i - 1), row_4 = left;
            int tmp = matrix[col_4][row_4];
            matrix[col_4][row_4]= matrix[col_3][row_3];
            matrix[col_3][row_3]= matrix[col_2][row_2];
            matrix[col_2][row_2]= matrix[col_1][row_1];
            matrix[col_1][row_1]= tmp;
        }


    }

    public static void rotate(int[][] matrix) {
        int l = matrix.length;
        for (int i = 1; i <= (l + 1) / 2; i++) {
            rotateLayer(matrix, i);
        }

    }


    public static void main(String[] args) {
        int[][] matrix = new int[][]{new int[]{5,1,9,11},new int[]{2,4,8,10},new int[]{13,3,6,7},new int[]{15,14,12,16}};
        rotate(matrix);
        for(int i=0;i< matrix.length;i++){
            System.out.print("[");
            for(int j=0;j<matrix.length;j++){
                System.out.print(matrix[i][j]+",");
            }
            System.out.println("],");
        }


    }

}
