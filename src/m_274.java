// https://leetcode.com/problems/h-index/

import java.util.Arrays;
import java.util.Collections;

public class m_274 {
    public static int hIndex(int[] citations) {
        Arrays.sort(citations);
        int h = citations.length, len = citations.length;

        while (h > 0 && citations[len - h] < h) {
            h--;
        }

        return h;
    }


    public static void main(String[] args) {


        System.out.println(hIndex(new int[]{4, 4, 0, 0}));
        System.out.println(hIndex(new int[]{11, 15}));
        System.out.println(hIndex(new int[]{100}));
        System.out.println(hIndex(new int[]{3, 0, 6, 1, 5}));
        System.out.println(hIndex(new int[]{1, 3, 1}));
        System.out.println(hIndex(new int[]{0, 0, 0}));
    }

}
