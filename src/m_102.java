//https://leetcode.com/problems/binary-tree-level-order-traversal/


import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class m_102 {
    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> results = new LinkedList<>();

        if (root != null) {

            Queue<TreeNode> thisLevelNodes = new LinkedList<>();
            Queue<TreeNode> nextLevelNodes = new LinkedList<>();
            thisLevelNodes.add(root);

            boolean hasNextLevel = true;
            while (hasNextLevel) {
                hasNextLevel = false;
                List<Integer> thisLevelVals = new LinkedList<>();

                while (!thisLevelNodes.isEmpty()) {
                    TreeNode node = thisLevelNodes.poll();
                    thisLevelVals.add(node.val);

                    if (node.left != null) {
                        nextLevelNodes.add(node.left);
                        hasNextLevel = true;
                    }
                    if (node.right != null) {
                        nextLevelNodes.add(node.right);
                        hasNextLevel = true;
                    }
                }
                results.add(thisLevelVals);
                thisLevelNodes = nextLevelNodes;
                nextLevelNodes = new LinkedList<>();
            }
        }


        return results;

    }


    public static void main(String[] args) {


        System.out.println(levelOrder(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
    }

}
