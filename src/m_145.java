//https://leetcode.com/problems/binary-tree-postorder-traversal/


import java.util.LinkedList;
import java.util.List;

public class m_145 {
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> results = new LinkedList<>();
        sub(root, results);
        return results;
    }


    public static void sub(TreeNode root, List<Integer> results) {
        if (root != null) {
            sub(root.left, results);
            sub(root.right, results);
            results.add(root.val);
        }
    }


    public static void main(String[] args){



        System.out.println("Hello World");
    }

}
