//https://leetcode.com/problems/longest-common-prefix/

public class m_14 {


    public static String longestCommonPrefix(String[] strs) {
        if(strs.length==0){
            return "";
        }
        int p = 0;
        char c = 0;
        try {
            while (true) {
                for (String str : strs) {
                    if (c == 0) {
                        c = str.charAt(p);
                    } else {
                        if (str.charAt(p) != c) {
                            if (p > 0) {
                                return str.substring(0, p );
                            } else {
                                return "";
                            }
                        }
                    }
                }
                c = 0;
                p++;
            }
        } catch (Exception e) {
            if (p > 0) {
                return strs[0].substring(0, p);
            } else {
                return "";
            }
        }
    }

    public static void main(String[] args) {
        String[] strs = new String[]{

        };
        System.out.println(longestCommonPrefix(strs));

    }
}
