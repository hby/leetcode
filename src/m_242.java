// https://leetcode.com/problems/valid-anagram/


public class m_242 {
    public static boolean isAnagram(String s, String t) {
        int[] cs = new int[26], ct = new int[26];
        for (char c : s.toCharArray()) {
            cs[c - 'a']++;
        }
        for (char c : t.toCharArray()) {
            ct[c - 'a']++;
        }
        for (int i = 0; i < 26; i++) {
            if (cs[i] != ct[i]) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {


        System.out.println(isAnagram("anagram", "nagaram"));
        System.out.println(isAnagram("rat", "car"));
    }

}
