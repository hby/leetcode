//https://leetcode.com/problems/maximum-subarray/


public class m_53 {
    public static int maxSubArray(int[] nums) {
        int m = nums[0];
        for (int n : nums) {
            m = Math.max(n, (n + m));
        }
        return m;
    }


    public static void main(String[] args) {


        System.out.println(maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

}
