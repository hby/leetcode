//https://leetcode.com/problems/accounts-merge/


import java.util.*;

public class m_721 {
    public static List<List<String>> accountsMerge(List<List<String>> accounts) {
        List<List<String>> result = new ArrayList<>();
        Map<String, Integer> mail2index = new HashMap<>();

        for (List<String> account : accounts) {
            int size = account.size();
            if (size == 1) {
                result.add(new ArrayList<>() {{
                    add(account.get(0));
                }});
            } else {
                int i = 0;
                int index = result.size();
                Set<String> emails = new HashSet<>();
                for (String e : account) {
                    if (i > 0) {
                        if (mail2index.containsKey(e)) {
                            if(index!=result.size()&&index!=mail2index.get(e)){
                                int merge = mail2index.get(e);
                                ArrayList<String> toBeMerged = (ArrayList<String>) result.get(merge);
                                result.get(index).addAll(toBeMerged.subList(1, toBeMerged.size()));
                                for(String ee:toBeMerged.subList(1,toBeMerged.size())){
                                    mail2index.put(ee,index);
                                }
                                result.get(merge).clear();
                            }
                            index = mail2index.get(e);
                        }else {
                            emails.add(e);
                        }
                    }
                    i++;
                }
                for (String e : emails) {
                    mail2index.put(e, index);
                }

                if (index == result.size()) {
                    result.add(new ArrayList<>() {{
                        add(account.get(0));
                    }});
                }
                result.get(index).addAll(emails);
            }
        }

        List<List<String>> rr=new ArrayList<>();
        for(List<String> s:result){
            if(!s.isEmpty()){
                Collections.sort(s.subList(1,s.size()));
                rr.add(s);
            }
        }

        return rr;
    }


    public static void main(String[] args) {
        for (List<String> c : accountsMerge(new ArrayList<>() {{
            add(new ArrayList<>() {{
                add("Alex");
                add("Alex5@m.co");
                add("Alex4@m.co");
                add("Alex0@m.co");
            }});
            add(new ArrayList<>() {{
                add("John");
                add("johnnybravo@mail.com");
            }});
            add(new ArrayList<>() {{
                add("John");
                add("johnsmith@mail.com");
                add("john_newyork@mail.com");
            }});
            add(new ArrayList<>() {{
                add("Mary");
                add("mary@mail.com");
            }});
        }})) {
            System.out.print("[");
            for (String s : c) {
                System.out.print(s + ",");
            }
            System.out.print("]");

        }

    }

}
