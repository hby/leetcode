//https://leetcode.com/problems/number-of-1-bits/


public class m_191 {
    public static int hammingWeight(int n) {
        int w = 0;
        for (int i = 0; i < 32; i++) {
            w = w + ((n << i) >>> 31);
        }

        return w;
    }


    public static void main(String[] args) {


        System.out.println(hammingWeight(3));
    }

}
