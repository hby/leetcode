//https://leetcode.com/problems/clone-graph/


public class m_133 {
    public static Node cloneGraph(Node node) {
        return sub(node, new Node[100]);
    }

    public static Node sub(Node node, Node[] cloned) {
        if (node == null) {
            return null;
        }
        Node exists = cloned[node.val - 1];
        if (exists == null) {
            exists = new Node(node.val);
            cloned[node.val - 1] = exists;
            for (Node neighbor : node.neighbors) {
                exists.neighbors.add(sub(neighbor, cloned));
            }
            return exists;
        } else {
            return exists;
        }
    }


    public static void main(String[] args) {


        System.out.println("Hello World");
    }

}
