//https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/


public class m_122 {
    public static int maxProfit(int[] prices) {
        boolean holding = false;
        int p = prices[0], profit = 0;

        for (int i = 0; i < prices.length - 1; i++) {
            int todayPrice = prices[i], tomorrowPrice = prices[i + 1];
            if (holding) {
                if (tomorrowPrice < todayPrice) { // sold
                    profit += todayPrice - p;
                    holding = false;
                    p = todayPrice;
                }
            } else {
                if (tomorrowPrice > todayPrice) {
                    holding = true;
                    p = todayPrice;
                }
            }
        }
        if (holding) {
            profit = profit + prices[prices.length - 1] - p;
        }

        return profit;
    }


    public static void main(String[] args) {


        System.out.println(maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
        System.out.println(maxProfit(new int[]{1, 2, 3, 4, 5}));
        System.out.println(maxProfit(new int[]{7, 6, 4, 3, 1}));
    }


}
