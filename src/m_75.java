//https://leetcode.com/problems/sort-colors/


import java.util.Arrays;
import java.util.Date;

public class m_75 {
    public static void sortColors(int[] nums) {
        int red=0,white=0,blue=0;
        for(int n:nums){
            switch (n){
                case 0:
                    red++;
                    break;
                case 1:
                    white++;
                    break;
                case 2:
                    blue++;
                    break;
            }
        }
        Arrays.fill(nums,0,red,0);
        Arrays.fill(nums,red,red+white,1);
        Arrays.fill(nums,red+white,red+white+blue,2);

    }

    public static void sortColors2(int[] nums) {
        Arrays.sort(nums);
    }


    public static void main(String[] args){

        int len = 100000000;
        int[] nums1 = new int[len];
        int[] nums2 = new int[len];

        for(int i=0;i<len;i++){
            int p=(int)(Math.random()*3);
            nums1[i]=p;
            nums2[i]=p;
        }

        long tic,toc;

        tic= new Date().getTime();
        sortColors(nums1);
        toc = new Date().getTime();
        System.out.println(toc-tic);


        tic= new Date().getTime();
        sortColors2(nums2);
        toc = new Date().getTime();
        System.out.println(toc-tic);
    }

}
