//https://leetcode.com/problems/excel-sheet-column-number/


public class m_171 {
    public static int titleToNumber(String columnTitle) {
        int i = 0;
        for(char c :columnTitle.toCharArray()){
            i=i*26;
            i= i+ (c-'A'+1);
        }

        return i;
    }


    public static void main(String[] args){



        System.out.println(titleToNumber("AA"));
    }

}
