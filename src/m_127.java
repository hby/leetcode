//https://leetcode.com/problems/word-ladder-ii/


import java.util.*;

public class m_127 {
    public static final int base = 27;

    public static Long[] mapToLong(String str, int wordLen) {
        // Since all str will be lowercase english letters
        // we will map every string to a 27-base number ( return in decimal)
        // 1 - 26 for a - z , 0 for a wildcard searcher
        // example: "abc" ->  "a":1,"b":2,"c":3 -> return 1*26^2+2*26^1+3*26^0
        //          "?bc" ->  "?":0,"b":2,"c":3 -> return 0*26^2+2*26^1+3*26^0
        // when we calculate "abc", we will also calculate "?bc","a?c","ab?"


        // a string will have 10 chars max, the return value will be less than 26^10
        // which is greater than Integer.MAX , so we use Long to represent the string
        Long[] m = new Long[wordLen + 1];
        Arrays.fill(m, 0L);
        int i = 0;
        for (char c : str.toCharArray()) {
            m[0] = m[0] * base + (c - 'a' + 1);
            for (int j = 1; j <= wordLen; j++) {
                m[j] = (j == i + 1) ? (m[j] * base) : (m[j] * base + (c - 'a' + 1));
            }
            i++;
        }
        return m;
    }

    public static String mapToString(long m, int wordLen) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wordLen; i++) {
            sb.append((char) ('a' + m % base - 1));
            m = m / base;
        }
        return sb.reverse().toString();
    }

    public static Set<Long> getMove(Set<Long> privSteps, Map<Long, Set<Long>> forwardMap, Map<Long, Set<Long>> backwardMap) {
        Set<Long> currentStep = new HashSet<>();
        for (Long privStep : privSteps) {
            Set<Long> forwards = forwardMap.get(privStep);
            for (Long f : forwards) {
                Set<Long> inner = backwardMap.get(f);
                if (inner != null) {
                    currentStep.addAll(inner);
                    backwardMap.remove(f);
                }
            }
            currentStep.remove(privStep);
        }

        return currentStep;
    }

    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        int wordLen = beginWord.length();

        Long[] froms = mapToLong(beginWord, wordLen);
        Long[] toes = mapToLong(endWord, wordLen);

        Map<Long, Set<Long>> forward = new HashMap<>();
        Map<Long, Set<Long>> backward = new HashMap<>();

        for (String word : wordList) {
            Long[] ws = mapToLong(word, wordLen);

            forward.put(ws[0], new HashSet<>(Arrays.asList(ws)));
            for (int j = 1; j <= wordLen; j++) {
                if (!backward.containsKey(ws[j])) {
                    backward.put(ws[j], new HashSet<>() {{
                        add(ws[0]);
                    }});
                } else {
                    backward.get(ws[j]).add(ws[0]);
                }
            }
        }
        List<Set<Long>> steps = new LinkedList<>();
        Set<Long> privSteps = new HashSet<>();
        for (int i = 1; i < froms.length; i++) {
            Set<Long> s = backward.get(froms[i]);
            if (s != null) {
                backward.remove(froms[i]);
                privSteps.addAll(s);
            }
        }
        steps.add(privSteps);

        while ((!(privSteps.contains(toes[0]) || privSteps.isEmpty()))) {
            privSteps = getMove(privSteps, forward, backward);
            steps.add(privSteps);
        }

        if(privSteps.contains(toes[0])){
            return steps.size()+1;
        }else {
            return 0;
        }
    }


    public static void main(String[] args) {
//        String a = "abczaa";
//        Long[] b = mapToLong(a, a.length());
//        for (Long bb : b) {
//
//            String c = mapToString(bb, a.length());
//            System.out.println(c);
//        }


        System.out.println(ladderLength("hit", "cog", new ArrayList<>() {{
            addAll(Arrays.asList("hot", "dot", "dog", "lot", "log", "cog"));
        }}));
    }

}
