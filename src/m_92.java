//https://leetcode.com/problems/reverse-linked-list-ii/


public class m_92 {
    public static ListNode reverseBetween(ListNode head, int left, int right) {
        ListNode lastNodeBeforeReverse = left == 1 ? null : head;
        int i = 1;
        while (i < left - 1) {
            lastNodeBeforeReverse = lastNodeBeforeReverse.next;
            i++;
        }
        ListNode reverseHead;
        if(lastNodeBeforeReverse==null){
            reverseHead=head;
        }else {
            i++;
            reverseHead=lastNodeBeforeReverse.next;
        }

        ListNode p = reverseHead;
        while (i < right) {
            ListNode next = p.next;
            p.next = next.next;
            next.next = reverseHead;
            reverseHead = next;
            i++;
        }
        if (lastNodeBeforeReverse == null) {
            return reverseHead;
        } else {
            lastNodeBeforeReverse.next = reverseHead;
            return head;
        }
    }


    public static void main(String[] args) {


        System.out.println(reverseBetween(new ListNode( 5), 1,1));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 2, 4));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 3, 5));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 1, 2));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 4, 5));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 5, 5));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 1, 1));
        System.out.println(reverseBetween(new ListNode(1, 2, 3, 4, 5), 3, 3));
    }

}
