//https://leetcode.com/problems/valid-palindrome/

public class m_125 {

    public static boolean isLetter(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');
    }

    public static boolean isSame(char a, char b) {
        if (a == b) {
            return true;
        } else if ((b >= '0' && b <= '9') || (a >= '0' && a <= '9')) {
            return false;
        } else if (a >= 'a' && a <= 'z') {
            return a - 32 == b;
        } else {
            return a + 32 == b;
        }
    }

    public static boolean isPalindrome(String s) {
        char[] cs = s.toCharArray();
        int start = 0, end = cs.length - 1;

        while (start <= end) {
            char css = cs[start], cse = cs[end];
            if (!isLetter(css)) {
                start++;
            } else if (!isLetter(cse)) {
                end--;
            } else {
                if (isSame(css, cse)) {
                    start++;
                    end--;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {

        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
    }

}
