//https://leetcode.com/problems/binary-tree-level-order-traversal/


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class m_107 {
    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        Stack<List<Integer>> results = new Stack<>();

        if (root != null) {

            Queue<TreeNode> thisLevelNodes = new LinkedList<>();
            Queue<TreeNode> nextLevelNodes = new LinkedList<>();
            thisLevelNodes.add(root);

            boolean hasNextLevel = true;
            while (hasNextLevel) {
                hasNextLevel = false;
                List<Integer> thisLevelVals = new LinkedList<>();

                while (!thisLevelNodes.isEmpty()) {
                    TreeNode node = thisLevelNodes.poll();
                    thisLevelVals.add(node.val);

                    if (node.left != null) {
                        nextLevelNodes.add(node.left);
                        hasNextLevel = true;
                    }
                    if (node.right != null) {
                        nextLevelNodes.add(node.right);
                        hasNextLevel = true;
                    }
                }
                results.add(thisLevelVals);
                thisLevelNodes = nextLevelNodes;
                nextLevelNodes = new LinkedList<>();
            }
        }
        List<List<Integer>> r = new LinkedList<>();
        while (!results.empty()){
            r.add(results.pop());
        }


        return r;

    }


    public static void main(String[] args) {


        System.out.println(levelOrderBottom(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
    }

}
