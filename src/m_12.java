//https://leetcode.com/problems/integer-to-roman/

public class m_12 {

    public static String subRoman(int num, int oct) {
        switch (oct) {
            case 1:
                switch (num) {
                    case 1:
                        return "I";
                    case 2:
                        return "II";
                    case 3:
                        return "III";
                    case 4:
                        return "IV";
                    case 5:
                        return "V";
                    case 6:
                        return "VI";
                    case 7:
                        return "VII";
                    case 8:
                        return "VIII";
                    case 9:
                        return "IX";
                    default:
                        return "";
                }
            case 2:
                switch (num) {
                    case 1:
                        return "X";
                    case 2:
                        return "XX";
                    case 3:
                        return "XXX";
                    case 4:
                        return "XL";
                    case 5:
                        return "L";
                    case 6:
                        return "LX";
                    case 7:
                        return "LXX";
                    case 8:
                        return "LXXX";
                    case 9:
                        return "XC";
                    default:
                        return "";
                }
            case 3:
                switch (num) {
                    case 1:
                        return "C";
                    case 2:
                        return "CC";
                    case 3:
                        return "CCC";
                    case 4:
                        return "CD";
                    case 5:
                        return "D";
                    case 6:
                        return "DC";
                    case 7:
                        return "DCC";
                    case 8:
                        return "DCCC";
                    case 9:
                        return "CM";
                    default:
                        return "";
                }
            case 4:
                switch (num) {
                    case 1:
                        return "M";
                    case 2:
                        return "MM";
                    case 3:
                        return "MMM";
                    default:
                        return "";
                }
            default:
                return "";
        }
    }

    public static String intToRoman(int num) {
        StringBuilder sb = new StringBuilder();
        int oct = 0;
        int num_oct = 1;
        for (int i = num; i > 0; i /= 10) {
            oct++;
            num_oct *= 10;
        }
        num_oct /= 10;

        for (int i = oct; i > 0; i--) {
            sb.append(subRoman(num / num_oct, i));
            num = num - (num / num_oct) * num_oct;
            num_oct /= 10;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
//        for(int i=1;i<=3999;i++){
//            intToRoman(i);
//        }
        System.out.println(intToRoman(400));
//        System.out.println(intToRoman(1994));
    }
}
