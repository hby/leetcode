// https://leetcode.com/problems/different-ways-to-add-parentheses/


import java.util.*;

public class m_241 {
    public static List<Integer> diffWaysToCompute(String expression) {
        char[] ec = expression.toCharArray();
        List<Integer> nums = new ArrayList<>();
        List<Character> operators = new ArrayList<>(); // '+':1  '-':2   '*':3

        int num = 0;
        for (char e : ec) {
            if (e >= '0' && e <= '9') {
                num = num * 10 + e - '0';
            } else {
                nums.add(num);
                num = 0;
                operators.add(e);
            }
        }
        nums.add(num);


        return subCal(nums, operators, 0, nums.size() - 1, new HashMap<>());
    }

    public static List<Integer> subCal(List<Integer> nums,
                                       List<Character> operators,
                                       int from, int to,
                                       Map<Integer, List<Integer>> cache) {
        int key = from * 100 + to;
        if (!cache.containsKey(key)) {
            if (from == to) {
                cache.put(key, List.of(nums.get(from)));
            } else if (from + 1 == to) {
                switch (operators.get(from)) {
                    case '+' -> cache.put(key, List.of(nums.get(from) + nums.get(to)));
                    case '-' -> cache.put(key, List.of(nums.get(from) - nums.get(to)));
                    case '*' -> cache.put(key, List.of(nums.get(from) * nums.get(to)));
                }
            } else {
                List<Integer> res = new LinkedList<>();

                for (int i = from; i <= to - 1; i++) {
                    List<Integer> left = subCal(nums, operators, from, i, cache);
                    List<Integer> right = subCal(nums, operators, i + 1, to, cache);


                    for (int l : left) {
                        for (int r : right) {
                            switch (operators.get(i)) {
                                case '+' -> res.add(l + r);
                                case '-' -> res.add(l - r);
                                case '*' -> res.add(l * r);
                            }
                        }
                    }
                }
                cache.put(key, res);

            }
        }

        return cache.get(key);

    }


    public static void main(String[] args) {


        System.out.println(diffWaysToCompute("2-1-1"));
        System.out.println(diffWaysToCompute("2*3-4*5"));
        System.out.println(diffWaysToCompute("99*99*99*99*99*99*9"));
    }

}
