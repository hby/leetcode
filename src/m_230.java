// https://leetcode.com/problems/kth-smallest-element-in-a-bst/


public class m_230 {
    public static int kthSmallest(TreeNode root, int k) {

        if (k == 1 && root.left == null) {
            return root.val;
        } else if (k == 1 && root.left != null) {
            return kthSmallest(root.left, k);
        } else if (k != 1 && root.left == null) {
            if (root.right == null) {
                return -1;
            } else {
                int p = kthSmallest(root.right, k - 1);
                if (p >= 0) {
                    return p;
                } else {
                    return p - 1;
                }

            }
        } else { // k!=1 && root.left!=null
            int p = kthSmallest(root.left, k);
            if (p >= 0) {
                return p;
            } else {
                p = -p;
                if (k - p == 1) {
                    return root.val;
                } else {
                    if (root.right == null) {
                        p = p + 1;
                        return -p;
                    } else {
                        int q = kthSmallest(root.right, k - p - 1);
                        if (q >= 0) {
                            return q;
                        } else {
                            q = -q;
                            return -(p + q + 1);
                        }
                    }
                }
            }
        }
    }


    public static void main(String[] args) {

        System.out.println(kthSmallest(TreeNode.gen(4, 2, 5, 1, 3), 4));
//        System.out.println(kthSmallest(TreeNode.gen(3, 1, 4, null, 2), 3));
//        System.out.println(kthSmallest(TreeNode.gen(3, 1, 4, null, 2), 1));
//        System.out.println(kthSmallest(TreeNode.gen(5, 3, 6, 2, 4, null, null, 1), 3));
//        System.out.println(kthSmallest(TreeNode.gen(31, 30, 48, 3, null, 38, 49, 0, 16, 35, 47, null, null, null, 2, 15, 27, 33, 37, 39, null, 1, null, 5, null, 22, 28, 32, 34, 36, null, null, 43, null, null, 4, 11, 19, 23, null, 29, null, null, null, null, null, null, 40, 46, null, null, 7, 14, 17, 21, null, 26, null, null, null, 41, 44, null, 6, 10, 13, null, null, 18, 20, null, 25, null, null, 42, null, 45, null, null, 8, null, 12, null, null, null, null, null, 24, null, null, null, null, null, null, 9), 1));
    }

}
