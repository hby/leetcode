//https://leetcode.com/problems/binary-tree-inorder-traversal/


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class m_94 {
    public static void sub(TreeNode root,List<Integer> results){
        if(root!=null){
            sub(root.left,results);
            results.add(root.val);
            sub(root.right,results);
        }
    }

    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> results = new LinkedList<>();
        sub(root,results);
        return results;
    }


    public static void main(String[] args) {


        System.out.println(inorderTraversal(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
    }

}
