//https://leetcode.com/problems/partition-list/


public class m_86 {
    public static ListNode partition(ListNode head, int x) {
        ListNode lessHead = null, lessCursor = null;
        ListNode greaterHead = null, greaterCursor = null;

        ListNode current = head;
        while (current != null) {
            ListNode next = current.next;
            if (current.val >= x) {
                if (greaterCursor == null) {
                    greaterHead = current;
                    greaterCursor = current;
                } else {
                    greaterCursor.next = current;
                    greaterCursor = greaterCursor.next;
                }
                greaterCursor.next = null;
            } else {
                if (lessCursor == null) {
                    lessHead = current;
                    lessCursor = current;
                } else {
                    lessCursor.next = current;
                    lessCursor=lessCursor.next;
                }
                lessCursor.next=null;
            }
            current = next;
        }

        if (lessHead == null) {
            return greaterHead;
        } else {
            lessCursor.next=greaterHead;
            return lessHead;
        }
    }


    public static void main(String[] args) {


        System.out.println(partition(new ListNode(1, 4, 3, 2, 5, 2), 3));
        System.out.println(partition(new ListNode(2,1), 2));
        System.out.println(partition(new ListNode(2,1,2), 2));
    }

}
