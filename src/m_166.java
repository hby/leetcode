//https://leetcode.com/problems/fraction-to-recurring-decimal/


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class m_166 {

    public static String fuck(long numerator, long denominator) {
        boolean isNeg = (numerator > 0 && denominator < 0) || (numerator < 0 && denominator > 0);
        long i = numerator / denominator; //整数部分
        if (numerator % denominator == 0) {
            return String.valueOf(i);  //整数
        } else { // 不可整除
            StringBuilder sb = new StringBuilder();
            numerator = Math.abs(numerator);
            denominator = Math.abs(denominator);
            i = Math.abs(i);
            numerator = numerator - i * denominator; // 只计算小数部分。整数部分为 i

            long remain = numerator; //剩余的分子
            Map<Long, String> rs = new HashMap<>();

            while (remain != 0) {
                remain *= 10;
                if (remain < denominator) {
                    sb.append("0");
                } else {
                    long j = remain / denominator;
                    remain = remain - j * denominator;
                    sb.append(j);
                    if (rs.containsKey(remain)) {

                        sb.delete(0, rs.get(remain).length());
                        return (isNeg ? "-" : "") + i + "." + rs.get(remain) + "(" + sb + ")";
                    } else {
                        rs.put(remain, sb.toString());
                    }
                }
            }
            return (isNeg ? "-" : "") + i + "." + sb;
        }
    }
    public static String fractionToDecimal(int numerator, int denominator) {
        return fuck(numerator,denominator);
    }


    public static void main(String[] args) {


        System.out.println(fractionToDecimal(7, -12));
        System.out.println(fractionToDecimal(1, 33));
        System.out.println(fractionToDecimal(1562, 9900));
        System.out.println(fractionToDecimal(99, 333));
    }

}
