//https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class m_103 {
    public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> results = new LinkedList<>();

        if (root != null) {

            Stack<TreeNode> thisLevelNodes = new Stack<>();
            Stack<TreeNode> nextLevelNodes = new Stack<>();
            thisLevelNodes.push(root);

            boolean hasNextLevel = true, left2right = true;

            while (hasNextLevel) {
                hasNextLevel = false;
                List<Integer> thisLevelVals = new LinkedList<>();

                while (!thisLevelNodes.isEmpty()) {
                    TreeNode node = thisLevelNodes.pop();
                    thisLevelVals.add(node.val);
                    if (left2right) {
                        if (node.left != null) {
                            nextLevelNodes.push(node.left);
                            hasNextLevel = true;
                        }
                        if (node.right != null) {
                            nextLevelNodes.push(node.right);
                            hasNextLevel = true;
                        }
                    } else {
                        if (node.right != null) {
                            nextLevelNodes.push(node.right);
                            hasNextLevel = true;
                        }
                        if (node.left != null) {
                            nextLevelNodes.push(node.left);
                            hasNextLevel = true;
                        }
                    }
                }
                results.add(thisLevelVals);
                thisLevelNodes = nextLevelNodes;
                nextLevelNodes = new Stack<>();
                left2right = !left2right;
            }
        }


        return results;

    }


    public static void main(String[] args) {


        System.out.println(zigzagLevelOrder(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
    }

}
