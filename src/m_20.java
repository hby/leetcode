//https://leetcode.com/problems/valid-parentheses/

import java.util.Stack;

public class m_20 {
    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '(':
                case '[':
                case '{':
                    stack.push(c);
                    break;
                case ')':
                    if(stack.empty()){
                        return false;
                    }
                    if (stack.peek() == '(') {
                        stack.pop();
                    }else {
                        return false;
                    }
                    break;
                case ']':
                    if(stack.empty()){
                        return false;
                    }
                    if (stack.peek() == '[') {
                        stack.pop();
                    }else {
                        return false;
                    }
                    break;
                case '}':
                    if(stack.empty()){
                        return false;
                    }
                    if (stack.peek() == '{') {
                        stack.pop();
                    }else {
                        return false;
                    }
                    break;
            }
        }
        return stack.empty();
    }


    public static void main(String[] args) {


        System.out.println(isValid("(})"));
    }

}
