//https://leetcode.com/problems/isomorphic-strings/


import java.util.HashMap;
import java.util.Map;

public class m_205 {
    public static boolean isIsomorphic(String s, String t) {
        Map<Character, Character> s2t = new HashMap<>();
        Map<Character, Character> t2s = new HashMap<>();
        int l = s.length();
        for (int i = 0; i < l; i++) {
            char sc = s.charAt(i), tc = t.charAt(i);
            if (s2t.containsKey(sc)) {
                if (tc != s2t.get(sc) || sc != t2s.get(tc)) {
                    return false;
                }
            } else {
                if (t2s.containsKey(tc)) {
                    return false;
                } else {
                    s2t.put(sc, tc);
                    t2s.put(tc, sc);
                }
            }
        }
        return true;

    }


    public static void main(String[] args) {


        System.out.println(isIsomorphic("foo", "see"));
    }

}
