//TODO https://leetcode.com/problems/count-primes/


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class m_204 {

    //???????????????????
    //https://leetcode.com/problems/count-primes/discuss/57593/12-ms-Java-solution-modified-from-the-hint-method-beats-99.95
    public static int countPrimes(int n) {
        if (n < 3)
            return 0;

        boolean[] f = new boolean[n];
        int count = n / 2;
        for (int i = 3; i * i < n; i += 2) {
            if (f[i])
                continue;

            for (int j = i * i; j < n; j += 2 * i) {
                if (!f[j]) {
                    --count;
                    f[j] = true;
                }
            }
        }
        return count;
    }


    public static void main(String[] args) {
        System.out.println(countPrimes(500000));

    }

}
