//https://leetcode.com/problems/implement-trie-prefix-tree/#


import java.util.HashMap;
import java.util.Map;

public class m_208 {
    static class Trie {
        private Map<Character, Trie> m;
        private boolean isEnd;

        /**
         * Initialize your data structure here.
         */
        public Trie() {
            this.m = new HashMap<>();
            this.isEnd = false;
        }

        /**
         * Inserts a word into the trie.
         */
        public void insert(String word) {
            char[] words = word.toCharArray();
            insert(words, 0);
        }

        private void insert(char[] words, int p) {
            if (p < words.length) {
                Trie t;
                char c = words[p];
                if (!this.m.containsKey(words[p])) {
                    this.m.put(c, new Trie());
                }
                t = this.m.get(c);
                t.insert(words, p + 1);
            } else {
                this.isEnd = true;
            }
        }

        /**
         * Returns if the word is in the trie.
         */
        public boolean search(String word) {
            return search(word.toCharArray(), 0);
        }

        private boolean search(char[] words, int p) {
            if (p < words.length) {
                char c = words[p];
                return this.m.containsKey(c) && this.m.get(c).search(words, p + 1);
            } else {
                return this.isEnd;
            }
        }

        /**
         * Returns if there is any word in the trie that starts with the given prefix.
         */
        public boolean startsWith(String prefix) {
            return startsWith(prefix.toCharArray(), 0);
        }

        private boolean startsWith(char[] words, int p) {
            if (p < words.length) {
                char c = words[p];
                return this.m.containsKey(c) && this.m.get(c).startsWith(words, p + 1);
            } else {
                return true;
            }
        }
    }
//["Trie",
// "insert","insert","insert","insert","insert","insert",
// "search","search","search","search","search",
// "search","search","search","search",
// "startsWith","startsWith","startsWith","startsWith","startsWith",
// "startsWith","startsWith","startsWith","startsWith"]
//[[],
// ["app"],["apple"],["beer"],["add"],["jam"],["rental"],
// ["apps"],["app"],["ad"],["applepie"],["rest"],
// ["jan"],["rent"],["beer"],["jam"],
// ["apps"],["app"],["ad"],["applepie"],["rest"],
// ["jan"],["rent"],["beer"],["jam"]]

    public static void main(String[] args) {
        Trie trie = new Trie();
//[null,null,null,null,null,null,null,
// false,true,false,false,false,false,false,true,true,
// false,true,true,false,false,false,true,true,true]

        trie.insert("app");
        trie.insert("apple");
        trie.insert("beer");
        trie.insert("add");
        trie.insert("jam");
        trie.insert("rental");
        System.out.println(trie.search("apps"));
        System.out.println(trie.search("app")+" TRUE");//T
        System.out.println(trie.search("ad"));
        System.out.println(trie.search("applepie"));
        System.out.println(trie.search("rest"));
        System.out.println(trie.search("jan"));
        System.out.println(trie.search("rent"));
        System.out.println(trie.search("beer")+" TRUE");
        System.out.println(trie.search("jam")+" TRUE");


        System.out.println(trie.startsWith("apps"));
        System.out.println(trie.startsWith("app")+" TRUE");
        System.out.println(trie.startsWith("ad")+" TRUE");
        System.out.println(trie.startsWith("applepie"));
        System.out.println(trie.startsWith("rest"));
        System.out.println(trie.startsWith("jan"));
        System.out.println(trie.startsWith("rent")+" TRUE");
        System.out.println(trie.startsWith("beer")+" TRUE");
        System.out.println(trie.startsWith("jam")+" TRUE");

    }

}
