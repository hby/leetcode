//https://leetcode.com/problems/decode-ways/

import java.util.Arrays;

public class m_91 {
    public static int numDecodings(String s) {

        int[] cache = new int[s.length()];
        Arrays.fill(cache, -1);

        int x = subDecode(s.toCharArray(), 0, cache);

        return x;
    }

    public static int subDecode(char[] cs, int fromIndex, int[] cache) {
        if (fromIndex >= cs.length) {
            return 1;
        } else if (cs[fromIndex] == '0') {
            return 0;
        } else if (fromIndex == cs.length - 1) {
            return 1;
        } else {
            if (cache[fromIndex] != -1) {
                return cache[fromIndex];
            }

            if (cs[fromIndex] == '1') {
                int a = subDecode(cs, fromIndex + 1, cache);
                int b = subDecode(cs, fromIndex + 2, cache);
                cache[fromIndex] = a + b;
                return a + b;
            } else if (cs[fromIndex] == '2') {
                if (cs[fromIndex + 1] >= '0' && cs[fromIndex + 1] <= '6') {
                    int a = subDecode(cs, fromIndex + 1, cache);
                    int b = subDecode(cs, fromIndex + 2, cache);
                    cache[fromIndex] = a + b;
                    return a + b;
                } else {
                    int b = subDecode(cs, fromIndex + 2, cache);
                    cache[fromIndex] = b;
                    return b;
                }
            } else {
                int a = subDecode(cs, fromIndex + 1, cache);
                cache[fromIndex] = a;
                return a;
            }
        }

    }


    public static void main(String[] args) {

        System.out.println(numDecodings("12"));
        System.out.println(numDecodings("0"));
        System.out.println(numDecodings("06"));
        System.out.println(numDecodings("226"));
        System.out.println(numDecodings("111111111111111111111111111111111111111111111"));
    }

}
