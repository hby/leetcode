import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


//https://leetcode.com/problems/remove-nth-node-from-end-of-list/

public class m_19 {

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode t = head;
        int len = 0;
        while (t != null) {
            len++;
            t = t.next;
        }
        head = removeNthFromHead(head, len - n);

        return head;
    }

    public static ListNode removeNthFromHead(ListNode head, int n) {
        if (n == 0) {
            return head.next;
        } else {
            head.next = removeNthFromHead(head.next, n - 1);
        }

        return head;
    }


    public static void main(String[] args) {

        System.out.println(removeNthFromEnd(new ListNode(new int[]{1,2,3,4,5}), 5));
    }
}
