//https://leetcode.com/problems/unique-binary-search-trees-ii/


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class m_95 {
    public static List<TreeNode> generateTrees(int n) {
        return sub(1, n, new HashMap<>());
    }

    public static List<TreeNode> sub(int from, int to, Map<Integer, List<TreeNode>> cache) {
        if (to - from < 0) {
            return new LinkedList<>();
        } else if (to - from == 0) {
            return new LinkedList<>() {{
                add(new TreeNode(to));
            }};
        } else {
            int key = from * 10 + to;
            if (cache.containsKey(key)) {
                return cache.get(key);
            } else {
                List<TreeNode> results = new LinkedList<>();
                for (int i = from; i <= to; i++) {
                    List<TreeNode> leftPossibles = sub(from, i - 1, cache);
                    List<TreeNode> rightPossibles = sub(i + 1, to, cache);

                    if (leftPossibles.isEmpty()) {
                        for (TreeNode right : rightPossibles) {
                            results.add(new TreeNode(i, null, right));
                        }

                    } else if (rightPossibles.isEmpty()) {
                        for (TreeNode left : leftPossibles) {
                            results.add(new TreeNode(i, left, null));

                        }
                    } else {
                        for (TreeNode left : leftPossibles) {
                            for (TreeNode right : rightPossibles) {
                                results.add(new TreeNode(i, left, right));
                            }
                        }
                    }

                }
                cache.put(key, results);
                return results;
            }

        }

    }


    public static void main(String[] args) {

        List<TreeNode> l = generateTrees(5);


        System.out.println("Hello World");
    }

}
