//https://leetcode.com/problems/sqrtx/


public class m_69 {
    public static int mySqrt(int x) {
        //https://leetcode.com/problems/sqrtx/discuss/575326/Java-100-faster-solution-time-taken-approx-1ms
        if (x <=1) {
            return x;
        }
        long sqrt = x;
        while (sqrt*sqrt > x) {
            System.out.println(sqrt);
            sqrt = (sqrt + x/sqrt) / 2;
        }
        return (int) sqrt;
    }


    public static void main(String[] args) {


//        System.out.println(mySqrt(0));
//        System.out.println(mySqrt(1999));
        System.out.println(mySqrt(Integer.MAX_VALUE));
    }

}
