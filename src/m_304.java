// https://leetcode.com/problems/range-sum-query-2d-immutable/

public class m_304 {
    static class NumMatrix {

        int[][] sumMatrix;

        public NumMatrix(int[][] matrix) {
            this.sumMatrix = new int[matrix.length + 1][matrix[0].length + 1];
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    sumMatrix[i + 1][j + 1] = sumMatrix[i][j + 1] + sumMatrix[i + 1][j] - sumMatrix[i][j] + matrix[i][j];

                }
            }
        }

        public int sumRegion(int row1, int col1, int row2, int col2) {
            int h = sumMatrix[row2+1][col1];
            int v = sumMatrix[row1][col2+1];
            int p = sumMatrix[row1][col1];
            int t = sumMatrix[row2+1][col2+1];
            return t+p-v-h;

        }
    }


    public static void main(String[] args) {


        NumMatrix obj = new NumMatrix(new int[][]{
                new int[]{3, 0, 1, 4, 2},
                new int[]{5, 6, 3, 2, 1},
                new int[]{1, 2, 0, 1, 5},
                new int[]{4, 1, 0, 1, 7},
                new int[]{1, 0, 3, 0, 5},
        });
        System.out.println(obj.sumRegion(2, 1, 4, 3));
        System.out.println(obj.sumRegion(1, 1, 2, 2));
        System.out.println(obj.sumRegion(1, 2, 2, 4));
    }

}
