package chanllenge;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class m20210514 {
    public static List<String> generate(char[] cs) {
        List<String> results = new LinkedList<>();
        if(cs.length==1){
            results.add(new String(cs));
        }else if(cs.length>=2){
            if(cs[0]=='0'){

            }else {

            }



        }

        return results;
    }

    public static List<String> ambiguousCoordinates(String s) {
        char[] cs = s.substring(1, s.length() - 1).toCharArray();
        int len = cs.length;
        List<String> results = new LinkedList<>();

        for (int splitter = 1; splitter < cs.length; splitter++) {
            char[] l = Arrays.copyOfRange(cs, 0, splitter);
            char[] r = Arrays.copyOfRange(cs, splitter, len);
            List<String> ls = generate(l);
            List<String> rs = generate(r);
            if (ls.size() > 0 && rs.size() > 0) {
                for (String ll : ls) {
                    for (String rr : rs) {
                        results.add("(" + ll + ", " + rr + ")");
                    }
                }
            }
        }


        return results;
    }

    public static void main(String[] args) {
        for (String c : ambiguousCoordinates("(12345)")) {
            System.out.println(c);
        }

    }
}
