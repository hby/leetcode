//https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/


public class m_123 {
    public static int maxProfit(int[] prices) {
        boolean holding = false;
        int p = prices[0], profit_1 = 0,profit_2=0;

        for (int i = 0; i < prices.length - 1; i++) {
            int todayPrice = prices[i], tomorrowPrice = prices[i + 1];
            if (holding) {
                if (tomorrowPrice < todayPrice) { // sold

                    int profit = todayPrice-p;
                    if(profit>=profit_1){
                        profit_2=profit_1;
                        profit_1=profit;
                    }else if(profit>profit_2){
                        profit_2=profit;
                    }

                    holding = false;
                    p = todayPrice;
                }
            } else {
                if (tomorrowPrice > todayPrice) {
                    holding = true;
                    p = todayPrice;
                }
            }
        }
        if (holding) {
            int profit =  prices[prices.length - 1] - p;
            if(profit>=profit_1){
                profit_2=profit_1;
                profit_1=profit;
            }else if(profit>profit_2){
                profit_2=profit;
            }
        }

        return profit_1+profit_2;
    }


    public static void main(String[] args) {


        System.out.println(maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
        System.out.println(maxProfit(new int[]{1, 2, 3, 4, 5}));
        System.out.println(maxProfit(new int[]{7, 6, 4, 3, 1}));
    }


}
