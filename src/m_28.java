//https://leetcode.com/problems/implement-strstr/

import java.util.Objects;

public class m_28 {
    public static int strStr(String haystack, String needle) {
        int needleLen = needle.length();
        if (Objects.equals(needle, "")) {
            return 0;
        }
        for (int i = 0; i < haystack.length(); i++) {
            if (i + needleLen > haystack.length()) {
                return -1;
            } else {
                if (haystack.substring(i, i + needleLen).equals(needle)) {
                    return i;
                }
            }

        }
        return -1;
    }


    public static void main(String[] args) {


        System.out.println(strStr("ab", ""));
    }

}
