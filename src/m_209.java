//https://leetcode.com/problems/minimum-size-subarray-sum/


import java.util.Arrays;

public class m_209 {
    public static int minSubArrayLen(int target, int[] nums) {

        int sum = 0;
        int[] sums = new int[nums.length + 1];
        for (int i = 0; i < nums.length; i++) {
            sums[i] = sum;
            sum += nums[i];
        }
        sums[nums.length] = sum;

        int minLen = nums.length + 1;
        for (int i = 0; i < nums.length; i++) {
            int pos = Arrays.binarySearch(sums, sums[i] + target);
            if (pos >= 0) {
                minLen = Math.min(minLen, pos - i);
            } else if (pos < -sums.length) {
                if (i == 0) {
                    if (sums[-pos - 2] == target) {
                        return -pos - 1;
                    } else {
                        return 0;
                    }
                }
            } else {
                pos = -pos;
                pos = pos - 1;
                minLen = Math.min(minLen, pos - i);
            }
        }

        if (minLen == nums.length + 1) {
            return 0;
        } else {
            return minLen;
        }
    }


    public static void main(String[] args) {


        System.out.println(minSubArrayLen(7, new int[]{2, 3, 1, 2, 4, 3}));
        System.out.println(minSubArrayLen(4, new int[]{1, 4, 4}));
        System.out.println(minSubArrayLen(11, new int[]{1, 1, 1, 1, 1, 1, 1, 1}));
        System.out.println(minSubArrayLen(11, new int[]{1, 2, 3, 4, 5}));
        System.out.println(minSubArrayLen(9, new int[]{1, 4, 4}));
        System.out.println(minSubArrayLen(5, new int[]{2, 3, 1, 1, 1, 1, 1}));
        System.out.println(minSubArrayLen(507, new int[]{2, 3, 1, 2, 400, 6, 100, 100}));
    }

}
