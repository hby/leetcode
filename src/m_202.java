//https://leetcode.com/problems/happy-number/


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class m_202 {
    public static int[] tab = new int[]{0, 1, 4, 9, 16, 25, 36, 49, 64, 81};

    public static List<Integer> consistsOf(int n) {
        List<Integer> i = new ArrayList<>(22);
        while (n > 0) {
            i.add(n % 10);
            n /= 10;
        }
        return i;
    }

    public static boolean isHappy(int n) {
        return sub(n, new HashSet<>());
    }

    public static boolean sub(int n, Set<Integer> set) {
        List<Integer> c = consistsOf(n);
        int nn = 0;
        for (int cc : c) {
            nn += tab[cc];
        }
        if (nn == 1) {
            return true;
        } else if (set.contains(nn)) {
            return false;
        } else {
            set.add(nn);
            return sub(nn, set);
        }
    }

    public static boolean isHappy2(int n) {
        List<Integer> c = consistsOf(n);
        int nn = 0;
        for (int cc : c) {
            nn += tab[cc];
        }
        if (nn <= 100) {
            return nn == 1 || nn == 7 || nn == 10 || nn == 13
                    || nn == 19 || nn == 23 || nn == 28 || nn == 31
                    || nn == 32 || nn == 44 || nn == 49 || nn == 68
                    || nn == 70 || nn == 79 || nn == 82 || nn == 86
                    || nn == 91 || nn == 94 || nn == 97 || nn == 100;
        } else {
            return isHappy2(nn);
        }
    }


    public static void main(String[] args) {

        for (int i = 0; i < 10000; i++) {
            if (isHappy(i) != isHappy2(i)) {
                System.out.println(i);
            }
        }

//        System.out.println(isHappy(Integer.MAX_VALUE));
    }

}
