//https://leetcode.com/problems/sum-root-to-leaf-numbers/


public class m_129 {

    public static int sum(TreeNode root, int from) {
        int v = from * 10 + root.val;
        int leftSum = 0, rightSum = 0;
        if (root.left != null) {
            leftSum = sum(root.left, v);
        }
        if (root.right != null) {
            rightSum = sum(root.right, v);
        }
        if (root.left == null && root.right == null) {
            return v;
        }
        return leftSum + rightSum;

    }

    public static int sumNumbers(TreeNode root) {
        return sum(root, 0);
    }


    public static void main(String[] args) {


        System.out.println(sumNumbers(TreeNode.gen(1)));
        System.out.println(sumNumbers(TreeNode.gen(1, 2, 3)));
        System.out.println(sumNumbers(TreeNode.gen(4,9,0,5,1)));
//        System.out.println(sumNumbers(TreeNode.gen(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1)));
    }

}
