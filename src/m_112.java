//https://leetcode.com/problems/path-sum/


public class m_112 {
    public static boolean isLeaf(TreeNode root) {
        if (root != null) {
            return root.left == null && root.right == null;
        } else {
            return false;
        }
    }

    public static boolean sub(TreeNode root, int targetSum) {
        if (isLeaf(root)) {
            return targetSum == root.val;
        } else {
            if (root.left == null && root.right != null) {
                return sub(root.right, targetSum - root.val);
            } else if (root.right == null) {
                return sub(root.left, targetSum - root.val);
            } else {
                return sub(root.left, targetSum - root.val) || sub(root.right, targetSum - root.val);
            }
        }
    }

    public static boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        } else {
            return sub(root, targetSum);
        }
    }


    public static void main(String[] args) {


        System.out.println(hasPathSum(TreeNode.gen(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1), 22));
    }

}
