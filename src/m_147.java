//https://leetcode.com/problems/insertion-sort-list/


public class m_147 {
    public static ListNode insertionSortList(ListNode head) {
        if (head.next == null) {
            return head;
        }

        ListNode current = head.next;
        ListNode priv = head;
        while (current != null) {
            ListNode next = current.next;
            if (current.val >= priv.val) {
                priv = current;
                current = next;
            } else if (current.val <= head.val) {
                current.next = head;
                head=current;
                priv.next = next;
                current = next;
            } else {
                ListNode p = head;
                while (p != priv) {
                    if (p.val < current.val && p.next.val >= current.val) {
                        current.next = p.next;
                        p.next = current;
                        break;
                    }
                    p = p.next;
                }
                priv.next = next;
                current = next;
            }


        }


        return head;
    }


    public static void main(String[] args) {


        System.out.println(insertionSortList(new ListNode(4, 2, 1, 3)));
        System.out.println(insertionSortList(new ListNode(-1, 5, 3, 4, 0)));
    }

}
