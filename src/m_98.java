//https://leetcode.com/problems/validate-binary-search-tree/


public class m_98 {
    public static boolean isValidBST(TreeNode root) {
        return sub(root, new long[]{Long.MIN_VALUE, Long.MAX_VALUE});
    }

    public static boolean sub(TreeNode root, long[] range) {
        return root == null || (root.val > range[0] && root.val < range[1]
                && sub(root.left, new long[]{range[0], root.val})
                && sub(root.right, new long[]{root.val, range[1]}));
    }


    public static void main(String[] args) {


        System.out.println(isValidBST(new TreeNode(Integer.MIN_VALUE, null, new TreeNode(Integer.MAX_VALUE))));

    }

}
