//https://leetcode.com/problems/reverse-nodes-in-k-group

public class m_25 {
    public static ListNode reverseKGroup(ListNode head, int k) {
        if (k == 1) {
            return head;
        }
        int counter = 0;
        ListNode current = head;
        ListNode sNode = current;
        int[] vals = new int[k];

        while (current != null) {
            if ((counter + 1) % k == 0) {
                vals[k - 1] = current.val;

                for (int i = 0; i < k; i++) {
                    sNode.val = vals[k - i - 1];
                    sNode = sNode.next;
                }
            } else {
                vals[counter % k] = current.val;
            }
            counter++;
            current = current.next;
        }

        return head;

    }


    public static void main(String[] args) {

        System.out.println(reverseKGroup(new ListNode(1), 1));
        System.out.println(reverseKGroup(new ListNode(1, 2, 3, 4, 5), 2));
        System.out.println(reverseKGroup(new ListNode(1, 2, 3, 4, 5, 6), 2));
        System.out.println(reverseKGroup(new ListNode(1, 2, 3, 4, 5, 6), 3));
        System.out.println(reverseKGroup(new ListNode(1, 2, 3, 4, 5, 6), 4));
    }

}
