// https://leetcode.com/problems/implement-queue-using-stacks/


public class m_232 {


    public static void main(String[] args) {


        MyQueue obj = new MyQueue();
        obj.push(1);
        obj.push(2);
        System.out.println(obj.peek());
        System.out.println(obj.pop());
        System.out.println(obj.empty());
    }

}

class MyQueue {

    class LN {
        int val;
        LN next;
    }

    LN root;
    LN tail;

    public MyQueue() {

    }

    public void push(int x) {
        if (root == null) {
            root = new LN();
            root.val = x;
            tail = root;
        } else {
            tail.next = new LN();
            tail.next.val = x;
            tail = tail.next;
        }
    }

    public int pop() {
        int t = root.val;
        root = root.next;

        return t;
    }

    public int peek() {
        return root.val;
    }

    public boolean empty() {
        return root == null;
    }
}

