//https://leetcode.com/problems/spiral-matrix-ii/


public class m_59 {
    public static int[][] generateMatrix(int n) {
        int[][] res = new int[n][n];
        int arrow = 0, layer = 0;
        int r = 0, c = 0;
        for (int i = 0; i < n * n; i++) {
            res[r][c] = i + 1;
            switch (arrow) {
                case 0:
                    if (c + 1 >= n - layer) {
                        arrow = 1;
                        r++;
                    } else {
                        c++;
                    }
                    break;
                case 1:
                    if (r + 1 >= n - layer) {
                        arrow = 2;
                        c--;
                    } else {
                        r++;
                    }
                    break;
                case 2:
                    if (c - 1 < layer) {
                        arrow = 3;
                        r--;
                    } else {
                        c--;
                    }
                    break;
                case 3:
                    if (r - 1 <= layer) {
                        arrow = 0;
                        c++;
                        layer++;
                    } else {
                        r--;
                    }
                    break;
            }
        }
        return res;
    }


    public static void main(String[] args) {
        int[][] r = generateMatrix(4);
        for(int i=0;i<r.length;i++){
            System.out.print("[");
            for(int j=0;j<r.length;j++){
                System.out.print(r[i][j]+",");
            }
            System.out.print("]");
            System.out.println("");
        }


    }

}
