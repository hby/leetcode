//https://leetcode.com/problems/remove-duplicates-from-sorted-array/
//TODO 用时

import java.util.Arrays;
import java.util.Collections;

public class m_26 {
    public static int removeDuplicates(int[] nums) {
        if (nums.length <= 1) {
            return nums.length;
        }

        int currentMaxValue = Integer.MIN_VALUE;
        int currentPointer;
        for (currentPointer = 0; currentPointer < nums.length; currentPointer++) {
            if (nums[currentPointer] > currentMaxValue) {
                currentMaxValue = nums[currentPointer];
            } else {
                boolean reachesEnd = true;
                for (int seeker = currentPointer + 1; seeker < nums.length ; seeker++) {
                    int temp = nums[seeker];
                    if (temp > nums[currentPointer] && temp > nums[currentPointer - 1]) {
                        nums[seeker] = nums[currentPointer];
                        nums[currentPointer] = temp;
                        currentMaxValue = temp;
                        reachesEnd = false;
                        break;
                    }
                }
                if(reachesEnd){
                    return currentPointer;
                }

            }
        }
        return currentPointer;
    }


    public static void main(String[] args) {
        int[] nums = new int[]{1};
//        int[] nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

        System.out.println(removeDuplicates(nums));
        for (int num : nums) {
            System.out.print(num + ",");
        }
    }

}
