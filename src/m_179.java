//https://leetcode.com/problems/largest-number/


import java.util.*;

public class m_179 {
    public static String largestNumber(int[] nums) {
        List<String> a = new ArrayList<>(nums.length);
        for(int num:nums){
            a.add(String.valueOf(num));
        }
        a.sort((o1, o2) -> {
            return -(o1+o2).compareTo(o2+o1);
        });
        String res = String.join("",a);
        int leadingZerosIndex = 0;
        while (leadingZerosIndex<res.length()&&res.charAt(leadingZerosIndex)=='0'){
            leadingZerosIndex++;
        }
        if(leadingZerosIndex==res.length()){
            return "0";
        }else {
            return res;
        }
    }


    public static void main(String[] args){



        System.out.println(largestNumber(new int[]{3,30,34,5,9}));
        System.out.println(largestNumber(new int[]{3,30,34,5,9}));
    }

}
