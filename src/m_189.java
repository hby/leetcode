//https://leetcode.com/problems/rotate-array/


import java.util.Arrays;

public class m_189 {
    public static void rotate(int[] nums, int k) {
        int l = nums.length;
        k %= l;
        int[] p = new int[k];
        System.arraycopy(nums, l - k, p, 0, k);
        System.arraycopy(nums, 0, nums, k, l - k);
        System.arraycopy(p, 0, nums, 0, k);
    }


    public static void main(String[] args) {

        int[] arr ;

//        arr= new int[]{1, 2, 3, 4, 5, 6, 7};
//        rotate(arr, 3);
//        System.out.println(Arrays.toString(arr));


        arr = new int[]{-1,-100,3,99};
        rotate(arr, 2);
        System.out.println(Arrays.toString(arr));
    }

}
