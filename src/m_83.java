//https://leetcode.com/problems/remove-duplicates-from-sorted-list/


public class m_83 {


    public static ListNode deleteDuplicates(ListNode head) {

        ListNode priv = head, current = head;
        while (current != null) {
            if (current.val == priv.val) {
                current = current.next;

                if (current == null) {
                    priv.next = null;
                }
            } else {
                priv.next = current;
                priv = current;
                current = current.next;
            }
        }

        return head;
    }


    public static void main(String[] args) {
        System.out.println(deleteDuplicates(new ListNode(1, 1, 1)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 2)));
        System.out.println(deleteDuplicates(new ListNode(1, 1, 2, 3, 3)));
    }

}
