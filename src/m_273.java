// https://leetcode.com/problems/integer-to-english-words/

public class m_273 {
    public static String numberToWords(int num) {

        return say_any(num).trim();
    }

    public static String say_0_20(int num) {
        String[] patterns = new String[]{"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
                "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty"};
        return patterns[num];
    }

    public static String say_10x(int num) {
        String[] patterns = new String[]{"Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
        return patterns[num / 10 - 1];
    }

    public static String say_100x(int num) {
        String[] patterns = new String[]{"Hundred", "Thousand", "Million", "Billion"};
        if (num == 100) {
            return patterns[0];
        }
        if (num == 1000) {
            return patterns[1];
        }
        if (num == 1000000) {
            return patterns[2];
        }
        if (num == 1000000000) {
            return patterns[3];
        }
        return "xxx";
    }

    public static String say_any_0_99(int num) {
        if (num >= 0 && num <= 20) {
            return say_0_20(num);
        } else if (num >= 21 && num <= 99) {
            if (num % 10 != 0) {
                return say_10x(num) + " " + say_0_20(num % 10);
            } else {
                return say_10x(num);
            }
        }
        return "xxx";
    }

    public static String say_any_0_999(int num) {
        if (num < 100) {
            return say_any_0_99(num);
        } else if (num <= 999) {
            String res = say_0_20(num / 100) + " " + say_100x(100);
            if (num % 100 != 0) {
                res = res + " " + say_any_0_99(num%100);
            }
            return res;
        }
        return "xxx";
    }

    public static String say_any(int num) {
        String res = "";
        if (num >= 1000000000) {
            res += say_any_0_999(num / 1000000000) + " " + say_100x(1000000000) + " ";
            num = num % 1000000000;
            if(num==0){return res;}
        }

        if (num >= 1000000) {
            res += say_any_0_999(num / 1000000) + " " + say_100x(1000000) + " ";
            num = num % 1000000;
            if(num==0){return res;}
        }

        if (num >= 1000) {
            res += say_any_0_999(num / 1000) + " " + say_100x(1000) + " ";
            num = num % 1000;
            if(num==0){return res;}
        }

        res += say_any_0_999(num);

        return res;

    }


    public static void main(String[] args) {


        System.out.println(numberToWords(1000));
    }

}
