//https://leetcode.com/problems/evaluate-reverse-polish-notation/


import java.util.Arrays;
import java.util.Stack;

public class m_150 {
    public static int evalRPN(String[] tokens) {

        Stack<Integer> s = new Stack<>();
        for (String token : tokens) {
            switch (token) {
                case "+":
                    s.push(s.pop() + s.pop());
                    break;
                case "-":
                    s.push(-s.pop() + s.pop());
                    break;
                case "*":
                    s.push(s.pop() * s.pop());
                    break;
                case "/":
                    int a = s.pop(), b = s.pop();
                    s.push(b / a);
                    break;
                default:
                    s.push(Integer.valueOf(token));
            }

        }

        return s.pop();
    }


    public static void main(String[] args) {


        System.out.println(evalRPN(new String[]{
                "10","6","9","3","+","-11","*","/","*","17","+","5","+"
        }));
    }

}
