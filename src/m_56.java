//https://leetcode.com/problems/merge-intervals/


import java.util.Arrays;

public class m_56 {
    public static int fulfill(int[] seq, int[] range, int m) {
        int mm = 0;
        for (int p = range[1]; p >= range[0]; p--) {
            if (seq[p] != 0) {
                mm = seq[p];
                break;
            }
        }
        if (mm == 0) {
            m++;
            mm = m;
        }
        int p = range[0];
        while (p >= 1 && seq[p] == seq[p - 1] && seq[p] != 0) {
            p--;
        }
        for (; p <= range[1]; p++) {
            seq[p] = mm;
        }
        return m;
    }

    public static int[][] merge(int[][] intervals) {
        int[] seq = new int[10001];
        int m = 0;

        for (int[] interval : intervals) {
            m = fulfill(seq, interval, m);
        }

        int[][] res = new int[m][2];
        int j = 0;
        for (int i = 0; i < seq.length; i++) {
            if ((i == 0 && seq[0] > 0) || (i > 0 && seq[i - 1] != seq[i])) {
                res[j][0] = i;
            }
            if ((i == seq.length - 1 && seq[i] != 0) || (seq[i] != 0 && i < seq.length - 1 && seq[i + 1] != seq[i])) {
                res[j][1] = i;
                j++;
                if (j == m) {
                    break;
                }
            }
        }

        return Arrays.copyOfRange(res, 0, j);
    }


    public static void main(String[] args) {
        int[][] r1 = merge(new int[][]{
                new int[]{1, 3},
                new int[]{2, 6},
                new int[]{8, 10},
                new int[]{15, 18},
        });
        int[][] r2 = merge(new int[][]{
                new int[]{1, 4},
                new int[]{0, 0},
        });
        int[][] r3 = merge(new int[][]{
                new int[]{1, 4},
                new int[]{4, 5},
        });
        int[][] r4 = merge(new int[][]{
                new int[]{2, 3},
                new int[]{4, 5},
                new int[]{6, 7},
                new int[]{8, 9},
                new int[]{1, 10},
        });
        int[][] r5 = merge(new int[][]{
                new int[]{2, 3},
                new int[]{4, 6},
                new int[]{5, 7},
                new int[]{3, 4},
        });

        for (int[] ints : r1) {//[[1,6],[8,10],[15,18]]
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : r2) {//[[0,0],[1,4]]
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + ",");
            }
            System.out.print("]");
        }
        System.out.println("");

        for (int[] ints : r3) { //[[1,5]]
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + ",");
            }
            System.out.print("]");
        }
        System.out.println("");
        for (int[] ints : r4) {//[[1,10]]
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + ",");
            }
            System.out.print("]");
        }
        System.out.println("");
        for (int[] ints : r5) {//[[2,7]]
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j] + ",");
            }
            System.out.print("]");
        }

    }

}
