import java.util.*;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public static TreeNode gen(Integer... args) {
        Queue<TreeNode> kids = new LinkedList<>();
        List<TreeNode> nodes = new ArrayList<>();
        for (Integer a : args) {
            if (a == null) {
                nodes.add(null);
                kids.add(null);
            } else {
                TreeNode t = new TreeNode(a);
                nodes.add(t);
                kids.add(t);
            }
        }
        TreeNode root = kids.poll();
        for (TreeNode node : nodes) {
            if (node != null) {
                if (!kids.isEmpty()) {
                    node.left = kids.poll();
                }
                if (!kids.isEmpty()) {
                    node.right = kids.poll();
                }
            }
        }
        return root;
    }

    public static void main(String[] args) {
        TreeNode x = gen(5,1,4,null,null,3,6);
        System.exit(0);
    }


}
