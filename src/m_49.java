//https://leetcode.com/problems/group-anagrams/


import java.util.*;

public class m_49 {
    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String, LinkedList<String>> aMap = new HashMap<>();
        for (String str : strs) {
            char[] cs = str.toCharArray();
            Arrays.sort(cs);
            String key = String.valueOf(cs);
            if (!aMap.containsKey(key)) {
                aMap.put(key, new LinkedList<>());
            }
            aMap.get(key).add(str);
        }
        List<List<String>> t = new LinkedList<>();
        for (String str : aMap.keySet()) {
            t.add(aMap.get(str));
        }
        return t;
    }


    public static void main(String[] args) {
        List<List<String>> arr = groupAnagrams(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"});


        for (List<String> ar : arr) {
            System.out.print("[");
            for (String a : ar) {
                System.out.print(a + ",");
            }
            System.out.println("],");
        }

    }

}
